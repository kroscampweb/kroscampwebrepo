﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;

namespace Server.Controllers
{
    /// <summary>
    ///     This class takes care of all requests pertaining to navigations.
    /// </summary>
    public class NavigationsController : ApiController
    {
        #region Static fields

        private static Dictionary<int, List<int>> _zeroLevelNavigationsOdPricelists;
        private static Dictionary<int, int[]> _directChildrenOfNavigation;

        #endregion

        #region Private fields

        private readonly AmazingDataModelUnit _data = new AmazingDataModelUnit();

        #endregion

        #region Static methods

        /// <summary>
        ///     Initialize static attributes for faster searching in DB. Uses threads for initialization.
        /// </summary>
        public static void InitializeStaticAttributes()
        {
            if (_zeroLevelNavigationsOdPricelists == null)
            {
                var t = new Thread(delegate()
                {
                    using (var context = new AmazingDataModelUnit())
                    {
                        IEnumerable<Navigation> zeroLevelNavigations =
                            context.NavigationRepository.GetObjectsByExpression(nav => nav.Level == 0);
                        IEnumerable<PriceList> priceLists = context.PriceListRepository.GetAllObjects();
                        _zeroLevelNavigationsOdPricelists = new Dictionary<int, List<int>>(priceLists.Count());
                        foreach (PriceList priceList in priceLists)
                        {
                            IEnumerable<Navigation> baseNavigations =
                                zeroLevelNavigations.Where(nav => nav.IdPriceList == priceList.Id);
                            _zeroLevelNavigationsOdPricelists.Add(priceList.Id, new List<int>(baseNavigations.Count()));
                            foreach (Navigation navigation in baseNavigations)
                            {
                                _zeroLevelNavigationsOdPricelists[priceList.Id].Add(navigation.Id);
                            }
                        }
                    }
                });
                t.Start();
            }
            if (_directChildrenOfNavigation == null)
            {
                var t = new Thread(delegate()
                {
                    using (var context = new AmazingDataModelUnit())
                    {
                        IEnumerable<Navigation> navigations = context.NavigationRepository.GetAllObjects();
                        _directChildrenOfNavigation = new Dictionary<int, int[]>(navigations.Count());
                        foreach (Navigation navigation in navigations)
                        {
                            if (navigation.DirectDescedantIds != null && !navigation.DirectDescedantIds.Equals(""))
                            {
                                _directChildrenOfNavigation.Add(navigation.Id,
                                    Helpers.GetIntegersFromStringArrayOfIntegers(navigation.DirectDescedantIds));
                            }
                        }
                    }
                });
                t.Start();
            }
        }

        #endregion

        #region Public methods

        // GET: api/Navigations/GetChildrenOfNavigation/{id}
        /// <summary>
        ///     Returns all children navigations of navigation with specific ID
        /// </summary>
        /// <param name="id">ID of the navigation, whose children we need</param>
        /// <returns>IEnumerable of Navigations or null</returns>
        [ActionName("GetChildrenOfNavigation")]
        [HttpGet]
        public IEnumerable<Navigation> GetChildrenOfNavigation(int id)
        {
            IEnumerable<Navigation> result = null;
#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif
            if (_directChildrenOfNavigation.ContainsKey(id))
            {
                int[] navigationIds = _directChildrenOfNavigation[id];
                if (navigationIds != null)
                {
                    result = _data.NavigationRepository.GetObjectsByExpression(nav => navigationIds.Contains(nav.Id),
                        q => q.OrderBy(nav => nav.Order));
                }
            }

#if(Measuring)
            watch.Stop();
            Tools.Helpers.LogMeasurement("Server - Selecting navigation - children of navigation with id " + id + " done in " + watch.ElapsedMilliseconds + " ms", 1);
#endif

            return result;
        }

        // GET: api/Navigations/GetBaseNavigationOfPriceList/{id}
        /// <summary>
        ///     Returns the first level (Level 0) navigations of specific distributor
        /// </summary>
        /// <param name="id">Id of price list</param>
        /// <returns>IEnumerable of Navigations or null</returns>
        [ActionName("GetBaseNavigationOfPriceList")]
        [HttpGet]
        public IEnumerable<Navigation> GetBaseNavigationOfPriceList(int id)
        {
#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif
            IEnumerable<Navigation> result = null;
            if (_zeroLevelNavigationsOdPricelists.ContainsKey(id))
            {
                List<int> navigationIds = _zeroLevelNavigationsOdPricelists[id];
                if (navigationIds != null)
                {
                    result = _data.NavigationRepository.GetObjectsByExpression(nav => navigationIds.Contains(nav.Id),
                        q => q.OrderBy(nav => nav.Order));
                }
            }

#if(Measuring)
            watch.Stop();
            Tools.Helpers.LogMeasurement("Server - Selecting base navigations of distributor with id " + id + " done in " + watch.ElapsedMilliseconds + " ms", 1);
#endif

            return result;
        }

        #endregion
    }
}