﻿using ModelAndTools.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;

namespace Server.Controllers
{
    /// <summary>
    /// Returns data pertaining to Distributors
    /// </summary>
    public class DistributorsController : ApiController
    {
        private AmazingDataModelUnit _data;

        public DistributorsController()
        {
            _data = new AmazingDataModelUnit();
        }

        // GET: api/Distributors
        /// <summary>
        /// Returns list of all distributors and their pricelists in database
        /// </summary>
        /// <returns>IEnumerable of distributors</returns>
        public IEnumerable<PriceListDistributor> GetDistributors()
        {
#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif
            var result = (IEnumerable<PriceListDistributor>)_data.PriceListDistributorRepository.GetAllObjects();
#if(Measuring)
            watch.Stop();
            Tools.Helpers.LogMeasurement("Server - Selecting all distributors done in " + watch.ElapsedMilliseconds + " ms", 1);
#endif
            return result;
        }
    }
}