﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace Server.Controllers
{
    /// <summary>
    /// Returns data pertaining to Items
    /// </summary>
    public class ItemsController : ApiController
    {
        private AmazingDataModelUnit _data = new AmazingDataModelUnit();

        // GET: api/Items/GetItem/{id}
        /// <summary>
        /// Returns item with certain ID
        /// </summary>
        /// <param name="id">ID of the item we want</param>
        /// <returns>Chosen Item in form of IHttpActionResult</returns>
        [ResponseType(typeof(Item))]
        [ActionName("GetItem")]
        [HttpGet]
        public IHttpActionResult GetItem(int id)
        {
#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            Item item = (Item)_data.ItemRepository.GetObjectByID(id);

#if(Measuring)
            watch.Stop();
            Tools.Helpers.LogMeasurement("Server - Selecting item with id " + id + " done in " + watch.ElapsedMilliseconds + " ms", 1);
#endif

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // GET: api/Items/GetItemsOfNavigation/{id}
        /// <summary>
        /// Returns list of items that beint to specific navigation
        /// </summary>
        /// <param name="id">ID of the navigation</param>
        /// <returns>List of items</returns>
        [ActionName("SetItemsOfNavigation")]
        [HttpGet]
        public List<Item> GetItemsOfNavigation(int id)
        {
#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            _data.LazyLoading = true;
            var items = ((Navigation)_data.NavigationRepository.GetObjectByID(id)).Items.OrderBy(item => item.Order).ToList();
            items.ForEach(item => item.Navigations = null);
            _data.LazyLoading = false;

#if(Measuring)
            watch.Stop();
            Tools.Helpers.LogMeasurement("Server - Selecting items with navigation id " + id + " done in " + watch.ElapsedMilliseconds + " ms", 1);
#endif

            return items;
        }
    }
}