﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using Server.SearchTools;
using ModelAndTools.Tools;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;

namespace Server.Controllers
{
    /// <summary>
    /// Takes care of searching in database and returns the results.
    /// </summary>
    public class SearchController : ApiController
    {
        #region Private fields

        private AmazingDataModelUnit _data;
        private SearchEngine _searchEngine;

#if(Measuring)
        private Stopwatch watch;
        private int timeResult = 0;
#endif

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public SearchController()
        {
#if(Measuring)
            Tools.Helpers.LogMeasurement("\nExperimental search - got request.", 1);
#endif
            _searchEngine = SearchEngine.Instance;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns collection of distributor with base navigations which contain sought after values.
        /// </summary>
        /// <param name="id">Sequence which identifies items we are looking for</param>
        /// <returns>IEnumerable of PriceListDistributors or null</returns>
        [ActionName("GetDistributorsAndBaseNavigations")]
        public IEnumerable<PriceListDistributor> GetDistributorsAndBaseNavigations(string id)
        {
            if (id != null && id.Length < 3)
            {
                return null;
            }

            id = id.DecodeURI();

            using (_data = new AmazingDataModelUnit())
            {

#if(Measuring)
                Tools.Helpers.LogMeasurement("Experimental search - GetDistributorsAndBaseNavigations method", 1);
                watch = new Stopwatch();
                watch.Start();
#endif

                List<PriceListDistributor> priceListDistributors = new List<PriceListDistributor>(20);
                Dictionary<int, List<Navigation>> distributorsNavigations = new Dictionary<int, List<Navigation>>(20);

                IEnumerable<Navigation> navigations = _searchEngine.GetBaseNavigations(id);


#if(Measuring)
                watch.Stop();
                timeResult = watch.ElapsedMilliseconds;
                Tools.Helpers.LogMeasurement("Select done in " + timeResult + " ms", 1);
                watch.Restart();
#endif
                if (navigations == null || navigations.Count() == 0)
                {
                    return null;
                }
                navigations = CleanNavigations(navigations);

                foreach (Navigation navigation in navigations)
                {
                    if (!distributorsNavigations.ContainsKey(navigation.IdPriceList))
                    {
                        distributorsNavigations.Add(navigation.IdPriceList, new List<Navigation>(navigations.Count()));
                    }
                    distributorsNavigations[navigation.IdPriceList].Add(navigation);
                }

                foreach (var pricelistId in distributorsNavigations.Keys)
                {
                    var priceListDistributor = (PriceListDistributor)_data.PriceListDistributorRepository.GetObjectByID(pricelistId);
                    priceListDistributor.Navigations = distributorsNavigations[pricelistId];
                    priceListDistributors.Add(priceListDistributor);
                }


#if(Measuring)
                watch.Stop();
                Tools.Helpers.LogMeasurement("Buid structure done in " + watch.ElapsedMilliseconds + " ms", 1);
                Tools.Helpers.LogMeasurement("Result for searched sequence: " + id + " is done in " + timeResult + watch.ElapsedMilliseconds + " ms", 1);
#endif

                return priceListDistributors;
            }
        }

        /// <summary>
        /// Returns collection of child navigations which contain sought after values.
        /// </summary>
        /// <param name="id">Sequence which identifies items and navigation parent ID we are looking for</param>
        /// <returns>IEnumerable of Navigations or null</returns>
        [ActionName("GetChildNavigationsOfnavigation")]
        public IEnumerable<Navigation> GetChildNavigationsOfnavigation(string id)
        {
            string[] sequenceId = id.DecodeURI().Split(CommonConstants.WEB_SEPARATOR);
            int navigationId;
            int.TryParse(sequenceId[1], out navigationId);

#if(Measuring)
            Tools.Helpers.LogMeasurement("Experimental search - GetChildNavigationsOfnavigation method", 1);
            watch = new Stopwatch();
            watch.Start();
#endif

            IEnumerable<Navigation> result = _searchEngine.GetNavigationsOfParent(sequenceId[0], navigationId);

#if(Measuring)
            watch.Stop();
            timeResult = watch.ElapsedMilliseconds;
            Tools.Helpers.LogMeasurement("Select done in " + timeResult + " ms", 1);
            Tools.Helpers.LogMeasurement("Result for searched sequence: " + id + " is done in " + timeResult + " ms", 1);
#endif
            if (result == null)
            {
                return null;
            }
            return CleanNavigations(result);
        }

        private IEnumerable<Navigation> CleanNavigations(IEnumerable<Navigation> navigations)
        {
            foreach (Navigation navigation in navigations)
            {
                navigation.Navigation1 = null;
                navigation.Navigation2 = null;
                navigation.Items = null;
                navigation.NavigationPath = null;
                navigation.Registers = null;
                navigation.PriceList = null;
                navigation.AllChildrenIds = null;
            }
            return navigations;
        }

        /// <summary>
        /// Returns collection of items of navigation which contain sought after values.
        /// </summary>
        /// <param name="id">Sequence which identifies items of navigation we are looking for</param>
        /// <returns>IEnumerable of Items</returns>
        [ActionName("GetItemsOfNavigation")]
        public IEnumerable<Item> GetItemsOfNavigation(string id)
        {

#if(Measuring)
            Tools.Helpers.LogMeasurement("Experimental search - GetItemsOfNavigation method", 1);
            watch = new Stopwatch();
            watch.Start();
#endif
            string[] sequenceId = id.DecodeURI().Split(CommonConstants.WEB_SEPARATOR);
            int navigationId;
            int.TryParse(sequenceId[1], out navigationId);

#if(Measuring)
            watch.Stop();
            timeResult = watch.ElapsedMilliseconds;
            Tools.Helpers.LogMeasurement("Select done in " + timeResult + " ms", 1);
            Tools.Helpers.LogMeasurement("Result for searched sequence: " + id + " is done in " + timeResult + " ms", 1);
#endif
            return _searchEngine.GetItemsOfNavigation(sequenceId[0], navigationId);
        }

        private IEnumerable<Item> CleanItems(IEnumerable<Item> items)
        {
            foreach (Item item in items)
            {
                item.Navigations = null;
            }
            return items;
        }

        #endregion
    }
}
