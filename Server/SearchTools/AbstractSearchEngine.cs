﻿using ModelAndTools.Tools;
using ModelAndTools.Tools.External;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Server.SearchTools
{
    /// <summary>
    /// Support class for SearchEngine classes.
    /// </summary>
    public abstract class AbstractSearchEngine
    {
        #region Protected properties
        /// <summary>
        /// Property for code register.
        /// </summary>
        protected abstract SearchRegister CodeRegister { get; }

        /// <summary>
        /// Property for word register.
        /// </summary>
        protected abstract SearchRegister WordRegister { get; }

        /// <summary>
        /// Property for word root register.
        /// </summary>
        protected abstract SearchRegister BaseWordRegister { get; }

        #endregion

        #region Protected methods

        /// <summary>
        /// Get Ids of relevant elements.
        /// </summary>
        /// <param name="sentence">Identificators of wanted items(parts of name or code)</param>
        /// <returns>IEnumerable int of Ids</int></returns>
        protected IEnumerable<int> GetSearchedElementIds(string[] words)
        {
            if (words.Count() == 1 && Regex.IsMatch(words[0], CommonConstants.CODE_PATTERN))
            {
                IEnumerable<int> tmpIds = CodeRegister.GetIndexesOfWord(words[0]);
                if (tmpIds != null && tmpIds.Count() > 0)
                {
                    return tmpIds;
                }
            }
            return GetIdsFromWordRegisters(words);
        }

        /// <summary>
        /// Get Ids of relevant elements from word registers.
        /// </summary>
        /// <param name="words">string[] of words which identify the items you want</param>
        /// <returns>Hashset int of Ids</returns>
        protected HashSet<int> GetIdsFromWordRegisters(string[] words)
        {
            HashSet<int> ids = null;
            HashSet<int> tmpIdsSet = null;
            foreach (string word in words)
            {
                tmpIdsSet = new HashSet<int>(WordRegister.GetIndexesOfWord(word));
                tmpIdsSet.UnionWith(BaseWordRegister.GetIndexesOfWord(word.ToWordRoot()));

                if (ids == null)
                {
                    ids = tmpIdsSet;
                }
                else
                {
                    ids.IntersectWith(tmpIdsSet);
                }

                if (ids.Count == 0)
                {
                    return null;
                }
            }
            return ids;
        }

        #endregion
    }
}