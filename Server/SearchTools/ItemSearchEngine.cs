﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System.Collections.Generic;
using System.Linq;

namespace Server.SearchTools
{
    /// <summary>
    /// Uses registers to quickly find items of navigation.
    /// </summary>
    public class ItemSearchEngine : AbstractSearchEngine
    {
        #region Private fields

        private SearchRegister _codeRegister;
        private SearchRegister _wordRegister;
        private SearchRegister _baseOfWordRegister;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor. Initialize private fields.
        /// <param name="navigationId">ID of navigation</param>
        /// </summary>
        public ItemSearchEngine(int navigationId)
        {
            _codeRegister = new SearchRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_CODE_INFO.Type, navigationId: navigationId);
            _wordRegister = new SearchRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_WORD_INFO.Type, navigationId: navigationId);
            _baseOfWordRegister = new SearchRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_BASEWORD_INFO.Type, navigationId: navigationId);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns Items that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <returns>IEnumerable of Items</returns>
        public IEnumerable<Item> GetItems(string[] words)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var ids = GetItemIds(words);
                if (ids == null)
                {
                    return null;
                }
                var items = context.ItemRepository.GetObjectsByExpression(item => ids.Contains(item.Id));
                return items;
            }
        }

        /// <summary>
        /// Returns Ids of items that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <returns>IEnumerable of ints</returns>
        public IEnumerable<int> GetItemIds(string[] words)
        {
            return GetSearchedElementIds(words);
        }

        #endregion

        #region Protected properties

        protected override SearchRegister CodeRegister
        {
            get { return _codeRegister; }
        }

        protected override SearchRegister WordRegister
        {
            get { return _wordRegister; }
        }

        protected override SearchRegister BaseWordRegister
        {
            get { return _baseOfWordRegister; }
        }

        #endregion
    }
}