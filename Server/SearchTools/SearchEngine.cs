﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using Server.Controllers;

namespace Server.SearchTools
{
    public class SearchEngine
    {
        #region Private fields

        private static SearchEngine _instance;

        private readonly Dictionary<int, List<string>> _distributorNames;
        private readonly ItemsController _itemsController;
        private readonly Dictionary<int, NavigationSearchEngine> _navigationRegisters;
        private readonly NavigationsController _navigationsController;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize attributes.
        /// </summary>
        private SearchEngine()
        {
            _itemsController = new ItemsController();
            _navigationsController = new NavigationsController();
            _navigationRegisters = new Dictionary<int, NavigationSearchEngine>();
            _distributorNames = new Dictionary<int, List<string>>();
            using (var context = new AmazingDataModelUnit())
            {
                context.LazyLoading = true;
                IEnumerable<PriceList> priceLists =
                    context.PriceListRepository.GetObjectsByExpression(pl => pl.Navigations.Any());

                var pool = new Semaphore(0, priceLists.Count());
                pool.Release(priceLists.Count());
                var mutex = new Semaphore(0, 1);
                mutex.Release(1);

                foreach (PriceList priceList in priceLists)
                {
                    var t = new Thread(delegate()
                    {
                        pool.WaitOne();
                        var engine = new NavigationSearchEngine(priceList.Id);

                        mutex.WaitOne();
                        _navigationRegisters.Add(priceList.Id, engine);
                        _distributorNames.Add(priceList.Id, priceList.Distributor.Name.GetWords());
                        mutex.Release();
                        pool.Release();
                    });
                    t.Start();
                }
                for (int i = 0; priceLists.Count() > i; i++)
                {
                    pool.WaitOne();
                }
            }
        }

        #endregion

        #region Static methods

        public static SearchEngine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SearchEngine();
                }
                return _instance;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        ///     Returns base navigations via sequence.
        /// </summary>
        /// <param name="sequence">Searched string</param>
        /// <returns>Navigations</returns>
        public IEnumerable<Navigation> GetBaseNavigations(string sequence)
        {
            var baseNavigations = new List<Navigation>();
            IEnumerable<string> searchedWords = sequence.GetWords();
            foreach (int priceListId in GetSearchedPriceListIds(ref searchedWords))
            {
                if (searchedWords.Any())
                {
                    IEnumerable<Navigation> result = _navigationRegisters[priceListId].GetBaseNavigations(searchedWords.ToArray());
                    if (result != null)
                    {
                        baseNavigations.AddRange(result);
                    }
                }
                else
                {
                    IEnumerable<Navigation> result = _navigationsController.GetBaseNavigationOfPriceList(priceListId);
                    if (result != null)
                    {
                        baseNavigations.AddRange(result);
                    }
                }
            }
            return baseNavigations;
        }

        /// <summary>
        ///     Returns navigations of parent via sequence and navigation ID.
        /// </summary>
        /// <param name="sequence">Searched string</param>
        /// <param name="parentId">Id of parent navigation</param>
        /// <returns>Navigations</returns>
        public IEnumerable<Navigation> GetNavigationsOfParent(string sequence, int parentId)
        {
            var navigations = new List<Navigation>();
            IEnumerable<string> searchedWords = sequence.GetWords();
            foreach (int priceListId in GetSearchedPriceListIds(ref searchedWords))
            {
                if (searchedWords.Any())
                {
                    IEnumerable<Navigation> result = _navigationRegisters[priceListId].GetNavigationsOfParent(searchedWords.ToArray(),
                        parentId);
                    if (result != null)
                    {
                        navigations.AddRange(result);
                    }
                }
                else
                {
                    IEnumerable<Navigation> result = _navigationsController.GetChildrenOfNavigation(parentId);
                    if (result != null)
                    {
                        navigations.AddRange(result);
                    }
                }
            }
            return navigations;
        }

        /// <summary>
        ///     Returns items of navigation via sequence and navigation ID.
        /// </summary>
        /// <param name="sequence">Searched string</param>
        /// <param name="navigationId">Id of navigation</param>
        /// <returns>Returns items</returns>
        public IEnumerable<Item> GetItemsOfNavigation(string sequence, int navigationId)
        {
            var items = new List<Item>();
            IEnumerable<string> searchedWords = sequence.GetWords();
            foreach (int priceListId in GetSearchedPriceListIds(ref searchedWords))
            {
                if (searchedWords.Any())
                {
                    IEnumerable<Item> result = _navigationRegisters[priceListId].GetItemsOfNavigation(searchedWords.ToArray(),
                        navigationId);
                    if (result != null)
                    {
                        items.AddRange(result);
                    }
                }
                else
                {
                    List<Item> result = _itemsController.GetItemsOfNavigation(navigationId);
                    if (result != null)
                    {
                        items.AddRange(result);
                    }
                }
            }
            return items;
        }

        #endregion

        #region Private helpers

        /// <summary>
        ///     Returns IDs of distributors which names are in searched strings.
        /// </summary>
        /// <param name="searchedStrings">Array of searched strings</param>
        /// <returns>Array of IDs of distributors</returns>
        private int[] GetSearchedPriceListIds(ref IEnumerable<string> searchedStrings)
        {
            foreach (int priceListId in _distributorNames.Keys)
            {
                if (_distributorNames[priceListId].Intersect(searchedStrings).Count() ==
                    _distributorNames[priceListId].Count)
                {
                    searchedStrings = searchedStrings.Except(_distributorNames[priceListId]);
                    return new[] {priceListId};
                }
            }
            return _distributorNames.Keys.ToArray();
        }

        #endregion
    }
}