﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.SearchTools
{
    /// <summary>
    /// Uses registers to quickly find navigations of distributor.
    /// </summary>
    public class NavigationSearchEngine : AbstractSearchEngine
    {
        private const short CIRCA_NAVIGATIONS_COUNT = 50;

        #region Private fields

        private SearchRegister _codeRegister;
        private SearchRegister _wordRegister;
        private SearchRegister _baseOfWordRegister;

        private Dictionary<int, int[]> _navigationPaths;
        private Dictionary<int, int[]> _navigationDescendants;
        private Dictionary<int, ItemSearchEngine> _navigationItemRegisters;

        #endregion

        #region Constructors

        /// <summary>
        /// Private Constructor.
        /// </summary>
        public NavigationSearchEngine(int priceListID)
        {
            _codeRegister = new SearchRegister(CommonConstants.REGISTER_NAVIGATION_CODE_INFO.Type,
                priceListId: priceListID);
            _wordRegister = new SearchRegister(CommonConstants.REGISTER_NAVIGATION_WORD_INFO.Type,
                priceListId: priceListID);
            _baseOfWordRegister = new SearchRegister(CommonConstants.REGISTER_NAVIGATION_BASEWORD_INFO.Type,
                priceListId: priceListID);

            CreateRegisters(priceListID);

            BuildNavigationPaths(priceListID);
            BuildDescendants(priceListID);
            BuildNavigationItemRegisters(priceListID);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns item IDs of navigation that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <param name="navigationId">Id of navigation</param>
        /// <returns>IEnumerable of Item IDs</returns>
        public IEnumerable<int> GetItemsOfNavigationId(string[] words, int navigationId)
        {
            if (!_navigationItemRegisters.ContainsKey(navigationId))
            {
                return null;
            }
            return _navigationItemRegisters[navigationId].GetItemIds(words);
        }

        /// <summary>
        /// Returns items of navigation that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <param name="navigationId">Id of navigation</param>
        /// <returns>IEnumerable of Items</returns>
        public IEnumerable<Item> GetItemsOfNavigation(string[] words, int navigationId)
        {
            if (!_navigationItemRegisters.ContainsKey(navigationId))
            {
                return null;
            }
            return _navigationItemRegisters[navigationId].GetItems(words);
        }

        /// <summary>
        /// Returns IDs of base(0 level) navigation that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <returns>IEnumerable of ints</returns>
        public IEnumerable<int> GetBaseNavigationIds(string[] words)
        {
            IEnumerable<int> navigationIds = GetSearchedElementIds(words);

            if (navigationIds == null || navigationIds.Count() == 0)
            {
                return null;
            }

            List<int> result = new List<int>(CIRCA_NAVIGATIONS_COUNT);
            foreach (int navigationId in navigationIds)
            {
                if (!result.Contains(navigationId))
                {
                    if (!_navigationPaths.ContainsKey(navigationId))
                    {
                        if (ContainsSearchedItem(navigationId, words))
                        {
                            result.Add(navigationId);
                        }
                    }
                    else if (!result.Contains(_navigationPaths[navigationId][0]) &&
                             ContainsSearchedItem(navigationId, words))
                    {
                        result.Add(_navigationPaths[navigationId][0]);
                    }
                }
            }

            return result; //.Distinct();
        }

        /// <summary>
        /// Returns base(0 level) navigations that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <returns>IEnumerable of Navigations</returns>
        public IEnumerable<Navigation> GetBaseNavigations(string[] words)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                IEnumerable<int> navigationIds = GetBaseNavigationIds(words);
                if (navigationIds == null)
                {
                    return null;
                }
                var navigations =
                    context.NavigationRepository.GetObjectsByExpression(
                        navigation => navigationIds.Contains(navigation.Id));
                return navigations;
            }
        }

        /// <summary>
        /// Returns children navigations that fit the criteria.
        /// </summary>
        /// <param name="sequence">Criteria for searching</param>
        /// <param name="parentId">Id of parent navigation</param>
        /// <returns>IEnumerable of Navigations</returns>
        public IEnumerable<Navigation> GetNavigationsOfParent(string[] words, int parentId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                IEnumerable<int> navigationIds = GetPotencionaNavigationIdsOfParent( words, parentId);
                if (navigationIds == null)
                {
                    return null;
                }
                var result =
                    context.NavigationRepository.GetObjectsByExpression(
                        navigation => navigationIds.Contains(navigation.Id), q => q.OrderBy(nav => nav.Order));
                return result;
            }
        }

        #endregion

        #region Private helpers

        private IEnumerable<Navigation> GetAllNavigationsOfPriceList(int priceListId, AmazingDataModelUnit context)
        {
            return context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceListId);
        }

        /// <summary>
        /// Initiaize attributes of this class.
        /// </summary>
        private void CreateRegisters(int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var navigationsCount =GetAllNavigationsOfPriceList(priceListId,context).Count();

                _navigationPaths = new Dictionary<int, int[]>(navigationsCount);
                _navigationDescendants = new Dictionary<int, int[]>(navigationsCount);
                _navigationItemRegisters = new Dictionary<int, ItemSearchEngine>(navigationsCount);
            }
        }

        /// <summary>
        /// Building structure for navigation-item registers.
        /// </summary>
        private void BuildNavigationItemRegisters(int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                foreach (var register in context.RegisterRepository.GetObjectsByExpression(register => register.Type == CommonConstants.REGISTER_ITEM_NAVIGATION_CODE_INFO.Type )) 
                {
                    _navigationItemRegisters.Add((int) register.NavigationId,
                        new ItemSearchEngine((int) register.NavigationId));
                }
            }
        }

        /// <summary>
        /// Building structure for descendants of navigations.
        /// </summary>
        private void BuildDescendants(int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var navigations = GetAllNavigationsOfPriceList(priceListId, context);
                foreach (Navigation navigation in navigations)
                {
                    if (navigation.AllChildrenIds != null)
                    {
                        string[] numbers = navigation.AllChildrenIds.Split(CommonConstants.COMMA_SIGN);

                        int[] convertedNumbers = Array.ConvertAll<string, int>(numbers, int.Parse);
                        _navigationDescendants.Add(navigation.Id, convertedNumbers);
                    }
                }
            }
        }

        /// <summary>
        /// Method for building paths of navigations.
        /// </summary>
        private void BuildNavigationPaths(int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var navigations = GetAllNavigationsOfPriceList(priceListId, context);
                foreach (Navigation navigation in navigations)
                {
                    if (navigation.Level > 0)
                    {
                        string[] numbers = navigation.NavigationPath.Split(CommonConstants.COMMA_SIGN);

                        int[] convertedNumbers = Array.ConvertAll<string, int>(numbers, int.Parse);
                        _navigationPaths.Add(navigation.Id, convertedNumbers);
                    }
                }
            }
        }

        /// <summary>
        /// Method for check if navigation contains searched items.
        /// </summary>
        /// <param name="navigationId">Id of navigation that can contains searched items</param>
        /// <param name="sequence">Searched sequence</param>
        /// <returns>true if navigation contains searched item else false</returns>
        private bool ContainsSearchedItem(int navigationId, string[] words)
        {
            if (_navigationItemRegisters.ContainsKey(navigationId))
            {
                var result = _navigationItemRegisters[navigationId].GetItemIds(words);
                if (result != null && result.Any() )
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method returns all IDs of children(navigations) that can contains searched items.
        /// </summary>
        /// <param name="sequence">searched sequence</param>
        /// <param name="parentId">Parent navigation ID</param>
        /// <returns>IEnumerabe int type IDs of potencional navigations</returns>
        private IEnumerable<int> GetPotencionaNavigationIdsOfParent(string[] words, int parentId)
        {
            if (!_navigationDescendants.ContainsKey(parentId))
            {
                return null;
            }

            int wantedLevel = _navigationPaths.ContainsKey(parentId) ? _navigationPaths[parentId].Count() + 1 : 1;

            IEnumerable<int> navigationIds = GetSearchedElementIds(words);
            HashSet<int> potencionalResult = new HashSet<int>(navigationIds);
            potencionalResult.IntersectWith(_navigationDescendants[parentId]);

            HashSet<int> result = new HashSet<int>();
            foreach (int id in potencionalResult)
            {
                int intId = (int) id;
                if (_navigationPaths[intId].Count() == wantedLevel)
                {
                    if (!result.Contains(intId))
                    {
                        if (ContainsSearchedItem(intId, words))
                        {
                            result.Add(intId);
                        }
                    }
                }
                else if (!result.Contains(_navigationPaths[intId][wantedLevel]))
                {
                    if (ContainsSearchedItem(intId, words))
                    {
                        result.Add(_navigationPaths[intId][wantedLevel]);
                    }
                }
            }

            return result;
        }

        #endregion

        #region Protected properties

        protected override SearchRegister CodeRegister
        {
            get { return _codeRegister; }
        }

        protected override SearchRegister WordRegister
        {
            get { return _wordRegister; }
        }

        protected override SearchRegister BaseWordRegister
        {
            get { return _baseOfWordRegister; }
        }

        #endregion
    }
}