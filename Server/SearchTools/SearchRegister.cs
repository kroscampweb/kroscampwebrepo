﻿using System.Data;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Server.SearchTools
{
    /// <summary>
    /// Register stores data for SearchEngine and return searched IDs of navigation.
    /// </summary>
    public class SearchRegister
    {
        #region Constants

        private short CIRCA_SELECT_WORD_IDS_COUNT = 250;
        private short CIRCA_SELECT_CODE_IDS_COUNT = 250;
        private short CIRCA_ITEMS_COUNT = 110;

        #endregion

        #region Private fields

        private Dictionary<String, int[]> _register;
        private short _typeOfRegister;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor. Initialization of private fields.
        /// </summary>
        /// <param name="typeOfRegister">Type of register (constants from CommonConstants)</param>
        /// <param name="navigationId">Mandatory only if this is register of navigation</param>
        public SearchRegister(short typeOfRegister, int navigationId = -1, int priceListId = -1)
        {
            _typeOfRegister = typeOfRegister;
            _register = new Dictionary<string, int[]>();
            if (navigationId != -1)
            {
                InitializeRegister(navigationId: navigationId);
            }
            else if (priceListId != -1)
            {
                InitializeRegister(priceListId: priceListId);
            }
            else
            {
                throw new DataException("priceListId or navigationId must be initialized.");
            }
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Property used to get register.
        /// </summary>
        public Dictionary<String, int[]> Register
        {
            get { return _register; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get indexes from register that contain searchWord.
        /// </summary>
        /// <param name="searchWord">Word which identifies relevant items</param>
        /// <returns>IEnumerable int of Ids</returns>
        public IEnumerable<int> GetIndexesOfWord(string searchWord)
        {
            switch (_typeOfRegister%3)
            {
                case 1: // Kódy položiek
                    return SearchInCodeRegister(searchWord);
                case 2: // Slová položiek
                    return SearchInWordRegister(searchWord);
                case 0: // Skrátené slova položiek
                    return SearchInBaseWordRegister(searchWord);
                default:
                    return null;
            }
        }

        #endregion

        #region Protected properties

        /// <summary>
        /// Property used to get type of register.
        /// </summary>
        protected short TypeOfRegister
        {
            get { return _typeOfRegister; }
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Initialize register.
        /// </summary>
        /// <param name="ID">ID of navigation</param>
        private void InitializeRegister(int navigationId = -1, int priceListId = -1)
        {
            if (navigationId + priceListId == -2)
            {
                throw new IndexOutOfRangeException("Nesprávne indexy registrov.");
            }
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                Register register;
                _register = new Dictionary<string, int[]>(CIRCA_ITEMS_COUNT);
                if (navigationId != -1)
                {
                    register =
                        (Register)
                            context.RegisterRepository.GetObjectsByExpression(
                                r => r.Type == TypeOfRegister && r.NavigationId == navigationId).First();
                }
                else if (priceListId != -1)
                {
                    //throw new KeyNotFoundException("Takýto kľúč sa v databáze nenachádza.");
                    //register = (Register)context.RegisterRepository.GetObjectsByExpression(r => r.Type == TypeOfRegister).First();
                    //_register = new Dictionary<string, int[]>(context.ItemRepository.GetAllObjects().Count() / 2);
                    register =
                        context.RegisterRepository.GetObjectsByExpression(
                            r => r.Type == TypeOfRegister && r.PriceListId == priceListId).First();
                }
                else
                {
                    throw new NullReferenceException("Nieje vyplnený žiaden parameter.");
                }
                using (StringReader reader = new StringReader(register.Data))
                {
                    string line;
                    string helper;

                    while ((line = reader.ReadLine()) != null)
                    {
                        try
                        {
                            string[] keyValue = line.Split(CommonConstants.WORD_IDS_SEPARATOR);
                            int[] convertedNumbers = Helpers.GetIntegersFromStringArrayOfIntegers(keyValue[1]);
                            if (Register.ContainsKey(keyValue[0]))
                            {
                                Register[keyValue[0]].Union(convertedNumbers);
                            }
                            else
                            {
                                Register.Add(keyValue[0], convertedNumbers);
                            }
                            helper = line;
                        }
                        catch (Exception e)
                        {
                            Helpers.LogError(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Use code to search for items.
        /// </summary>
        /// <param name="code">Code which identifies relevant items</param>
        /// <returns>HashSet int of Ids</returns>
        private IEnumerable<int> SearchInCodeRegister(string code)
        {
            List<int> ids = new List<int>(CIRCA_SELECT_CODE_IDS_COUNT);
            foreach (var key in Register.Keys)
            {
                if (key.Length >= code.Length && key.Substring(0, code.Length).Equals(code))
                {
                    ids.AddRange(Register[key]);
                }
            }
            return ids.Distinct();
        }

        /// <summary>
        /// Use word to search for items.
        /// </summary>
        /// <param name="searchWord">Word which identifies relevant items</param>
        /// <returns>HashSet int of Ids</returns>
        private IEnumerable<int> SearchInWordRegister(string searchWord)
        {
            List<int> ids = new List<int>(CIRCA_SELECT_WORD_IDS_COUNT);
            var synonyms = GetSynonymsOfWord(searchWord);

            foreach (string synonym in synonyms)
            {
                foreach (var word in Register.Keys)
                {
                    if (word.Length >= synonym.Length && word.Substring(0, synonym.Length).Equals(synonym))
                    {
                        ids.AddRange(Register[word]);
                    }
                }
            }
            return ids.Distinct();
        }

        /// <summary>
        /// Use baseword to search for items.
        /// </summary>
        /// <param name="searchBaseWord">BaseWord which identifies relevant items</param>
        /// <returns>HashSet int of Ids</returns>
        private IEnumerable<int> SearchInBaseWordRegister(string searchBaseWord)
        {
            if (Register.ContainsKey(searchBaseWord))
            {
                return Register[searchBaseWord];
            }
            return new List<int>();
        }

        /// <summary>
        /// Get synonyms of word.
        /// </summary>
        /// <param name="word">Word, which you want to check if it has synonyms</param>
        /// <returns>List string if word has synonms returns synonyms (including parameter word), else List contains only word</returns>
        private IEnumerable<string> GetSynonymsOfWord(string word)
        {
            foreach (var words in CommonConstants.Synonyms)
            {
                int i = words.IndexOf(word);
                if (i != -1)
                {
                    return words;
                }
            }
            List<string> list = new List<string>();
            list.Add(word);
            return list;
        }

        #endregion
    }
}