﻿using Server.Controllers;
using Server.SearchTools;
using System;
using System.Diagnostics;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Server
{
    public class Global : HttpApplication
    {
#if Measuring
        private const string message = "Experimentalne meranie";
#endif
        void Application_Start(object sender, EventArgs e)
        {
#if Measuring
            Stopwatch watch=new Stopwatch();
            watch.Start();
#endif

            // Code that runs on application startup
            var engine = SearchEngine.Instance;
            NavigationsController.InitializeStaticAttributes();


            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

#if Measuring
            watch.Stop();
            Tools.Helpers.LogMeasurement("\n\nServer started - " + message + " on " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString() + ", " + DateTime.Now,1);
            Tools.Helpers.LogMeasurement("Server started in " + watch.ElapsedMilliseconds,1);
#endif
        }
    }
}