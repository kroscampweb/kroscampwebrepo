﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;

namespace AfterRegisterBuilderTest
{
    [TestClass]
    public class NavigationDescedantsBuilderTest
    {
        [TestMethod]
        public void NavigationsDescedantsCount()
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                int navigations1 = context.NavigationRepository.GetObjectsByExpression(nav => nav.AllChildrenIds != null).Count();
                var navigations2 = context.NavigationRepository.GetObjectsByExpression(nav => nav.DirectDescedantIds != null).Count();
                var navigations3 = context.NavigationRepository.GetObjectsByExpression(nav => nav.Navigation1.Any()).Count();
                Assert.AreEqual(navigations1, navigations2);
                Assert.AreEqual(navigations3, navigations2);
            }
        }
    }
}
