﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelAndTools.Models;

namespace AfterRegisterBuilderTest
{
    /// <summary>
    ///     Summary description for NavigationRegisterAndPathBuilderTest
    /// </summary>
    [TestClass]
    public class NavigationRegisterAndPathBuilderTest
    {
        // TODO: Skontrolovať, či sú inicializované IDčka v DB po zbehnutí buildera.
        [TestMethod]
        public void NavigationsPathCount()
        {
            using (var context = new AmazingDataModelUnit())
            {
                int navigations1 =
                    context.NavigationRepository.GetObjectsByExpression(nav => nav.NavigationPath != null).Count();
                int navigations2 =
                    context.NavigationRepository.GetObjectsByExpression(nav => nav.Level > 0).Count();
                Assert.AreEqual(navigations1, navigations2);
            }
        }

        [TestMethod]
        public void NavigationsRegisterCount()
        {
            using (var context = new AmazingDataModelUnit())
            {
                int navigations3 =
                    context.NavigationRepository.GetObjectsByExpression(nav => nav.Items.Any()).Count()*3 + 3;
                int registers = context.RegisterRepository.GetAllObjects().Count();
                Assert.AreEqual(navigations3, registers);
            }
        }
    }
}