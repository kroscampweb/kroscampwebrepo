﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ModelAndTools.Models
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Select all objects from data table
        /// </summary>
        /// <returns>IEnumerable of objects</returns>
        IEnumerable<TEntity> GetAllObjects();

        /// <summary>
        /// Select from database
        /// </summary>
        /// <param name="filter">Query, which determines what is selected (voluntary parameter)</param>
        /// <param name="orderBy">Function, which is used to order data (voluntary parameter)</param>
        /// <param name="includeProperties">List of columns we want (voluntary parameter)</param>
        /// <returns></returns>
        IEnumerable<TEntity> GetObjectsByExpression(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");

        /// <summary>
        /// Selects object from database
        /// </summary>
        /// <param name="Id">ID of object</param>
        /// <returns>Wanted object</returns>
        TEntity GetObjectByID(int Id);

        /// <summary>
        /// Add item to repository
        /// </summary>
        /// <param name="element">Added item</param>
        void Add(TEntity element);

        /// <summary>
        /// Save changes to database
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Attach object with context.
        /// </summary>
        void Attach(TEntity element);

        /// <summary>
        /// Remove object from database.
        /// </summary>
        TEntity Remove(TEntity element);
    }
}
