﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ModelAndTools.Models
{
    /// <summary>
    /// Generic class, which mines data from context
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Private fields

        private DbContext _context;
        private DbSet<TEntity> _dbSet;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes attributes
        /// </summary>
        /// <param name="context">Data context we want to use</param>
        public GenericRepository(DbContext context)
        {
            this._context = context;
            this._dbSet = context.Set<TEntity>();
        }

        #endregion

        #region Public methods

        public virtual IEnumerable<TEntity> GetAllObjects()
        {
            return _dbSet.ToList();
        }

        public virtual IEnumerable<TEntity> GetObjectsByExpression(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual TEntity GetObjectByID(int Id)
        {
            return _dbSet.Find(Id);
        }

        public virtual void Add(TEntity item)
        {
            _dbSet.Add(item);
        }

        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Attach(TEntity element)
        {
            _dbSet.Attach(element);
        }

        public TEntity Remove(TEntity element)
        {
            return _dbSet.Remove(element);
        }

        #endregion
    }
}