﻿using ModelAndTools.Models.DataModels;
using System.Collections.Generic;

namespace ModelAndTools.Models
{
    /// <summary>
    /// Data structure, which represents combination of pricelist and distributor
    /// </summary>
    public class PriceListDistributor
    {
        public int PriceListId { get; set; }
        public string PriceListCode { get; set; }
        public string PriceListName { get; set; }
        public string PriceListContent { get; set; }
        public int DistributorId { get; set; }
        public string DistributorContact { get; set; }
        public string DistributorName { get; set; }
        public string Logo { get; set; }
        public List<Navigation> Navigations { get; set; }
    }
}