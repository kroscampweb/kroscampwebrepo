namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PriceList")]
    public partial class PriceList
    {
        public PriceList()
        {
            Navigations = new HashSet<Navigation>();
        }

        public int Id { get; set; }

        public int IdDistributor { get; set; }

        [Required]
        [StringLength(15)]
        public string Code { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Content { get; set; }

        [StringLength(150)]
        public string Updates { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ValidUntil { get; set; }

        [StringLength(255)]
        public string TransportCost { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual ICollection<Navigation> Navigations { get; set; }

        public virtual ICollection<Register> Registers { get; set; }
    }
}