namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Distributor")]
    public partial class Distributor
    {
        public Distributor()
        {
            PriceLists = new HashSet<PriceList>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Location { get; set; }

        [Column(TypeName = "text")]
        public string Contact { get; set; }

        [StringLength(255)]
        public string Logo { get; set; }

        public virtual ICollection<PriceList> PriceLists { get; set; }
    }
}
