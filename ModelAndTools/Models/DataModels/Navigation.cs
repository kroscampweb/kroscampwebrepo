namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Navigation")]
    public partial class Navigation
    {
        public Navigation()
        {
            Registers = new HashSet<Register>();
            Navigation1 = new HashSet<Navigation>();
            Items = new HashSet<Item>();
        }

        public int Id { get; set; }

        public int IdPriceList { get; set; }

        public int? Parent { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public short Level { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public int? Order { get; set; }

        [Column(TypeName = "text")]
        public string AllChildrenIds { get; set; }

        [Column(TypeName = "text")]
        public string DirectDescedantIds { get; set; }

        [StringLength(255)]
        public string NavigationPath { get; set; }

        public virtual PriceList PriceList { get; set; }

        public virtual ICollection<Register> Registers { get; set; }

        public virtual ICollection<Navigation> Navigation1 { get; set; }

        public virtual Navigation Navigation2 { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
