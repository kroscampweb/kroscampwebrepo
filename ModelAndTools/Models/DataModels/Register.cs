namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Register")]
    public partial class Register
    {
        public int Id { get; set; }

        public short Type { get; set; }

        [Column(TypeName = "text")]
        public string Data { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public int? NavigationId { get; set; }

        public virtual Navigation Navigation { get; set; }

        public int? PriceListId { get; set; }

        public virtual PriceList PriceList { get; set; }
    }
}
