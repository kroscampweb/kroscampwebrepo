namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Common;
    using ModelAndTools.Tools;

    public partial class AmazingDataModel : DbContext
    {
        public static DbConnection GetDefaultConnection()
        {
            DbConnection _connection = DbProviderFactories.GetFactory(CommonConstants.PROVIDER_NAME).CreateConnection();
            _connection.ConnectionString = CommonConstants.CONNECTION_STRING;
            return _connection;
        }

        public AmazingDataModel()
            : base(GetDefaultConnection(), true)
        {
        }

        public AmazingDataModel(DbConnection connection)
            : base(connection, true)
        {
        }

        public virtual DbSet<Distributor> Distributors { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Navigation> Navigations { get; set; }
        public virtual DbSet<PriceList> PriceLists { get; set; }
        public virtual DbSet<Register> Registers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Distributor>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Distributor>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<Distributor>()
                .Property(e => e.Contact)
                .IsUnicode(false);

            modelBuilder.Entity<Distributor>()
                .Property(e => e.Logo)
                .IsUnicode(false);

            modelBuilder.Entity<Distributor>()
                .HasMany(e => e.PriceLists)
                .WithRequired(e => e.Distributor)
                .HasForeignKey(e => e.IdDistributor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Unit)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Url)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.ImgUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.ManufacturerCode)
                .IsUnicode(false);

            modelBuilder.Entity<Item>()
                .HasMany(e => e.Navigations)
                .WithMany(e => e.Items)
                .Map(m => m.ToTable("ItemNavigation").MapLeftKey("ItemId").MapRightKey("navigationId"));

            modelBuilder.Entity<Navigation>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Navigation>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Navigation>()
                .Property(e => e.AllChildrenIds)
                .IsUnicode(false);

            modelBuilder.Entity<Navigation>()
                .HasMany(e => e.Navigation1)
                .WithOptional(e => e.Navigation2)
                .HasForeignKey(e => e.Parent);

            modelBuilder.Entity<PriceList>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<PriceList>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PriceList>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<PriceList>()
                .Property(e => e.Updates)
                .IsUnicode(false);

            modelBuilder.Entity<PriceList>()
                .Property(e => e.TransportCost)
                .IsUnicode(false);

            modelBuilder.Entity<PriceList>()
                .HasMany(e => e.Navigations)
                .WithRequired(e => e.PriceList)
                .HasForeignKey(e => e.IdPriceList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Register>()
                .Property(e => e.Data)
                .IsUnicode(false);

            modelBuilder.Entity<Register>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}
