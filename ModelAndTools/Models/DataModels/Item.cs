namespace ModelAndTools.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Item")]
    public partial class Item
    {
        public Item()
        {
            Navigations = new HashSet<Navigation>();
        }

        public int Id { get; set; }

        public int Order { get; set; }

        [Required]
        [StringLength(35)]
        public string Code { get; set; }

        [Required]
        [StringLength(50)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [StringLength(255)]
        public string Url { get; set; }

        [StringLength(255)]
        public string ImgUrl { get; set; }

        public double? Weight { get; set; }

        public decimal? VAT { get; set; }

        [StringLength(35)]
        public string ManufacturerCode { get; set; }

        public virtual ICollection<Navigation> Navigations { get; set; }
    }
}