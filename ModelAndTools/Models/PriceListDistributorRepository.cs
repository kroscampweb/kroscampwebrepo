﻿using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ModelAndTools.Models
{
    /// <summary>
    /// Class, which mines data of pricelists and distributors and returns PriceListDistributor
    /// </summary>
    public class PriceListDistributorRepository : IRepository<PriceListDistributor>
    {
        #region Private fields

        private IRepository<PriceList> _priceListRepository;
        private IRepository<Distributor> _distributorRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes attribute
        /// </summary>
        /// <param name="context"></param>
        public PriceListDistributorRepository(IRepository<Distributor> dr, IRepository<PriceList> plr)
        {
            _priceListRepository = plr;
            _distributorRepository = dr;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns all distributors
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PriceListDistributor> GetAllObjects()
        {
            return GetAllPriceListDistributors();
        }

        public PriceListDistributor GetObjectByID(int Id)
        {
            return GetPriceListDistributorByPriceListId(Convert.ToInt32(Id));
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Selects data from PriceLists and Distributors and creates PriceListDistributors
        /// </summary>
        /// <returns>Collection of PriceListDistributors</returns>
        private IEnumerable<PriceListDistributor> GetAllPriceListDistributors()
        {
            return from d in _distributorRepository.GetAllObjects()
                   join p in _priceListRepository.GetAllObjects()
                   on d equals p.Distributor
                   select new PriceListDistributor()
                   {
                       PriceListId = p.Id,
                       PriceListCode = p.Code,
                       PriceListName = p.Name,
                       PriceListContent = p.Content,
                       DistributorId = d.Id,
                       DistributorContact = d.Contact,
                       DistributorName = d.Name,
                       Logo = d.Logo
                   };
        }

        /// <summary>
        /// Returns PriceListDistributor with select PriceList ID
        /// </summary>
        /// <param name="Id">ID of the PriceList</param>
        /// <returns>PriceListDistributor</returns>
        private PriceListDistributor GetPriceListDistributorByPriceListId(int Id)
        {
            PriceList priceList = (PriceList)_priceListRepository.GetObjectByID(Id);
            Distributor distributor = (Distributor)_distributorRepository.GetObjectByID(priceList.IdDistributor);
            return new PriceListDistributor() { DistributorContact = distributor.Contact, DistributorId = distributor.Id, DistributorName = distributor.Name, PriceListCode = priceList.Code, PriceListContent = priceList.Content, PriceListId = priceList.Id, PriceListName = priceList.Name, Logo = distributor.Logo };
        }

        #endregion

        #region Not implemented

        public void Add(PriceListDistributor element)
        {
            throw new NotImplementedException();
        }

        public void Attach(PriceListDistributor element)
        {
            throw new NotImplementedException();
        }

        public PriceListDistributor Remove(PriceListDistributor element)
        {
            throw new NotImplementedException();
        }


        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws exception
        /// </summary>
        /// <param name="filter">Irrelevant - only because it is inherited from Interface</param>
        /// <param name="orderBy">Irrelevant - only because it is inherited from Interface</param>
        /// <param name="includeProperties">Irrelevant - only because it is inherited from Interface</param>
        /// <returns></returns>
        public IEnumerable<PriceListDistributor> GetObjectsByExpression(System.Linq.Expressions.Expression<Func<PriceListDistributor, bool>> filter = null, Func<IQueryable<PriceListDistributor>, IOrderedQueryable<PriceListDistributor>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}