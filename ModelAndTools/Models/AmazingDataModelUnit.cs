﻿using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace ModelAndTools.Models
{
    /// <summary>
    /// Class used to manipulate database.
    /// </summary>
    public class AmazingDataModelUnit : IDisposable
    {

        #region Private fields

        private AmazingDataModel _context;
        private IRepository<Item> _itemRepository;
        private IRepository<Navigation> _navigationRepository;
        private IRepository<PriceList> _priceListRepository;
        private IRepository<Distributor> _distributorRepository;
        private IRepository<PriceListDistributor> _priceListDistributorRepository;
        private IRepository<Register> _registerRepository;
        private bool _disposed = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes data context.
        /// </summary>
        public AmazingDataModelUnit()
        {
            InitializeAttributes();
        }

        #endregion

        #region Destructor

        ~AmazingDataModelUnit()
        {
            Dispose();
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Property for setting lazyloading of current context.
        /// </summary>
        public bool LazyLoading
        {
            set
            {
                _context.Configuration.LazyLoadingEnabled = value;
            }
        }

        /// <summary>
        /// Getter of current DbContext.
        /// </summary>
        public DbContext Context
        {
            get
            {
                return _context;
            }
        }

        /// <summary>
        /// Getter of object that manipulates items in database.
        /// </summary>
        public IRepository<Item> ItemRepository
        {
            get
            {
                if (this._itemRepository == null)
                {
                    this._itemRepository = new GenericRepository<Item>(_context);
                }
                return _itemRepository;
            }
        }

        /// <summary>
        /// Getter of object that manipulates navigations in database.
        /// </summary>
        public IRepository<Navigation> NavigationRepository
        {
            get
            {
                if (this._navigationRepository == null)
                {
                    this._navigationRepository = new GenericRepository<Navigation>(_context);
                }
                return _navigationRepository;
            }
        }

        /// <summary>
        /// Getter of object that manipulates price lists in database.
        /// </summary>
        public IRepository<PriceList> PriceListRepository
        {
            get
            {
                if (this._priceListRepository == null)
                {
                    this._priceListRepository = new GenericRepository<PriceList>(_context);
                }
                return _priceListRepository;
            }
        }

        /// <summary>
        /// Getter of object that manipulates distributors in database.
        /// </summary>
        public IRepository<Distributor> DistributorRepository
        {
            get
            {
                if (this._distributorRepository == null)
                {
                    this._distributorRepository = new GenericRepository<Distributor>(_context);
                }
                return _distributorRepository;
            }
        }

        /// <summary>
        /// Getter of object that manipulates registers in database.
        /// </summary>
        public IRepository<Register> RegisterRepository
        {
            get
            {
                if (this._registerRepository == null)
                {
                    this._registerRepository = new GenericRepository<Register>(_context);
                }
                return _registerRepository;
            }
        }

        /// <summary>
        /// Getter of object that manipulates price list - distributors.
        /// </summary>
        public IRepository<PriceListDistributor> PriceListDistributorRepository
        {
            get
            {
                if (this._priceListDistributorRepository == null)
                {
                    this._priceListDistributorRepository = new PriceListDistributorRepository(DistributorRepository, PriceListRepository);
                }
                return _priceListDistributorRepository;
            }
        }

        #endregion

        #region Protected method

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Saves changes to database.
        /// </summary>
        public void SaveChanges()
        {
            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        /// <summary>
        /// Disposes of context.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Save changes to database then dispose of context and create new DbContext.
        /// </summary>
        /// <param name="lazyLoading">Set lazyLoading of current context</param>
        public void SaveChangesAndRecreateContext(Boolean lazyLoading)
        {
            SaveChanges();
            AllRepositorySetToNull();
            _context.Dispose();
            InitializeAttributes();
            LazyLoading = lazyLoading;
        }
        #endregion

        #region Private helpers

        /// <summary>
        ///  Method for initialization fields.
        /// </summary>
        private void InitializeAttributes()
        {
            _context = new AmazingDataModel();
            _context.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<AmazingDataModel>(null);
        }

        /// <summary>
        /// Removes all instances of repositories.
        /// </summary>
        private void AllRepositorySetToNull()
        {
            _itemRepository = null;
            _navigationRepository = null;
            _priceListRepository = null;
            _distributorRepository = null;
            _priceListDistributorRepository = null;
            _registerRepository = null;
        }

        #endregion
    }
}