namespace ModelAndTools.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PricelistRegister : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Register", "PriceListId", c => c.Int());
            CreateIndex("dbo.Register", "PriceListId");
            AddForeignKey("dbo.Register", "PriceListId", "dbo.PriceList", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Register", "PriceListId", "dbo.PriceList");
            DropIndex("dbo.Register", new[] { "PriceListId" });
            DropColumn("dbo.Register", "PriceListId");
        }
    }
}
