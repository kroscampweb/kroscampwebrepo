namespace ModelAndTools.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zaklad : DbMigration
    {
        public override void Up()
        {
            
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceList", "IdDistributor", "dbo.Distributor");
            DropForeignKey("dbo.Navigation", "IdPriceList", "dbo.PriceList");
            DropForeignKey("dbo.Register", "NavigationId", "dbo.Navigation");
            DropForeignKey("dbo.Navigation", "Parent", "dbo.Navigation");
            DropForeignKey("dbo.ItemNavigation", "navigationId", "dbo.Navigation");
            DropForeignKey("dbo.ItemNavigation", "ItemId", "dbo.Item");
            DropIndex("dbo.ItemNavigation", new[] { "navigationId" });
            DropIndex("dbo.ItemNavigation", new[] { "ItemId" });
            DropIndex("dbo.Register", new[] { "NavigationId" });
            DropIndex("dbo.Navigation", new[] { "Parent" });
            DropIndex("dbo.Navigation", new[] { "IdPriceList" });
            DropIndex("dbo.PriceList", new[] { "IdDistributor" });
            DropTable("dbo.ItemNavigation");
            DropTable("dbo.Register");
            DropTable("dbo.Item");
            DropTable("dbo.Navigation");
            DropTable("dbo.PriceList");
            DropTable("dbo.Distributor");
        }
    }
}
