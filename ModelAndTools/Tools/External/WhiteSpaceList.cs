﻿

namespace ModelAndTools.Tools.External
{
    /// <summary>
    /// Slová, ktoré sa majú vo vyhľadavaní ignorovať.
    /// </summary>
    using System.Collections.Generic;
    public class WhiteSpaceList : IWhiteSpaceList
    {
        #region Fields

        private static WhiteSpaceList _instance;
        private HashSet<string> _ignoreList;

        #endregion


        #region Constructors

        /// <summary>
        /// Konštruktor.
        /// </summary>
        private WhiteSpaceList()
        {
            _ignoreList = new HashSet<string>() {"A", "ABY", "AJ", "AK", "AKO", "ANI", "AŽ", "BO", "BY", "ČI", 
                        "DO", "HOC", "I", "IBA", "K", "KEĎ", "KU", "KÝM", "LEN", "NA", "NECH", "NEŽ", "O", "OD", 
                        "ODO", "PO", "S", "SO", "SŤA", "TAK", "TÝM", "U", "V", "VO", "Z", "ZA", "ZO", "ŽE" };

        }

        #endregion


        #region Public Properties

        /// <summary> 
        /// Vráti inštanciu triedy WhiteSpaceList.
        /// </summary>
        public static WhiteSpaceList Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WhiteSpaceList();
                }
                return _instance;
            }
        }


        /// <summary>
        /// Interface pre slová, ktoré sa majú vo vyhľadavaní ignorovať.
        /// </summary>
        public bool IsWhiteSpace(string iWord)
        {
            return _ignoreList.Contains(iWord);
        }

        #endregion

    }


    /// <summary>
    /// Interface pre slová, ktoré sa majú vo vyhľadavaní ignorovať.
    /// </summary>
    public interface IWhiteSpaceList
    {
        /// <summary>
        /// Zistí či je dané slovo medzi ignorovanými.
        /// </summary>
        /// <param name="iWord">Slovo, ktoré sa zisťuje.</param>
        /// <returns>Vráti true ak sa iWord nachádza v zozname ignorovaných slov.</returns>
        bool IsWhiteSpace(string iWord);
    }
}