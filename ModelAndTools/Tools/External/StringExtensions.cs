﻿using System;
using System.Text;
using System.Diagnostics.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections;

namespace ModelAndTools.Tools.External
{
    /// <summary>
    /// Extension pre string.
    /// </summary>
    public static class StringExtension
    {

        #region Constants

        public const string CONSONANTS = "DGHKLNTCJBMPRSVZF";
        public const int WORD_ROOT = 4;

        #endregion

        #region Methods

        /// <summary>
        /// Pridá k stringbuilderu text so separátorom.
        /// </summary>
        /// <param name="iValue">
        /// String builder, ku ktorému pridávame.
        /// </param>
        /// <param name="iText">
        /// Text, ktorý chceme pridať k stringbuilderu.
        /// </param>
        /// <param name="iSeparator">
        /// Pridávaný separátor.
        /// </param>
        public static void AddTextWithSeparator(this StringBuilder iValue,
                                                                        string iText,
                                                                        string iSeparator)
        {
            Contract.Requires(iValue != null);
            if (!string.IsNullOrWhiteSpace(iText))
            {
                if (iValue.Length > 0)
                {
                    iValue.Append(iSeparator);
                }
                iValue.Append(iText);
            }
        }

        /// <summary>
        /// Odstráni diakritiku zo stringu.
        /// </summary>
        /// <param name="iValue">Upravovaný text.</param>
        /// <returns>Pôvodný text bez diakritiky.</returns>
        public static string RemoveDiacritic(this string iValue)
        {
            return iValue.RemoveDiacritic(false);

        }

        /// <summary>
        /// Odstráni diakritiku zo stringu v možnosťou vrátiť text ako upper.
        /// </summary>
        /// <param name="iValue">Upravovaný text.</param>
        /// <param name="iToUpper">Prepínač, či vrátiť výsledný text ako upper.</param>
        /// <returns>Pôvodný text bez diakritiky.</returns>
        public static string RemoveDiacritic(this string iValue,
                                                                bool iToUpper)
        {
            if (iValue != null)
            {
                StringBuilder ret = new StringBuilder();
                string text = iValue.Normalize(System.Text.NormalizationForm.FormD);
                for (int i = 0; i < text.Length; i++)
                {
                    if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(text[i]) !=
        System.Globalization.UnicodeCategory.NonSpacingMark)
                    {
                        ret.Append(iToUpper ? Char.ToUpper(text[i]) : text[i]);
                    }
                }
                return ret.ToString().Normalize(System.Text.NormalizationForm.FormC);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Zistí, či text obsahuje všetky hľadané slová.
        /// <para>
        /// Je Case Sensitive (neignoruje diakritiku).
        /// </para>
        /// </summary>
        /// <param name="iValue">Text.</param>
        /// <param name="iWords">Hladané slová.</param>
        /// <returns>Vráti true, len ak text obsahuje všetky hľadané slová (v akomkoľvek poradí). 
        /// Inak vráti false.</returns>
        public static bool ContainsAllWords(this string iValue, string[] iWords)
        {
            Contract.Requires(Contract.ForAll(iWords, p => p != null));
            Contract.Assume(iWords != null);
            return !iWords.Any(p => !iValue.Contains(p));
        }

        /// <summary>
        /// Zistí, či text obsahuje všetky hľadané slová.
        /// <para>
        /// Je Case Sensitive (neignoruje diakritiku).
        /// </para>
        /// </summary>
        /// <param name="iValue">Text.</param>
        /// <param name="iWords">Hladané slová.</param>
        /// <returns>Vráti true, len ak text obsahuje všetky hľadané slová (v akomkoľvek poradí). 
        /// Inak vráti false.</returns>
        public static bool ContainsAllWords(this string iValue, List<string> iWords)
        {
            Contract.Requires(Contract.ForAll(iWords, p => p != null));
            Contract.Assume(iWords != null);
            return !iWords.Any(p => !iValue.Contains(p));
        }

        /// <summary>
        /// Z vety vytvorí zoznam slov (ToUpper).
        /// </summary>
        /// <param name="iValue">Veta.</param>
        /// <returns>Zoznam slov.</returns>
        public static List<string> ToWords(this string iValue, IZoznamMJ iZoznamMJ)
        {
            return iValue.ToWords(true, iZoznamMJ);

        }

        /// <summary>
        /// Z vety vytvorí zoznam slov (ToUpper).
        /// </summary>
        /// <param name="iValue">Veta.</param>
        /// <param name="iAcceptJonters">Akceptuje spojovníky.</param>
        /// <returns>Zoznam slov.</returns>
        public static List<string> ToWords(this string iValue, bool iAcceptJonters, IZoznamMJ iZoznamMJ)
        {
            string tmpString;
            char c;
            bool newLine;
            StringBuilder newWord = null;
            if (iValue == null)
            {
                iValue = string.Empty;

            }

            List<string> ret = new List<string>();
            iValue = Regex.Replace(iValue, "(\n|\r)+", " ");
            string text = iValue.Normalize(System.Text.NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            char preview = Char.MinValue;
            for (int i = 0; i < text.Length; i++)
            {
                newLine = true;
                if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(text[i]) !=
                                           System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    c = Char.ToUpper(text[i]);
                    if (!c.IsSeparator() || c.IsSeparatorInMernaJednotka(iZoznamMJ, sb.ToString(), text.Substring(i)))
                    {
                        sb.Append(c);
                        if (newWord != null)
                        {
                            newWord.Append(c);
                        }
                    }
                    else
                    {
                        if (i + 1 < text.Length)
                        {
                            if (!char.IsSeparator(c))
                            {
                                if (c.IsJointer() && !preview.IsSeparator())
                                {
                                    if (!text[i + 1].IsSeparator())
                                    {
                                        if (iAcceptJonters)
                                        {
                                            if (newWord != null)
                                            {
                                                tmpString = newWord.ToString(); if (!string.IsNullOrEmpty(tmpString))
                                                {
                                                    AddToWords(ret, tmpString.Normalize(System.Text.NormalizationForm.FormC), iZoznamMJ);
                                                }
                                            }
                                            else
                                            {
                                                tmpString = sb.ToString();
                                                if (!string.IsNullOrEmpty(tmpString))
                                                {
                                                    if (tmpString.IsNumber() && c == ',')
                                                    {
                                                        //keby si niekto zmyslel, opat rozdelovat cisla na celu a destinnu cast :)
                                                    }
                                                    else
                                                    {
                                                        AddToWords(ret, tmpString.Normalize(System.Text.NormalizationForm.FormC), iZoznamMJ);
                                                    }
                                                }
                                            }
                                            newWord = new StringBuilder();
                                        }
                                        sb.Append(c);
                                        newLine = false;
                                    }
                                }
                            }
                            else
                            {
                                if (c.IsNumbers(sb.ToString(), text[i + 1]))
                                {
                                    newLine = false;
                                }
                            }
                        }
                        if (newLine)
                        {
                            if (newWord != null)
                            {
                                tmpString = newWord.ToString();
                                if (!string.IsNullOrEmpty(tmpString))
                                {
                                    if (sb.ToString().IsNumber() && tmpString.IsNumber())
                                    {
                                        //keby si niekto zmyslel, opat rozdelovat cisla na celu a destinnu cast :)
                                    }
                                    else
                                    {
                                        AddToWords(ret, tmpString.Normalize(System.Text.NormalizationForm.FormC), iZoznamMJ);
                                    }
                                }
                            }
                            newWord = null;
                            if (sb.Length > 0)
                            {
                                AddToWords(ret, sb.ToString().Normalize(System.Text.NormalizationForm.FormC), iZoznamMJ);
                            }
                            sb = new StringBuilder();
                        }
                    }
                    preview = c;
                }
            }
            if (sb.Length > 0)
            {
                AddToWords(ret, sb.ToString().Normalize(System.Text.NormalizationForm.FormC), iZoznamMJ);
            }
            ret = ret.Where(p => !WhiteSpaceList.Instance.IsWhiteSpace(p)).ToList();
            return ret;
        }

        /// <summary>
        /// Vytvorí slovotvorný základ slova.
        /// </summary>
        /// <param name="iValue">Slovo.</param>
        /// <returns>Slovotvorný základ slova.</returns>
        public static string ToWordRoot(this string iValue)
        {
            string ret = iValue;
            if (iValue.Length > WORD_ROOT && !iValue.IsNumber())
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(iValue.Substring(0, WORD_ROOT));
                for (int i = WORD_ROOT; i < iValue.Length; i++)
                {
                    if (CONSONANTS.Contains(iValue[i]))
                    {
                        sb.Append(iValue[i]);
                    }
                }
                ret = sb.ToString();
            }
            return ret;
        }

        /// <summary>
        /// Zistí, či je reťazec špeciálne číslo.
        /// </summary>
        /// <param name="iValue">Reťazec.</param>
        /// <returns>Ak je špeciálne číslo, vráti true. Inak vráti false.</returns>
        public static bool IsSpecialNumber(this string iValue)
        {
            bool ret = true;
            for (int i = 0; i < iValue.Length; i++)
            {
                if (!(Char.IsDigit(iValue[i]) || CharExtension.NUMBER_SEPARATOR.Contains(iValue[i])))
                {
                    if (!CharExtension.NUMBER_CHAR.Contains(iValue[i]))
                    {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Zistí, či je reťazec číslo.
        /// </summary>
        /// <param name="iValue">Reťazec.</param>
        /// <returns>Vráti true, ak je reťazec číslo. Inak vráti false.</returns>
        public static bool IsNumber(this string iValue)
        {
            double number;
            bool ret = double.TryParse(iValue.Trim(), out number);
            return ret;
        }

        /// <summary>
        /// Zistí, či je reťazec celé číslo.
        /// </summary>
        /// <param name="iValue">Reťazec.</param>
        /// <returns>Vráti true, ak je reťazec celé číslo. Inak vráti false.</returns>
        public static bool IsInt(this string iValue)
        {
            int number;
            bool ret = int.TryParse(iValue.Trim(), out number);
            return ret;
        }

        /// <summary>

        /// Bezpečné odstránenie bielych znamkov zo začiatku stringu.

        /// </summary>

        /// <param name="iValue">Reťazec na odstánenie biely znakov zo začiatku.</param>

        /// <returns>Orezany reťazec, alebo Null ak bola hodnota null.</returns>

        public static string SafeTrimStart(this string iValue)
        {

            return iValue == null ? null : iValue.TrimStart();

        }





        /// <summary>

        /// Zistenie dĺžky reťazca.

        /// </summary>

        /// <param name="iValue">Zisťovaný reťazec.</param>

        /// <returns>Dĺžka reťazca, alebo 0 ak je reťazec null.</returns>

        public static int SafeLength(this string iValue)
        {

            return iValue == null ? 0 : iValue.Length;

        }





        /// <summary>

        /// Bezpečné získanie substringu, v prípade, že oreginálny text je kratší tak vráti oreginálny text.

        /// </summary>

        /// <param name="iValue">Oreginálny text.</param>

        /// <param name="iLength">Počet znakov ktoré chceme.</param>

        /// <returns>

        /// Daný počet znakov od začiatku.

        /// </returns>

        public static string SafeSubstring(this string iValue,

                                                   int iLength)
        {

            Contract.Requires(iLength > 0);



            Contract.Assert(iLength > 0);

            string ret = iValue;

            var length = iValue.SafeLength();



            if (length > 0 && iLength < length)
            {

                ret = iValue.Substring(0, iLength);

            }



            if (string.IsNullOrWhiteSpace(ret))
            {

                ret = null;

            }



            return ret;

        }





        /// <summary>

        /// Odstránenie číslic zo začiatku stringu.

        /// </summary>

        /// <param name="iValue">Kontrolovaný string.</param>

        /// <returns>String bez počiatočných číslic.</returns>

        public static string RemoveStartDigits(this string iValue)
        {

            return iValue.TrimStart(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });

        }





        /// <summary>

        /// Odstráni z reťazca nechcené znaky.

        /// </summary>

        /// <param name="iValue">Reťazec.</param>

        /// <param name="iUnexpectedCharsString">Reťazec znakov na vymazanie.</param>

        /// <returns></returns>

        public static string RemoveChars(this string iValue,

                                              string iUnexpectedCharsString)
        {

            string empty = "";

            string ret = iValue;



            if (!(string.IsNullOrEmpty(iValue) || string.IsNullOrEmpty(iUnexpectedCharsString)))
            {

                foreach (var item in iUnexpectedCharsString)
                {

                    var str = item.ToString();

                    if (!string.IsNullOrEmpty(str))
                    {

                        ret = ret.Replace(str, empty);

                    }

                }

            }



            return ret;

        }





        /// <summary>

        /// Oreže reťazec na požadovaný počet znakov.

        /// </summary>

        /// <param name="iValue">Reťazec.</param>

        /// <param name="iMaxLength">Maximálna dlžka.</param>

        /// <returns>Orezaný reťazec.</returns>

        public static string Truncate(this string iValue,

                                              int iMaxLength)
        {

            Contract.Requires(iMaxLength >= 0);

            Contract.Ensures(Contract.Result<string>() == null || Contract.Result<string>().Length <= iMaxLength);



            string ret = iValue;



            if (iValue == null)
            {

                ret = null;

            }

            else
            {

                if (iValue.Length > iMaxLength)
                {

                    ret = iValue.Substring(0, iMaxLength);

                }

            }



            return ret;

        }





        /// <summary>

        /// Zabezpečí maximimálnu dĺžku vyskladaného stringu podľa zadanej hodnoty.

        /// </summary>

        /// <param name="iFormat">Formátovací reťazec, ktorý sa má použiť ako string.Format.</param>

        /// <param name="iValues">Hodnoty z ktorých sa má vytvoriť názov.</param>

        /// <param name="iMaxLength">Maximálna dĺžka reťazca.</param>

        /// <returns>Vráti reťazec vyskladaný podľa zadaného formátu a hodnôt. Nepresiahne zadanú dĺžku.</returns>

        /// <exception cref="ArgumentOutOfRangeException">

        /// Ak je iFormat sam o sebe dlhsi ako je maximalna dlzka.

        /// </exception>

        public static string ProportionalTruncate(this string iFormat,

                                                          int iMaxLength,

                                              params string[] iValues)
        {

            Contract.Requires(!string.IsNullOrEmpty(iFormat));

            Contract.Requires(iMaxLength > 0);

            Contract.Requires(iValues != null);

            Contract.Ensures(Contract.Result<string>().Length <= iMaxLength);



            if (string.Format(iFormat, iValues.Select(p => "").ToArray()).Length > iMaxLength)
            {

                throw new ArgumentOutOfRangeException("Zadany string format je dlhsi ako je maximalna dlzka.");

            }



            string ret = string.Format(iFormat, iValues);



            if (ret.Length > iMaxLength)
            {

                var overSize = ret.Length - iMaxLength + 4;

                var orezane = TruncateParams(iValues, overSize);



                ret = string.Format("{0}{1}", string.Format(iFormat, orezane.ToArray()), HashCodeFromString(ret));

                Contract.Assume(ret.Length <= iMaxLength);

            }



            return ret;

        }





        /// <summary>

        /// Replace desatinnej bodky a čiarky desatinným oddeľovačom.

        /// </summary>

        /// <param name="iValue">kontrolovan8 hodnota.</param>

        /// <returns>

        /// Hodnota so správnym desatinným oddeľvoačom.

        /// </returns>

        public static string ReplaceWithCorrectDecimalSeparator(this string iValue)
        {

            string ret = null;

            var decimalSeparator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;



            if (iValue != null)
            {

                ret = iValue.ToString().Replace(CommonConstants.COMMA_SIGN.ToString(), decimalSeparator).

                        Replace(CommonConstants.DOT_SIGN.ToString(), decimalSeparator);

            }



            return ret;

        }





        /// <summary>

        /// Rozdelenie stringu podľa oddeľovača.

        /// </summary>

        /// <param name="iValue">String, ktorý chcem rozdeliť.</param>

        /// <param name="iSeparator">Oddeľovač podľa ktorého rozdeľujem.</param>

        /// <returns>

        /// Rozdelené podľa oddeľovača.

        /// </returns>

        public static IList<string> SplitBySeparator(this string iValue,

                                                          string iSeparator)
        {

            return SplitBySeparator(iValue, iSeparator, true);

        }





        /// <summary>

        /// Rozdelenie stringu podľa oddeľovača.

        /// </summary>

        /// <param name="iValue">String, ktorý chcem rozdeliť.</param>

        /// <param name="iSeparator">Oddeľovač podľa ktorého rozdeľujem.</param>

        /// <param name="iCanRemoveEmptyLines">

        /// True, ak má z rozdeleného stringu odstrániť prázdne riadky. Inak false.

        /// </param>

        /// <returns>

        /// Rozdelené podľa oddeľovača.

        /// </returns>

        public static IList<string> SplitBySeparator(this string iValue,

                                                          string iSeparator,

                                                            bool iCanRemoveEmptyLines)
        {

            List<string> ret = new List<string>();



            if (!string.IsNullOrEmpty(iValue))
            {

                var splitOptions = iCanRemoveEmptyLines ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

                ret = iValue.Split(new string[] { iSeparator }, splitOptions).ToList();

            }



            return ret;

        }





        /// <summary>

        /// Vráti prvý riadok reťazca.

        /// </summary>

        /// <param name="iValue">Reťazec.</param>

        /// <returns>

        /// Vráti prvý riadok reťazca.

        /// <para> Ak nemá reťazec viaceré riadky, vráti pôvodný reťazec.</para>

        /// <para> Ak je reťazec string.Empty, vráti string.Empty.</para>

        /// <para> Ak je reťazec null, vráti null.</para>

        /// </returns>

        public static Line GetFirstLine(this string iValue)
        {

            Contract.Ensures(Contract.Result<Line>() != null);



            int startIndex = 0;

            int indexOfNewLine;

            Line ret;



            if (string.IsNullOrEmpty(iValue))
            {

                ret = new Line(iValue, false);

            }

            else
            {

                indexOfNewLine = iValue.IndexOf(Environment.NewLine);



                if (indexOfNewLine == -1)
                {

                    ret = new Line(iValue, false);

                }

                else
                {

                    ret = new Line(iValue.Substring(startIndex, indexOfNewLine), true);

                }

            }



            return ret;

        }





        /// <summary>

        /// Riadok reťazca.

        /// </summary>

        public class Line
        {



            #region Constructors



            /// <summary>

            /// Konštruktor.

            /// </summary>

            /// <param name="iText">Text riadku.</param>

            /// <param name="iIsFromMultilineText">Príznak, či riadok vznikol z textu s viacerými riadkami.</param>

            public Line(string iText,

                          bool iIsFromMultilineText)
            {

                Text = iText;

                IsFromMultilineText = iIsFromMultilineText;

            }



            #endregion





            #region Public Properties



            /// <summary>

            /// Text riadku.

            /// </summary>

            public string Text
            {

                get;

                private set;

            }





            /// <summary>

            /// Príznak, či riadok vznikol z textu s viacerými riadkami.

            /// </summary>

            public bool IsFromMultilineText
            {

                get;

                private set;

            }



            #endregion



        }





        /// <summary>

        /// Získanie informácie z textovej hodnoty. (Informácia musí byť ohraničená kľučovím slovom a koncom riadku)

        /// </summary>

        /// <param name="iValue">Zdroj, v ktorom hľadám informáciu.</param>

        /// <param name="iStartKeyWords">Kĺučové slovo, určujúce začiatok informácie.</param>

        /// <returns>

        /// Informácia z textovej hodnoty. Môže byť aj String.Empty ak sa informácia nenašla.

        /// </returns>

        public static string GetInformationFromValue(this string iValue,

                                                          string iStartKeyWords,

                                                          string iEndKeyWords)
        {

            Contract.Requires(!string.IsNullOrEmpty(iStartKeyWords));

            Contract.Ensures(Contract.Result<string>() != null);



            string ret = string.Empty;



            if (!string.IsNullOrWhiteSpace(iValue))
            {

                int startIndex = iValue.IndexOf(iStartKeyWords, StringComparison.OrdinalIgnoreCase);



                if (startIndex > 0)
                {

                    int endIndex = iValue.IndexOf(iEndKeyWords, startIndex);

                    endIndex = endIndex > 0 ? endIndex : iValue.Length;

                    endIndex = endIndex - startIndex - iStartKeyWords.Length;



                    if (endIndex > 0)
                    {

                        ret = iValue.Substring(startIndex + iStartKeyWords.Length, endIndex).TrimStart(' ');

                    }

                }

            }



            return ret;

        }



        #endregion

        #region Private Helpers





        /// <summary>

        /// Zisťovanie, či sa jedná o separátor ktorý je možné považovať za súčasť mernej jednotky.

        /// </summary>

        /// <param name="iSeparator">Kontrolovaný separátor.</param>

        /// <param name="iZoznamMJ">Zoznam merných jednotiek.</param>

        /// <param name="iPreviewChars">Predchádzajúce znaky v slove.</param>

        /// <param name="iNextChars">Nasledujúce znaky do konca.</param>

        /// <returns>Jedná o separátor ktorý je možné považovať za súčasť mernej jednotky</returns>

        private static bool IsSeparatorInMernaJednotka(this char iSeparator,

                                                       IZoznamMJ iZoznamMJ,

                                                          string iPreviewChars,

                                                          string iNextChars)
        {

            bool ret = false;



            if (!Char.IsSeparator(iSeparator))
            {



                var nasledujuceSlovo = iNextChars.Replace(",", " ").Split(new Char[] { ' ' }).FirstOrDefault(p => p.Length > 0);

                if (nasledujuceSlovo != null && iZoznamMJ != null)
                {

                    ret = iZoznamMJ.IsMernaJednotka(string.Format("{0}{1}",

                        iPreviewChars.RemoveStartDigits(), nasledujuceSlovo.ToUpper()));

                }

            }



            return ret;

        }





        /// <summary>

        /// Pridá do zonamu slov, tak že checkuje ci sa nejedná o číslo s mernou jednotkou, 

        /// lebo v tom pripade sa ma dostat do registra slovo spolu s MJ a nie samostatne.

        /// </summary>

        /// <param name="iWords">Kolekcia slov.</param>

        /// <param name="iWord">Pridávané slovo.</param>

        /// <param name="iZoznamMJ">Zoznam merných jednotiek.</param>

        private static void AddToWords(List<string> iWords, string iWord, IZoznamMJ iZoznamMJ)
        {

            Contract.Requires(iWords != null);



            string last = iWords.LastOrDefault();



            if (iZoznamMJ != null && iWords.Count > 0 && iZoznamMJ.IsMernaJednotka(iWord) && last.IsNumber())
            {

                iWords[iWords.Count - 1] = last + iWord;

            }

            else
            {

                iWords.Add(iWord);

            }

        }





        private static string HashCodeFromString(string iValue)
        {

            Contract.Ensures(Contract.Result<string>() == null || Contract.Result<string>().Length <= 4);



            int hashCode = iValue.GetHashCode();



            if (hashCode < 0)
            {

                hashCode = -1 * hashCode;

            }



            return hashCode.ToString().Truncate(4);

        }





        private static List<string> TruncateParams(string[] iValues,

                                                        int iOverSize)
        {

            Contract.Requires(iValues != null);



            var ret = new List<string>();

            var orez = iOverSize / iValues.Count();

            var zvysok = iOverSize % iValues.Count();



            foreach (var item in iValues)
            {

                int oKolko = orez;

                if (zvysok > 0)
                {

                    oKolko = orez + zvysok;

                    zvysok = 0;

                }



                var naKolko = item.Length - oKolko;

                if (naKolko < 0)
                {

                    zvysok = item.Length - naKolko - 1;

                    naKolko = 1;

                }



                ret.Add(item.Truncate(naKolko));

            }



            return ret;

        }



        #endregion

    }


    /// <summary>
    /// Interface pre zoznam MJ.
    /// </summary>
    public interface IZoznamMJ : IEnumerable<string>
    {
        /// <summary>
        /// Zistí či je zadané slovo mernou jednotkou.
        /// </summary>
        /// <param name="iWord">Slovo, ktoré sa zisťuje.</param>
        /// <returns>Vráti true ak sa iWord nachádza v zozname merných jednotiek.</returns>
        bool IsMernaJednotka(string iWord);

        /// <summary>
        /// Zistí či sa jedná o číslo s mernou jednotkou?
        /// </summary>
        /// <param name="iWord">Kontrolované slovo.</param>
        /// <returns>Jedná sa o číslo s mernou jednotkou?</returns>
        bool IsNumberWithMernaJednotka(string iWord);
    }

    /// <summary>
    /// Singleton. Trieda poskytujúca zoznam merných jednotiek.
    /// Created by: MINAROVSKY at 12/7/2011 9:14:27 AM
    /// </summary>        
    public class ZoznamMJ : IZoznamMJ
    {

        #region Fields

        private static ZoznamMJ _instance;

        private static IEnumerable<string> _merneJednotky =

            new List<string>() {"M","M2","M3","MM","MM2","MM3", "M/S","VA", "KVA","MVA","KW", "MW","W","%", "KUS",

                "AR", "BALENI", "CM", "CM2", "DM2", "HA", "HOD", "KG", "KM", "M2/KUS", "LITR", "PRM", "S",  "SH", "T", 

                "L", "KG/M3", "L/S","°C", "KS/M2", "KM/H","KM/HOD"};



        #endregion

        #region Constructors

        /// <summary>

        /// Konštruktor. Privátny, čiže táto trieda sa používa ako singleton

        /// </summary>

        private ZoznamMJ()
        {

        }

        #endregion

        #region Properties

        /// <summary>

        /// Inštancia singletona

        /// </summary>

        public static ZoznamMJ Instance
        {

            get
            {

                if (_instance == null)
                {

                    _instance = new ZoznamMJ();

                }



                return _instance;

            }

        }



        #endregion

        #region Interfaces



        /// <summary>

        /// Zistí či je zadané slovo mernou jednotkou.

        /// </summary>

        /// <param name="iWord">Slovo, ktoré sa zisťuje.</param>

        /// <returns>Vráti true ak sa iWord nachádza v zozname merných jednotiek.</returns>

        public bool IsMernaJednotka(string iWord)
        {

            return _merneJednotky.Contains(iWord.ToUpper());

        }





        /// <summary>

        /// Zistí či sa jedná o číslo s mernou jednotkou?

        /// </summary>

        /// <param name="iWord">Kontrolované slovo.</param>

        /// <returns>Jedná sa o číslo s mernou jednotkou?</returns>

        public bool IsNumberWithMernaJednotka(string iWord)
        {

            bool ret = false;



            var withoutDigit = iWord.Replace(" ", "").RemoveStartDigits();



            if (iWord.Length > withoutDigit.Length)
            {
                ret = this.IsMernaJednotka(withoutDigit);
            }
            return ret;
        }

        /// <summary>
        /// Aby sa dalo sobjektom pracovať ako s IEnumerable.
        /// </summary>
        /// <returns>Vráti enumerator.</returns>
        public IEnumerator<string> GetEnumerator()
        {
            return _merneJednotky.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _merneJednotky.GetEnumerator();
        }
        #endregion
    }

}
