﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelAndTools.Tools.External
{
    /// <summary>
    /// Extension pre Char.
    /// </summary>
    public static class CharExtension
    {

        #region Constants

        public const string WORD_SEPARATOR = @"&# ;:.,+-=-'""(){}<>[]…„“/\|*×÷ß½`_";
        public const string NUMBER_SEPARATOR = "., :/";
        public const string NUMBER_CHAR = "%";
        public const string WORD_JOINTERS = @"&#*+,-./:;\_`|­·×ß½–’…";

        #endregion


        #region Methods

        /// <summary>
        /// Zistí, či je kontrolovaný znak považovaný za separátor.
        /// </summary>
        /// <param name="iValue">Kontrolovaný znak.</param>
        /// <returns>Vráti true, ak je kontrolovaný znak spearátor. Ak nie je, vráti false.</returns>
        public static bool IsSeparator(this char iValue)
        {
            bool ret = false;

            if (Char.IsSeparator(iValue))
            {
                ret = true;
            }
            else
            {
                ret = WORD_SEPARATOR.Contains(iValue);
            }

            return ret;
        }


        /// <summary>
        /// Zistí, či je kontrolovaný znak považovaný za spojovník.
        /// </summary>
        /// <param name="iValue">Kontrolovaný znak.</param>
        /// <returns>Vráti true, ak je kontrolovaný znak spojovník. Ak nie je, vráti false.</returns>
        public static bool IsJointer(this char iValue)
        {
            return WORD_JOINTERS.Contains(iValue);
        }


        /// <summary>
        /// Zistí, či je aktuálny znak z hľadiska kontextu čislo.
        /// </summary>
        /// <param name="iValue">Aktuálny znak.</param>
        /// <param name="iPreviewChars">Predchádzajúce znaky.</param>
        /// <param name="iNextChar">Nasledujúci znak.</param>
        /// <returns>Vráti true, ak je aktuálny znak súčasťou čísla. Ak nie, vráti false.</returns>
        public static bool IsNumbers(this char iValue,
                                       string iPreviewChars,
                                         char iNextChar)
        {
            bool ret = false;

            if (Char.IsDigit(iValue) || NUMBER_SEPARATOR.Contains(iValue))
            {
                if ((iPreviewChars.IsNumber() && Char.IsDigit(iNextChar)) ||
                    (iPreviewChars.IsNumber() && NUMBER_CHAR.Contains(iNextChar)))
                {
                    ret = true;
                }
            }

            return ret;
        }

        #endregion

    }
}