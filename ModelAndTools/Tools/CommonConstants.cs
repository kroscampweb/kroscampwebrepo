﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace ModelAndTools.Tools
{
    /// <summary>
    /// Class for storing global constants.
    /// </summary>
    public class CommonConstants
    {
        #region Constants

        // Cesty
#if(Dominik)
        public const string CONNECTION_STRING = @"data source=VIRUS;initial catalog=AmazingDatabase;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
        private const string PATH_TO_SYNONYMS = @"C:\Users\kloski\Documents\Visual Studio 2013\Projects\InternetoveStavebniny\Server\App_Data\Synonyma.xml";
#endif

#if(Tomas)
        private const string PATH_TO_SYNONYMS = @"D:\kros\kroscampwebrepo\Server\App_Data\synonyma.xml";
        public const string CONNECTION_STRING = @"data source=TOMAS-NOTEBOOK\TOMASDB;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
#endif

        // Konštanty
        public const string PROVIDER_NAME = "System.Data.SqlClient";
        public const string CODE_PATTERN = @"[A-Za-z0-9]+\.?[A-Z0-9]*\-?\/?[A-Za-z0-9]*";

        public const char WORD_IDS_SEPARATOR = 'Đ';
        public const char COMMA_SIGN = ',';
        public const char DOT_SIGN = '.';
        public const char WEB_SEPARATOR = 'đ';
        
        public static readonly short[] REGISTER_NAVIGATION_RANGE =   {1,2,3};
        public static readonly RegisterEnum REGISTER_NAVIGATION_CODE_INFO = new RegisterEnum(REGISTER_NAVIGATION_RANGE[0], "Register kódov položiek, vyskytujúcich sa vo všetkých navigáciach.");
        public static readonly RegisterEnum REGISTER_NAVIGATION_WORD_INFO = new RegisterEnum(REGISTER_NAVIGATION_RANGE[1], "Register slov položiek, vyskytujúcich sa vo všetkých navigáciach.");
        public static readonly RegisterEnum REGISTER_NAVIGATION_BASEWORD_INFO = new RegisterEnum(REGISTER_NAVIGATION_RANGE[2], "Register slovotvorných základov položiek, vyskytujúcich sa vo všetkých navigáciach.");

        public static readonly short[] REGISTER_ITEM_NAVIGATION_RANGE =  { 4, 5, 6 };
        public static readonly RegisterEnum REGISTER_ITEM_NAVIGATION_CODE_INFO = new RegisterEnum(REGISTER_ITEM_NAVIGATION_RANGE[0], "Register kódov položiek, vyskytujúcich sa v konkrétnej navigácii.");
        public static readonly RegisterEnum REGISTER_ITEM_NAVIGATION_WORD_INFO = new RegisterEnum(REGISTER_ITEM_NAVIGATION_RANGE[1], "Register slov položiek, vyskytujúcich sa v konkrétnej navigácii.");
        public static readonly RegisterEnum REGISTER_ITEM_NAVIGATION_BASEWORD_INFO = new RegisterEnum(REGISTER_ITEM_NAVIGATION_RANGE[2], "Register slovotvorných základov položiek, vyskytujúcich sa v konkrétnej navigácii.");

        #endregion

        #region Private fields

        private static List<List<string>> _synonyms;

        #endregion

        /// <summary>
        /// Property for List of synonyms.
        /// </summary>
        public static List<List<string>> Synonyms
        {
            get
            {
                if (_synonyms == null)
                {
                    _synonyms = InitializeSynonyms();
                }
                return _synonyms;
            }
        }

        #region Static methods

        /// <summary>
        /// Initializes list of synonyms.
        /// </summary>
        private static List<List<string>> InitializeSynonyms()
        {
            XElement doc = XElement.Load(PATH_TO_SYNONYMS);
            List<List<string>> synonyms = new List<List<string>>();
            foreach (var category in doc.Elements())
            {
                List<string> words = new List<string>();
                foreach (var word in category.Elements())
                {
                    foreach (var splitWord in word.Value.GetWords())
                    {
                        words.Add(splitWord);
                    }
                }
                synonyms.Add(words);
            }
            return synonyms;
        }

        #endregion

        public class RegisterEnum
        {
            private short _type;
            private string _description;
            public RegisterEnum(short type, string description)
            {
                _type = type;
                _description = description;
            }

            public short Type
            {
                get
                {
                    return _type;
                }
            }

            public string Description
            {
                get
                {
                    return _description;
                }
            }
        }
    }
}