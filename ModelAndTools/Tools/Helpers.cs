﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ModelAndTools.Tools.External;

namespace ModelAndTools.Tools
{
    /// <summary>
    /// Helper methods.
    /// </summary>
    public static class Helpers
    {
        #region Constants

        public const string DIRECTORY = "c:\\InternetoveStavebniny";
        public const string PATH_TO_ERROR_FILE = DIRECTORY + "\\ErrorLog.txt";
        public const string PATH_TO_SERVER_LOG_FILE = DIRECTORY + "\\ServerMeasureLog.txt";
        public const string PATH_TO_KLIENT_LOG_FILE = DIRECTORY + "\\KlientMeasureLog.txt";

        #endregion

        #region Static methods

        /// <summary>
        /// Logs error to file.
        /// </summary>
        /// <param name="ErrorMessage">Message of the error</param>
        public static void LogError(string ErrorMessage)
        {
            if (!Directory.Exists(DIRECTORY))
            {
                Directory.CreateDirectory(DIRECTORY);
            }
            using (StreamWriter w = File.AppendText(PATH_TO_ERROR_FILE))
            {
                w.WriteLine(DateTime.Now + ": " + ErrorMessage);
            }
        }

        /// <summary>
        /// Logs results of measurement to file.
        /// </summary>
        /// <param name="LogMessage">Results of measurement</param>
        /// <param name="origin">Identificator of sender</param>
        public static void LogMeasurement(string LogMessage, int origin)
        {
            bool exists = System.IO.Directory.Exists(DIRECTORY);

            if (!exists)
                System.IO.Directory.CreateDirectory(DIRECTORY);
            string path;
            switch (origin)
            {
                case 1:
                    path = PATH_TO_SERVER_LOG_FILE;
                    break;
                case 2:
                    path = PATH_TO_KLIENT_LOG_FILE;
                    break;
                default:
                    path = PATH_TO_ERROR_FILE;
                    break;
            }
            using (StreamWriter w = File.AppendText(path))
            {
                w.WriteLine(LogMessage);
            }
        }

        /// <summary>
        /// Extension to string to split sentence into words.
        /// </summary>
        /// <param name="sequence">Sentence we want to split into words</param>
        /// <returns>List of strings</returns>
        public static List<string> GetWords(this string sequence)
        {

            List<string> words = sequence.ToWords(ZoznamMJ.Instance);
            List<string> helpWords = new List<string>();
            foreach (string word in words)
            {
                helpWords.AddRange(Regex.Split(word, @"(?<=\d)(?=\p{L})|(?<=\p{L})(?=\d)|[\-+/]+"));
            }
            return helpWords.Distinct().ToList();
        }

        /// <summary>
        /// Encodes URI so that it can be used in URL.
        /// </summary>
        /// <param name="value">string that needs to be encoded</param>
        /// <returns>encoded string</returns>
        public static string EncodeURI(this string value)
        {
            return System.Uri.EscapeDataString(value).Replace(".", "%2E").Replace("%", "æ");
        }

        /// <summary>
        /// Decodes encoded URI.
        /// </summary>
        /// <param name="value">string that needs to be decoded</param>
        /// <returns>decoded string</returns>
        public static string DecodeURI(this string value)
        {
            return System.Uri.UnescapeDataString(value.Replace("æ", "%")).Replace("%2E", ".");
        }

        /// <summary>
        /// Gets array of integers from string of integers separed by CommonConstants.COMMA_SIGN.
        /// </summary>
        /// <param name="stringIntegers">String of integers separed by CommonConstants.COMMA_SIGN</param>
        /// <returns>Array of integers</returns>
        public static int[] GetIntegersFromStringArrayOfIntegers(string stringIntegers)
        {
            string[] numbers = stringIntegers.Split(CommonConstants.COMMA_SIGN);
            return Array.ConvertAll<string, int>(numbers, int.Parse);
        }

        #endregion
    }
}