﻿using System;
using ModelAndTools.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;

namespace ServerTest.Tools
{
    [TestClass]
    public class HelpersTest
    {
        [TestMethod]
        public void GetWordsEmptyStringTest()
        {
            var emptyArray = "".GetWords();
            Assert.AreEqual(emptyArray.Count, 0);
        }

        [TestMethod]
        public void GetWordsWordTest()
        {
            var wordArray = "nazdár".GetWords();
            Assert.AreEqual(wordArray.Count, 1);
        }

        [TestMethod]
        public void GetWordsTwoWordsTest()
        {
            var wordArray = "ahoj sveT".GetWords();
            Assert.AreEqual(wordArray.Count, 2);
        }

        [TestMethod]
        public void GetWordsSlashTest()
        {
            var slash1 = "14/100".GetWords();
            var slash2 = "14 /100".GetWords();
            var slash3 = "14 / 100".GetWords();
            var slash4 = "14/ 100".GetWords();
            CollectionAssert.AreEqual(slash3, slash2);
            CollectionAssert.AreEqual(slash3, slash4);
            CollectionAssert.AreEqual(slash1, slash2);
        }

        [TestMethod]
        public void GetWordsDashTest()
        {
            var dash1 = "10-40 mm".GetWords();
            var dash2 = "10-40mm".GetWords();
            var dash3 = "10 - 40mm".GetWords();
            var dash4 = "10- 40 mm".GetWords();
            var dash5 = "10 -40mm".GetWords();
            CollectionAssert.AreEqual(dash3, dash2);
            CollectionAssert.AreEqual(dash3, dash4);
            CollectionAssert.AreEqual(dash1, dash2);
            CollectionAssert.AreEqual(dash5, dash4);
        }

        [TestMethod]
        public void GetWords_x_Test()
        {
            var x1 = "100x500x60mm".GetWords();
            var x2 = "100x500x60 mm".GetWords();
            var x4 = "100x500 60mm".GetWords();
            CollectionAssert.AreEqual(x2, x4);
            CollectionAssert.AreEqual(x1, x2);
        }

        [TestMethod]
        public void GetWords_plus_Test()
        {
            var x1 = "akryl+butyl+cement".GetWords();
            var x2 = "akryl butyl cement".GetWords();
            CollectionAssert.AreEqual(x1, x2);
        }

        [TestMethod]
        public void GetWords_ml_Test()
        {
            var x1 = "750ml".GetWords();
            var x2 = "750 ml".GetWords();
            CollectionAssert.AreEqual(x1, x2);
        }

        [TestMethod]
        public void GetWords_kg_ks_Test()
        {
            var x1 = "3,84kg/ks".GetWords();
            var x2 = "3,84 kg/ks".GetWords();
            var x3 = "3,84 kg ks".GetWords();
            Assert.IsTrue(x1.Count >= x2.Count);
            CollectionAssert.AreEqual(x3, x2);
        }

        [TestMethod]
        public void GetWordsXTest()
        {
            var x1 = "100X100kg".GetWords();
            var x2 = "100 X 100 kg".GetWords();
            CollectionAssert.AreEqual(x1, x2);
        }

        [TestMethod]
        public void EncodeDecodeTest()
        {
            string s = "Ahoj svet";
            string sEncode = s.EncodeURI();
            string sDecode = sEncode.DecodeURI();
            Assert.AreEqual(s, sDecode);
            s = @"ahoj/ako\sa-máš?XxX!(celkom)[dobre].%ťažko<>123 456 789 - * # | 'ahoj'";
            sEncode = s.EncodeURI();
            sDecode = sEncode.DecodeURI();
            Assert.AreEqual(s, sDecode);
        }
    }
}
