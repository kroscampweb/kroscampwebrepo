﻿using ModelAndTools.Models;
using System;
using System.Linq;
namespace RegisterBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            new NavigationBuilder().BuildAndSaveAllPriceLists();

            Console.WriteLine("Tap to continue.");
            Console.Read();
        }
    }
}
