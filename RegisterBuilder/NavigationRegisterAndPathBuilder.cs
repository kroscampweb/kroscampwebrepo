﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using ModelAndTools.Tools.External;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RegisterBuilder
{
    /// <summary>
    /// Builds registers from navigations.
    /// </summary>
    class NavigationRegisterAndPathBuilder : AbstractRegisterBuilder, IBuilder
    {
        #region Constants

        private const short INITIAL_VALUE_FOR_EXTENSIONWORDS = 60;
        private const int ITEMS_COUNT = 100000;

        #endregion

        #region Private fields

        Dictionary<int, List<string>> parentsNames = new Dictionary<int, List<string>>();

        #endregion

        #region Constructors

        public NavigationRegisterAndPathBuilder()
            : base(ITEMS_COUNT) { }

        #endregion

        #region Public methods

        public void BuildAndSavePriceList(int priceListId)
        {
            string distributorName;
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                context.LazyLoading = true;
                distributorName = context.PriceListRepository.GetObjectByID(priceListId).Distributor.Name;
                var navigations = context.NavigationRepository.GetObjectsByExpression(navigation => navigation.IdPriceList == priceListId).OrderBy(navigation => navigation.Level);
                int navigationsCount = navigations.Count();
                int processedItems = 0;

                foreach (Navigation navigation in navigations)
                {
                    context.NavigationRepository.Attach(navigation);

                    SetPathToNavigation(navigation);
                    processedItems += SetItemRegistersToNavigationAndEnrichNavigationRegisters(context, navigation);

                    if (processedItems > 375)
                    {
                        context.SaveChangesAndRecreateContext(true);
                        processedItems = 0;
                    }
                }
                context.SaveChanges();
            }
            SaveNavigationRegistersToDB(priceListId);
            Console.WriteLine("Building registers and paths of '" + distributorName + "' complete.");
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Set path to navigation.
        /// </summary>
        /// <param name="navigation">Navigation</param>
        private void SetPathToNavigation(Navigation navigation)
        {
            if (navigation.Level > 0)
            {
                if (navigation.Level == 1)
                {
                    navigation.NavigationPath = navigation.Parent.ToString();
                }
                else
                {
                    navigation.NavigationPath = navigation.Navigation2.NavigationPath + CommonConstants.COMMA_SIGN.ToString() + navigation.Parent.ToString();
                }
            }
        }

        /// <summary>
        /// Set item registers to navigation and add values to navigation registers dictionary.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="navigation">Navigation</param>
        /// <returns>Number of processed items</returns>
        private int SetItemRegistersToNavigationAndEnrichNavigationRegisters(AmazingDataModelUnit context, Navigation navigation)
        {
            int processedItems = 0;
            AddToParentNames(navigation);
            if (navigation.Items.Any())
            {
                var items = navigation.Items;
                ItemNavigationRegisters itemRegisters = new ItemNavigationRegisters(items.Count);
                foreach (Item item in items)
                {
                    // Adding item identificators to item registers
                    itemRegisters.AddIdToDictionaryRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_CODE_INFO.Type, item.Id, item.Code);
                    // Adding item identificators to navigation registers
                    AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_CODE_INFO.Type, navigation.Id, item.Code);

                    foreach (string word in item.Name.GetWords())
                    {
                        // for item registers
                        itemRegisters.AddIdToDictionaryRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_WORD_INFO.Type, item.Id, word);
                        itemRegisters.AddIdToDictionaryRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_CODE_INFO.Type, item.Id, word.ToWordRoot());
                        // to navigation registers
                        AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_WORD_INFO.Type, navigation.Id, word);
                        AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_BASEWORD_INFO.Type, navigation.Id, word.ToWordRoot());
                    }

                    foreach (string word in parentsNames[navigation.Id])
                    {
                        // Adding item extensions to item word register
                        itemRegisters.AddIdToDictionaryRegister(CommonConstants.REGISTER_ITEM_NAVIGATION_WORD_INFO.Type, item.Id, word);
                        // Adding item extensions to navigation word register
                        AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_WORD_INFO.Type, navigation.Id, word);
                    }
                    processedItems++;
                }
                navigation.Registers = itemRegisters.GetAddedRegistersOfNavigation(context, navigation);
            }
            return processedItems;
        }

        private void AddToParentNames(Navigation navigation)
        {
            parentsNames[navigation.Id] = navigation.Name.GetWords();
            if (navigation.Parent != null && parentsNames.ContainsKey((int)navigation.Parent))
            {
                parentsNames[navigation.Id].AddRange(parentsNames[(int)navigation.Parent]);
            }
        }

        /// <summary>
        /// Returns words from names of navigation, parent navigations and distributor.
        /// </summary>
        /// <param name="navigation">Navigation</param>
        /// <returns>List of words</returns>
        private List<string> GetExtensionWordsForNavigation(Navigation navigation)
        {
            List<string> extensionWords = new List<string>(INITIAL_VALUE_FOR_EXTENSIONWORDS);
            extensionWords.AddRange(parentsNames[navigation.Id]);
            //extensionWords.AddRange(navigation.Name.GetWords());

            //Navigation parent = navigation.Navigation2;
            //while (parent != null) // adding navigations and their parents to registers
            //{
            //    List<string> words = parent.Name.GetWords();
            //    extensionWords.AddRange(words);
            //    foreach (string word in words)
            //    {
            //        AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_WORD_INFO.Type, navigation.Id, word);
            //    }
            //    parent = parent.Navigation2;
            //}

            //List<string> words1 = navigation.PriceList.Distributor.Name.GetWords();
            //extensionWords.AddRange(words1);
            //foreach (string word in words1) // adding distributor to registers
            //{
            //    AddIdToDictionaryRegister(CommonConstants.REGISTER_NAVIGATION_WORD_INFO.Type, navigation.Id, word);
            //}
            return extensionWords;
        }

        /// <summary>
        /// Method save or update database registers.
        /// </summary>
        /// <param name="context">Database context</param>
        private void SaveNavigationRegistersToDB(int priceListID)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                //for (short typeOfRegister = CommonConstants.REGISTER_NAVIGATION_CODE_INFO.Type; typeOfRegister <= CommonConstants.REGISTER_NAVIGATION_BASEWORD_INFO.Type; typeOfRegister++)
                foreach (short typeOfRegister in CommonConstants.REGISTER_NAVIGATION_RANGE)
                {
                    Register register;

                    var registers = context.RegisterRepository.GetObjectsByExpression(r => r.Type == typeOfRegister && r.PriceListId == priceListID);
                    if (!registers.Any()) //If register isn't in DB yet
                    {
                        register = new Register() { Data = RegisterToText(typeOfRegister), Type = typeOfRegister, PriceListId = priceListID };
                        context.RegisterRepository.Add(register);
                    }
                    else // If register already is in database, just update
                    {
                        register = registers.First();
                        register.Data = RegisterToText(typeOfRegister);
                    }
                }
                context.SaveChanges();
            }
        }
        #endregion

        #region Not implemented

        public void BuildAndSaveAllPriceLists()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
