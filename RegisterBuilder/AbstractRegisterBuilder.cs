﻿using ModelAndTools.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RegisterBuilder
{
    /// <summary>
    /// Abstract class for register builders.
    /// </summary>
    public abstract class AbstractRegisterBuilder
    {
        #region Constants

        public const short NUMBER_OF_REGISTERS = 3;
        private const short CHARACTER_COUNT_OF_REGISTER = 250;
        private const short CODE_REGISTER_TYPE = 1;
        private const short WORD_REGISTER_TYPE = 2;
        private const short BASEWORD_REGISTER_TYPE = 0; // 3 % NUMBER_OF_REGISTERS == 0

        #endregion

        #region Private fields

        private int _itemsCount;
        protected Dictionary<String, List<int>> _codeRegister;
        protected Dictionary<String, List<int>> _wordRegister;
        protected Dictionary<String, List<int>> _baseWordRegister;

        #endregion

        #region Constructors

        protected AbstractRegisterBuilder(int itemsCount)
        {
            _itemsCount = itemsCount;
            _codeRegister = new Dictionary<string, List<int>>(itemsCount);
            _wordRegister = new Dictionary<string, List<int>>(itemsCount / 2);
            _baseWordRegister = new Dictionary<string, List<int>>(itemsCount / 3);
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Make text representation from register.
        /// </summary>
        /// <param name="typeOfRegister">Type of register</param>
        /// <returns>Text representation of register</returns>
        protected string RegisterToText(short typeOfRegister)
        {
            Dictionary<String, List<int>> dictionaryRegister = GetRegister(typeOfRegister);
            StringBuilder text = new StringBuilder(dictionaryRegister.Count * CHARACTER_COUNT_OF_REGISTER);

            foreach (var key in dictionaryRegister.Keys)
            {
                StringBuilder values = null;
                foreach (int value in dictionaryRegister[key].Distinct())
                {
                    if (values == null)
                    {
                        values = new StringBuilder(CHARACTER_COUNT_OF_REGISTER);
                        values.Append(value);
                    }
                    else
                    {
                        values.Append(CommonConstants.COMMA_SIGN.ToString() + value);
                    }
                }
                text.Append(key + CommonConstants.WORD_IDS_SEPARATOR.ToString() + values + System.Environment.NewLine);
            }
            return text.ToString();
        }

        /// <summary>
        /// Add id to word in register.
        /// </summary>
        /// <param name="typeOfRegister">Type of register</param>
        /// <param name="elementId">Id of added element</param>
        /// <param name="word">Word in dictionary</param>
        public void AddIdToDictionaryRegister(short typeOfRegister, int elementId, string word)
        {

            Dictionary<String, List<int>> register = GetRegister(typeOfRegister);

            if (!register.ContainsKey(word))
            {
                List<int> set;
                if (typeOfRegister % 3 != 1 && _itemsCount > 2000)
                {
                    set = new List<int>(_itemsCount / 300);
                }
                else
                {
                    set = new List<int>();
                }
                set.Add(elementId);
                register.Add(word, set);
            }
            else
            {
                register[word].Add(elementId);
            }
        }

        /// <summary>
        /// Returns registers via type.
        /// </summary>
        /// <param name="typeOfRegister">Type of register</param>
        /// <returns>Register(Dictionary)</returns>
        protected Dictionary<string, List<int>> GetRegister(short typeOfRegister)
        {
            switch (typeOfRegister % NUMBER_OF_REGISTERS)
            {
                case CODE_REGISTER_TYPE:
                    return _codeRegister;
                case WORD_REGISTER_TYPE:
                    return _wordRegister;
                case BASEWORD_REGISTER_TYPE:
                    return _baseWordRegister;
                default:
                    Helpers.LogError("Type of register not supported (" + typeOfRegister + ").");
                    return null;
            }
        }

        #endregion
    }
}
