﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System.Collections.Generic;
using System.Linq;

namespace RegisterBuilder
{
    /// <summary>
    /// Saves or updates item registers to navigations.
    /// </summary>
    class ItemNavigationRegisters : AbstractRegisterBuilder
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public ItemNavigationRegisters(int itemCount)
            : base(itemCount) { }

        #endregion

        #region Public methods

        /// <summary>
        /// Get setted registers of navigation.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="navigation">Navigation</param>
        /// <returns></returns>
        public Register[] GetAddedRegistersOfNavigation(AmazingDataModelUnit context, Navigation navigation)
        {
            Register[] threeRegisters = new Register[CommonConstants.REGISTER_ITEM_NAVIGATION_RANGE.Length];
            //for (short i = 0; i <= CommonConstants.TYPE_REGISTER_ITEMNAVIGATION_BASEWORD - CommonConstants.TYPE_REGISTER_ITEMNAVIGATION_CODE; i++)//  short i = CommonConstants.TYPE_REGISTER_ITEMNAVIGATION_CODE; i <= CommonConstants.TYPE_REGISTER_ITEMNAVIGATION_BASEWORD; i++)
            foreach (short i in CommonConstants.REGISTER_ITEM_NAVIGATION_RANGE)
            {
                threeRegisters[i % CommonConstants.REGISTER_ITEM_NAVIGATION_RANGE.Length] = AddRegisterToNavigation(context, i, navigation.Id);
            }
            return threeRegisters;
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Get setted one register of navigation.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="typeOfRegister">Type of register</param>
        /// <param name="navigationId">Id of navigation</param>
        /// <returns></returns>
        private Register AddRegisterToNavigation(AmazingDataModelUnit context, short typeOfRegister, int navigationId)
        {
            Register register;

            if (!context.RegisterRepository.GetObjectsByExpression(r => r.Type == typeOfRegister && r.NavigationId == navigationId).Any()) //If register isn't in DB yet
            {
                register = new Register() { Data = RegisterToText(typeOfRegister), Type = typeOfRegister };
                context.RegisterRepository.Add(register);
            }
            else // If register already is in database, just update
            {
                register = (Register)context.RegisterRepository.GetObjectsByExpression(r => r.Type == typeOfRegister && r.NavigationId == navigationId).First();
                register.Data = RegisterToText(typeOfRegister);
            }
            return register;
        }

        #endregion
    }
}
