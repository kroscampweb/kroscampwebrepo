﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RegisterBuilder
{
    /// <summary>
    /// Adding descedants Ids and registers of items to navigations.
    /// </summary>
    class NavigationDescedantsBuilder : IBuilder
    {
        #region Constants

        private const short INITIAL_VALUE_FOR_LIST_OF_CHILDREN = 60;
        private const short NUMBER_OF_CHARACTERS_IN_CODE = 20;

        #endregion

        #region Constructors

        public NavigationDescedantsBuilder() { }

        #endregion

        #region Public methods

        public void BuildAndSavePriceList(int priceListId)
        {
            Dictionary<int, IEnumerable<int>> navigationsDescendants;
            string priceListName;
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                priceListName = context.PriceListRepository.GetObjectByID(priceListId).Name;
                context.LazyLoading = true;
                var navigations = context.NavigationRepository.GetObjectsByExpression(navigation => navigation.IdPriceList == priceListId).OrderByDescending(navigation => navigation.Level);
                int navigationsCount = navigations.Count();
                navigationsDescendants = new Dictionary<int, IEnumerable<int>>(navigationsCount);
                int i = 0;
                foreach (Navigation navigation in navigations)
                {
                    context.NavigationRepository.Attach(navigation);
                    if (navigation.Navigation1.Any())
                    {
                        AddDescendantsToNavigation(navigation, ref navigationsDescendants);
                    }
                    if (++i % 100 == 0)
                    {
                        Console.Write("\r" + (int)(i * 100 / navigationsCount) + "% navigation descedants of '" + priceListName + "' complete.");
                    }
                }
                context.SaveChanges();
                Console.WriteLine("\rAll navigation descedants of '" + priceListName + "' complete.");
            }
            Console.WriteLine("Start saving descedants to database.");
            SaveDescedandsToDB(navigationsDescendants);
            Console.WriteLine("Ading descendants to '" + priceListName + "' navigations complete.");
        }

        /// <summary>
        /// Set ids of descendants and direct descedants of items to navigations.
        /// </summary>
        public void BuildAndSaveAllPriceLists()
        {
            Console.WriteLine("Started adding descendants to all distributors.");
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var priceListIds = context.PriceListRepository.GetAllObjects().Select(pl => pl.Id);
                foreach (int plID in priceListIds)
                {
                    BuildAndSavePriceList(plID);
                }
            }
            Console.WriteLine("Descedants for all distributors complete.");
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Add descedants to navigations.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="navigation">Navigations</param>
        private void AddDescendantsToNavigation(Navigation navigation, ref  Dictionary<int, IEnumerable<int>> navigationsDescendants)
        {
            List<int> navigationWithChildren = new List<int>(INITIAL_VALUE_FOR_LIST_OF_CHILDREN);
            navigation.DirectDescedantIds = "";
            foreach (Navigation nav in navigation.Navigation1)
            {
                navigationWithChildren.Add(nav.Id);
                navigation.DirectDescedantIds += nav.Id.ToString() + CommonConstants.COMMA_SIGN;
                if (navigationsDescendants.ContainsKey(nav.Id))
                {
                    navigationWithChildren.AddRange(navigationsDescendants[nav.Id]);
                }
            }
            navigation.DirectDescedantIds = navigation.DirectDescedantIds.Remove(navigation.DirectDescedantIds.Length - 1);
            navigationsDescendants.Add(navigation.Id, navigationWithChildren.Distinct());
        }

        /// <summary>
        /// Method saves descedants to database.
        /// </summary>
        private void SaveDescedandsToDB(Dictionary<int, IEnumerable<int>> navigationsDescendants)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                foreach (int key in navigationsDescendants.Keys)
                {
                    Navigation navigation = (Navigation)context.NavigationRepository.GetObjectByID(key);


                    StringBuilder values = null;
                    foreach (int value in navigationsDescendants[key])
                    {
                        if (values == null)
                        {
                            values = new StringBuilder(navigationsDescendants[key].Count() * NUMBER_OF_CHARACTERS_IN_CODE);
                            values.Append(value);
                        }
                        else
                        {
                            values.Append(CommonConstants.COMMA_SIGN.ToString() + value);
                        }
                    }
                    if (values != null)
                    {
                        navigation.AllChildrenIds = values.ToString();
                    }
                }
                context.SaveChanges();
            }
        }
        #endregion

    }
}
