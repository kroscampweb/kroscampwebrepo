﻿using ModelAndTools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegisterBuilder
{
    /// <summary>
    /// Class builds registers, descedants and paths for navigations.
    /// </summary>
    class NavigationBuilder : IBuilder
    {

        private static Semaphore _pool;

        #region Public methods

        /// <summary>
        /// Build structures for all price lists.
        /// </summary>
        public void BuildAndSaveAllPriceLists()
        {
            Console.WriteLine("Building started.");
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var priceListIds = context.PriceListRepository.GetAllObjects().Select(pl => pl.Id);
                var priceListsCount = priceListIds.Count();
                _pool = new Semaphore(0, priceListsCount);
                _pool.Release(priceListsCount);

                List<Thread> threads = new List<Thread>();
                foreach (int plID in priceListIds)
                {
                    Thread t = new Thread(delegate()
                    {
                        _pool.WaitOne();
                        BuildAndSavePriceList(plID);
                        _pool.Release();
                    });
                    t.Start();
                }
                for (int i = 0; i < priceListsCount; i++)
                {
                    _pool.WaitOne();
                }
            }
            Console.WriteLine("Building complete");
        }

        /// <summary>
        /// Build structures for price list.
        /// </summary>
        /// <param name="priceListId">Id of price list</param>
        public void BuildAndSavePriceList(int priceListId)
        {
            new NavigationRegisterAndPathBuilder().BuildAndSavePriceList(priceListId);
            new NavigationDescedantsBuilder().BuildAndSavePriceList(priceListId);
        }

        #endregion
    }
}
