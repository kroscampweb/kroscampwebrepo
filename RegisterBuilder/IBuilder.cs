﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegisterBuilder
{
    interface IBuilder
    {
        void BuildAndSaveAllPriceLists();
        void BuildAndSavePriceList(int priceListId);
    }
}
