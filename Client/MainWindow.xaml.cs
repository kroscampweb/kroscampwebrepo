﻿using Klient.Command;
using Klient.HelperClasses;
using Klient.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Klient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        Storyboard storyBoardSuccess = null;

        #region Constructor

        /// <summary>
        /// Initializes the Main Window.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            BindShortcuts();
            Dispatcher.BeginInvoke(new Action(() =>
            {
                FocusFirstItem();
            }));

            storyBoardSuccess = border.FindResource("PlayAnimationSuccess") as Storyboard;



#if Measuring
            Stopwatch watch = App.GetWatch();
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("\nKlient finished starting at " + watch.ElapsedMilliseconds + " ms", 2);
#endif
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Adds handling od keypress.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">RoutedEventArgs</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window.KeyDown += HandleKeyPress;
        }

        /// <summary>
        /// Handles keypress
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">KeyEventArgs</param>
        private void HandleKeyPress(object sender, KeyEventArgs e)
        {
            if (!searchBox.IsFocused)
            {
                string pressed = e.Key.ToString();
                if (pressed.Length < 3 && char.IsLetterOrDigit(pressed, 0))
                {
                    searchBox.Focus();
                    searchBox.Text = "";
                }
            }
        }

        /// <summary>
        /// Subscribes to Event SearchFinish.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MetroWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                (e.NewValue as TreeViewModel).OnSearchFinish += MainWindow_OnSearchFinish;
            }

        }

        /// <summary>
        /// Handles event SearchFinish.
        /// </summary>
        void MainWindow_OnSearchFinish()
        {
            FocusFirstItem();

            TreeViewModel model = this.DataContext as TreeViewModel;
            if (!model.IsEmpty && GetDataFromServer.SearchedSequence != null)
            {
                storyBoardSuccess.Begin();
            }

            //storyBoard.Begin();
        }

        /// <summary>
        /// Focuses first item of the tree.
        /// </summary>
        private void FocusFirstItem()
        {
            if (Strom.Items.Count > 0)
            {
                Strom.Focus();
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (Strom.Items.Count > 0)
                    {

                        var treeviewItem = Strom.ItemContainerGenerator.ContainerFromItem(Strom.Items[0]) as TreeViewItem;
                        if (treeviewItem != null)
                        {
                            treeviewItem.Focus();
                        }
                    }

                }));
            }
        }

        /// <summary>
        /// Binds shortcuts that concern purely View matter.
        /// </summary>
        private void BindShortcuts()
        {
            InputBindings.Add(new KeyBinding()
            {
                Command = FindCommand,
                Key = Key.F,
                Modifiers = ModifierKeys.Control
            });
            InputBindings.Add(new KeyBinding()
            {
                Command = InfoCommand,
                Key = Key.I,
                Modifiers = ModifierKeys.Control
            });
        }

        #endregion

        #region Commands

        private ICommand _findCommand;

        /// <summary>
        /// Focuses searchbox.
        /// </summary>
        public ICommand FindCommand
        {
            get
            {
                if (_findCommand == null)
                {
                    _findCommand = new RelayCommand((a) =>
                    {
                        searchBox.Focus();
                    }, (a) => { return true; });
                }

                return _findCommand;
            }
        }

        private ICommand _infoCommand;

        /// <summary>
        /// Expands InfoPanel
        /// </summary>
        public ICommand InfoCommand
        {
            get
            {
                if (_infoCommand == null)
                {
                    _infoCommand = new RelayCommand((a) =>
                    {
                        Overview.IsExpanded = !Overview.IsExpanded;
                    }, (a) => { return true; });
                }

                return _infoCommand;
            }
        }

        #endregion
    }
}
