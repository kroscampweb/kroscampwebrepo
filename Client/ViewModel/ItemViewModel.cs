﻿using ModelAndTools.Models.DataModels;
using System.Globalization;

namespace Klient.ViewModel
{
    /// <summary>
    /// ViewModel which is responsible for items.
    /// </summary>
    public class ItemViewModel : ViewModelBase
    {
        private Item _item;

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="item">Item we use to create this ItemViewModel</param>
        public ItemViewModel(Item item)
        {
            _item = item;
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Property, which exposes Item object to public.
        /// </summary>
        public Item Item
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }

        /// <summary>
        /// Checks if Item has a picture, either returns it or a default one.
        /// </summary>
        public string Picture
        {
            get
            {
                if(_item.ImgUrl==null || _item.ImgUrl.Equals(""))
                {
                    return "Resources/Icons/itemDefault.png";
                }
                else
                {
                    return _item.ImgUrl;
                }
            }
        }

        /// <summary>
        /// Returns price of Item aint with currency symbol.
        /// </summary>
        public string Price
        {
            get
            {
                return "" + Item.Price +" €";
            }
        }

        /// <summary>
        /// Informs, that this object isn't a Distributor.
        /// </summary>
        public bool IsDistributor
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Informs, that this object is an Item.
        /// </summary>
        public bool IsItem
        {
            get
            {
                return true;
            }
        }

        #endregion
    }
}
