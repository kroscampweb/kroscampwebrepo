﻿using Klient.HelperClasses;
using Newtonsoft.Json.Linq;
using ModelAndTools;
using ModelAndTools.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

namespace Klient.ViewModel
{
    /// <summary>
    /// ViewModel which is responsible for Distributors (and their pricelists).
    /// </summary>
    public class DistributorViewModel : ViewModelBase
    {
        #region Private fields

        private List<NavigationViewModel> _navigations;
        private PriceListDistributor _priceListDistributor;
        private bool _isLoaded;
        private bool _isExpanded;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pld">PriceListDistributor we use to create this ViewModel</param>
        public DistributorViewModel(PriceListDistributor pld)
        {
            InitializeProperties(pld);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Property which exposes List of Navigations to public.
        /// </summary>
        public List<NavigationViewModel> Navigations
        {
            get
            {
                return _navigations;
            }
            set
            {
                _navigations = value;

            }
        }

        /// <summary>
        /// Property which exposes PriceListDistributor object to public.
        /// </summary>
        public PriceListDistributor PriceListDistributor
        {
            get
            {
                return _priceListDistributor;
            }
            set
            {
                _priceListDistributor = value;
                OnPropertyChanged("PriceListDistributor");
            }
        }

        /// <summary>
        /// Is called when distributor is expanded and download data if needed.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                if (value == true && !_isLoaded)
                {
#if Measuring
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
#endif

                    Navigations = GetDataFromServer.GetNavigationsOfPriceList(this._priceListDistributor.PriceListId);

                    OnPropertyChanged("navigations");
                    _isLoaded = true;

#if Measuring
                    watch.Stop();
                    Server.Tools.Helpers.LogMeasurement("\nKlient finished expanding tree at " + watch.ElapsedMilliseconds + " ms", 2);
#endif
                }
            }
        }

        /// <summary>
        /// Informs, that this object is a Distributor.
        /// </summary>
        public bool IsDistributor
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Informs, that this object isn't an Item.
        /// </summary>
        public bool IsItem
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Initializes properties of the instance.
        /// </summary>
        /// <param name="pld">PriceListDistributor we use to create this ViewModel</param>
        private void InitializeProperties(PriceListDistributor pld)
        {
            _priceListDistributor = pld;
            if (_priceListDistributor.Logo == null || _priceListDistributor.Logo.Equals(""))
            {
                _priceListDistributor.Logo = "Icons/distributorDefault.png";
            }

            Navigations = new List<NavigationViewModel>();
            if (pld != null && pld.Navigations != null)
            {
                if (pld.Navigations.Count > 0)
                {
                    pld.Navigations.ForEach(nav => Navigations.Add(new NavigationViewModel(nav)));
                }
                _isLoaded = true;
            }
            else
            {
                Navigations.Add(new NavigationViewModel(null));
                _isLoaded = false;
            }
        }

        #endregion
    }
}
