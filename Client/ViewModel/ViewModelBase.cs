﻿using System.ComponentModel;

namespace Klient.ViewModel
{
    /// <summary>
    /// Basic ViewModel from which other ViewModels inherit OnPropertyChanged.
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region Public methods

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Warns GUI that property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property that changed</param>
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
