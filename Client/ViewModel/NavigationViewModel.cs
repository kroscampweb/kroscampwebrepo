﻿using Klient.HelperClasses;
using ModelAndTools.Models.DataModels;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Data;

namespace Klient.ViewModel
{
    /// <summary>
    /// ViewModel which is responsible for Navigations and their children.
    /// </summary>
    public class NavigationViewModel : ViewModelBase
    {
        #region Private fields

        private List<ItemViewModel> _items;
        private List<NavigationViewModel> _navNavigations;
        private bool _isExpanded;
        private bool _isLoaded;
        private Navigation _navigation;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="navigation">Navigation we use to create this NavigationViewModel</param>
        public NavigationViewModel(Navigation navigation)
        {
            InitializeProperties(navigation);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Exposes List of child Items.
        /// </summary>
        public List<ItemViewModel> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged("Items");
            }
        }

        /// <summary>
        /// Exposes Navigation object to public.
        /// </summary>
        public Navigation Navigation
        {
            get
            {
                return _navigation;
            }
            set
            {
                _navigation = value;
                OnPropertyChanged("Navigation");
            }
        }

        /// <summary>
        /// Returns if the navigation should be shown in bold depending on level.
        /// </summary>
        public string NavigationBold
        {
            get
            {
                if(_navigation.Level==0)
                {
                    return "ExtraBold";
                }
                else
                {
                    return "Normal";
                }
            }
        }

        /// <summary>
        /// Exposes List of child Navigations to public.
        /// </summary>
        public List<NavigationViewModel> ChildNavigations
        {
            get
            {
                return _navNavigations;
            }
            set
            {
                _navNavigations = value;
                OnPropertyChanged("childNavigations");
            }
        }

        /// <summary>
        /// Returns collection of all children of this Navigation.
        /// </summary>
        public object GetChildren
        {
            get
            {
                CompositeCollection children = new CompositeCollection();
                if (ChildNavigations != null)
                    ChildNavigations.ForEach(child => children.Add(child));
                if (Items != null)
                    Items.ForEach(child => children.Add(child));
                return children;
            }
        }

        /// <summary>
        /// Is called when navigation is expanded and download data if needed
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                if (value == true && !_isLoaded)
                {
#if Measuring
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
#endif

                    ChildNavigations = GetDataFromServer.GetChildNavigations(this._navigation.Id);
                    Items = GetDataFromServer.GetItems(this._navigation.Id);

                    OnPropertyChanged("GetChildren");
                    _isLoaded = true;

#if Measuring
                    watch.Stop();
                    Server.Tools.Helpers.LogMeasurement("\nKlient finished expanding tree at " + watch.ElapsedMilliseconds + " ms", 2);
#endif
                }
            }
        }

        /// <summary>
        /// Informs, that this object isn't a Distributor.
        /// </summary>
        public bool IsDistributor
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Informs, that this object isn't an Item.
        /// </summary>
        public bool IsItem
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Initializes properties of the instance.
        /// </summary>
        /// <param name="navigation">Navigation we use to create this NavigationViewModel</param>
        private void InitializeProperties(Navigation navigation)
        {
            _navigation = navigation;

            _isLoaded = false;
            ChildNavigations = new List<NavigationViewModel>();
            Items = new List<ItemViewModel>();
            if (navigation != null)
            {
                if (navigation.Navigation1 != null && navigation.Navigation1.Count > 0)
                {
                    foreach (Navigation nav in navigation.Navigation1)
                    {
                        NavigationViewModel navi = new NavigationViewModel(nav);
                        ChildNavigations.Add(navi);
                    }
                    _isLoaded = true;
                }
                if (navigation.Items != null && navigation.Items.Count > 0)
                {
                    foreach (Item item in navigation.Items)
                    {
                        Items.Add(new ItemViewModel(item));
                    }
                    _isLoaded = true;
                }
                if (_isLoaded == false)
                {
                    ChildNavigations.Add(new NavigationViewModel(null));
                }
            }
        }

        #endregion
    }
}
