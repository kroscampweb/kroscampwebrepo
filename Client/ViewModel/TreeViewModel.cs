﻿using Klient.Command;
using Klient.HelperClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using System.Windows.Media;

namespace Klient.ViewModel
{
    /// <summary>
    /// Delegate, which helps inform the View that Search is finished.
    /// </summary>
    public delegate void SearchFinishHandler();

    /// <summary>
    /// ViewModel which is given to the TreeView
    /// </summary>
    public class TreeViewModel : ViewModelBase
    {
        #region Private fields

        private bool _isSearching=false;
        private bool _isEmpty = false;
        private string _searchColor;
        private ObservableCollection<DistributorViewModel> _distributors;

        /// <summary>
        /// Event that informs the View that Search is finished.
        /// </summary>
        public event SearchFinishHandler OnSearchFinish;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public TreeViewModel()
        {
            Distributors = GetDefaultDistributors();
        }

        #endregion

        #region Public properties

        public string SearchColor
        {
            get { return _searchColor; }
            set
            {
                _searchColor = value;
                OnPropertyChanged("SearchColor");
            }
        }

        /// <summary>
        /// Property, which reveals if there is an active search.
        /// </summary>
        public bool IsSearching
        {
            get { return _isSearching; }
            set { 
                _isSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }

        /// <summary>
        /// Informs, if search returned empty result.
        /// </summary>
        public bool IsEmpty
        {
            get { return _isEmpty; }
            set { 
                _isEmpty = value;
                OnPropertyChanged("IsEmpty");
            }
        }

        /// <summary>
        /// Exposes collection of Distributors to public.
        /// </summary>
        public ObservableCollection<DistributorViewModel> Distributors
        {
            get
            {
                return _distributors;
            }
            set
            {
                _distributors = value;
                OnPropertyChanged("Distributors");
            }
        }

        private ICommand _searchCommand;

        /// <summary>
        /// Command responsible for searching based on parameter.
        /// </summary>
        public ICommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new RelayCommand((a) =>
                    {
                        IsSearching = true;
                        Distributors = new ObservableCollection<DistributorViewModel>(GetDataFromServer.SearchPriceListDistributorsAndBaseNavigations(a.ToString()));
                        IsSearching = false;
                        IsEmpty = Distributors.Count == 0;
                        SearchColor = IsEmpty == false ? "green" : "red";

                        if (OnSearchFinish != null)
                        {
                            OnSearchFinish();
                        }
                    }, (a) => { return a != null && !string.IsNullOrWhiteSpace(a.ToString()) && a.ToString().Length > 2; });
                }
                return _searchCommand;
            }
        }

        private ICommand _homeCommand;

        /// <summary>
        /// Returns tree to default value.
        /// </summary>
        public ICommand HomeCommand
        {
            get
            {
                if (_homeCommand == null)
                {
                    _homeCommand = new RelayCommand((a) =>
                    {
                        SearchColor = "white";
                        IsEmpty = false;
                        Distributors = GetDefaultDistributors();

                        if (OnSearchFinish != null)
                        {
                            OnSearchFinish();
                        }
                    }, (a) => { return true; });
                }

                return _homeCommand;
            }
        }

        private ICommand _navigateCommand;

        /// <summary>
        /// Opens link to an item in browser.
        /// </summary>
        public ICommand NavigateCommand
        {
            get
            {
                if (_navigateCommand == null)
                {
                    _navigateCommand = new RelayCommand((a) =>
                    {
                        Process.Start(new ProcessStartInfo(a.ToString()));
                    }, (a) => { return true; });
                }

                return _navigateCommand;
            }
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Retrieves Collection of default Distributors.
        /// </summary>
        /// <returns>ObservableCollection of Distributors</returns>
        private ObservableCollection<DistributorViewModel> GetDefaultDistributors()
        {
            return new ObservableCollection<DistributorViewModel>(GetDataFromServer.GetDistributors());
        }

        #endregion
    }
}
