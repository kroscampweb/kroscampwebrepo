﻿using System;
using System.Diagnostics;
using System.Windows;

namespace Klient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
#if Measuring
        private static Stopwatch watch;
        private const string message = "Experimentálny vyhľadávač - klient";
#endif

        protected override void OnStartup(StartupEventArgs e)
        {
#if Measuring
            watch = new Stopwatch();
            watch.Start();
#endif

            base.OnStartup(e);

#if Measuring
            Server.Tools.Helpers.LogMeasurement("\nKlient started - " + message + " o " + DateTime.Now,2);
#endif
        }

        protected override void OnExit(ExitEventArgs e)
        {
#if Measuring
            Server.Tools.Helpers.LogMeasurement("Klient stopped - " + message + " o " + DateTime.Now,2);
#endif
            base.OnExit(e);
        }

#if Measuring
        public static Stopwatch GetWatch()
        {
            return watch;
        }

        
#endif
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
        }
    }
}
