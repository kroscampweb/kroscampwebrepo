﻿using Klient.ViewModel;
using Newtonsoft.Json.Linq;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using ModelAndTools.Tools;
using System.Diagnostics;

namespace Klient.HelperClasses
{
    /// <summary>
    /// Static class responsible for connectiong to server and getting data from it.
    /// </summary>
    static class GetDataFromServer
    {
        #region Constants

        /// <summary>
        /// URL address of server
        /// </summary>
        public const string URL = "http://localhost:1832";

        #endregion

        #region Private fields

        private static string _searchedSequence = null; 

        #endregion

        public static string SearchedSequence
        {
            get
            {
                return _searchedSequence;
            }
        }

        #region Static methods

        /// <summary>
        /// Loads Distributors from server.
        /// </summary>
        /// <returns>List of DistribitorViewModel</returns>
        public static List<DistributorViewModel> GetDistributors()
        {
            _searchedSequence = null;
            List<DistributorViewModel> distributors = new List<DistributorViewModel>();

#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            var data = GetJArrayFromServer("/api/distributors");

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient got result in " + watch.ElapsedMilliseconds + " ms", 2);
            watch.Restart();
#endif

            foreach (var obj in data)
            {
                PriceListDistributor priceListDistributor = obj.ToObject<PriceListDistributor>();
                DistributorViewModel dist = new DistributorViewModel(priceListDistributor);
                dist.Navigations.Add(new NavigationViewModel(null));
                distributors.Add(dist);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient processed results in " + watch.ElapsedMilliseconds + " ms\n", 2);
#endif

            return distributors;
        }

        /// <summary>
        /// Loads base navigations of Pricelist.
        /// </summary>
        /// <param name="IdPriceList">Id of PriceList</param>
        /// <returns>List of NavigationViewModel</returns>
        public static List<NavigationViewModel> GetNavigationsOfPriceList(int IdPriceList)
        {
            List<NavigationViewModel> navigations = new List<NavigationViewModel>();

#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            var data = GetJArrayFromServer("/api/navigations/GetBaseNavigationOfPriceList/" + IdPriceList);

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient got result in " + watch.ElapsedMilliseconds + " ms", 2);
            watch.Restart();
#endif

            foreach (var obj in data)
            {
                Navigation navigation = obj.ToObject<Navigation>();
                NavigationViewModel nav = new NavigationViewModel(navigation);
                nav.ChildNavigations.Add(new NavigationViewModel(null));
                navigations.Add(nav);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient processed results in " + watch.ElapsedMilliseconds + " ms\n", 2);
#endif

            return navigations;
        }

        /// <summary>
        /// Loads child navigations of navigation with set Id.
        /// </summary>
        /// <param name="IdNavigation">Id of parent navigation</param>
        /// <returns>List of NavigationViewModel</returns>
        public static List<NavigationViewModel> GetChildNavigations(int IdNavigation)
        {
            List<NavigationViewModel> childNavigations = new List<NavigationViewModel>();

#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            JArray data;
            if (_searchedSequence == null)
            {
                data = GetJArrayFromServer("/api/navigations/GetChildrenOfNavigation/" + IdNavigation);
            }
            else
            {
                data = GetJArrayFromServer("/api/Search/GetChildNavigationsOfnavigation/" + _searchedSequence + CommonConstants.WEB_SEPARATOR + IdNavigation);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient got result in " + watch.ElapsedMilliseconds + " ms", 2);
            watch.Restart();
#endif

            foreach (var obj in data)
            {
                Navigation navigation = obj.ToObject<Navigation>();
                NavigationViewModel nav = new NavigationViewModel(navigation);
                nav.ChildNavigations.Add(new NavigationViewModel(null));
                childNavigations.Add(nav);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient processed results in " + watch.ElapsedMilliseconds + " ms\n", 2);
#endif

            return childNavigations;
        }

        /// <summary>
        /// Loads items of navigation with chosen Id from server.
        /// </summary>
        /// <param name="IdNavigation">Id of parent navigation</param>
        /// <returns>List of ItemViewModel</returns>
        public static List<ItemViewModel> GetItems(int IdNavigation)
        {
            List<ItemViewModel> items = new List<ItemViewModel>();

#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            JArray data;
            if (_searchedSequence == null)
            {
                data = GetJArrayFromServer("/api/items/SetItemsOfNavigation/" + IdNavigation);
            }
            else
            {
                data = GetJArrayFromServer("/api/Search/GetItemsOfNavigation/" + _searchedSequence + CommonConstants.WEB_SEPARATOR + IdNavigation);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient got result in " + watch.ElapsedMilliseconds + " ms", 2);
            watch.Restart();
#endif

            foreach (var obj in data)
            {
                var x = obj["navigations"];
                if (obj["Code"] != null && obj["Name"] != null)
                {
                    Item i = obj.ToObject<Item>();
                    ItemViewModel item = new ItemViewModel(i);
                    items.Add(item);
                }
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient processed results in " + watch.ElapsedMilliseconds + " ms\n", 2);
#endif

            return items;
        }

        /// <summary>
        /// Sends search query to server and loads result.
        /// </summary>
        /// <param name="param">Text of search query</param>
        /// <returns>List of DistributorViewModel that fit the query</returns>
        public static List<DistributorViewModel> SearchPriceListDistributorsAndBaseNavigations(string param)
        {
            _searchedSequence = param.EncodeURI();
            List<DistributorViewModel> distributors = new List<DistributorViewModel>();

#if(Measuring)
            Stopwatch watch = new Stopwatch();
            watch.Start();
#endif

            var search = GetJArrayFromServer("/api/Search/GetDistributorsAndBaseNavigations/" + _searchedSequence);

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient got result for " + param + " in " + watch.ElapsedMilliseconds + " ms", 2);
            watch.Restart();
#endif

            foreach (var obj in search)
            {
                PriceListDistributor i = obj.ToObject<PriceListDistributor>();
                DistributorViewModel item = new DistributorViewModel(i);
                distributors.Add(item);
            }

#if(Measuring)
            watch.Stop();
            Server.Tools.Helpers.LogMeasurement("Experimental klient processed results for " + param + " in " + watch.ElapsedMilliseconds + " ms\n", 2);
#endif

            return distributors;
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Helper method which connects to server and gets data from it.
        /// </summary>
        /// <param name="url">URL</param>
        /// <returns>JArray with results of the query</returns>
        private static JArray GetJArrayFromServer(string url)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                var jsonDistributors = client.DownloadString(URL + url);
                if (jsonDistributors.Equals("null"))
                {
                    return JArray.Parse("[]");
                }
                return JArray.Parse(jsonDistributors);
            }
        }

        #endregion
    }
}
