﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.IO;
using System.Linq;

namespace FileDataMiner
{
    /// <summary>
    /// Delete all data from price list.
    /// </summary>
    class PriceListHelpClass
    {

        /// <summary>
        /// Starting FileDataminer application.
        /// </summary>
        class Program
        {
            static void Main(string[] args)
            {
                //Console.WriteLine("Načítať zoznam cenníkov? [y/n]");
                //if (Console.Read() == 121)
                //{
                //    CreateDistributorsAndPriceLists();
                //}
                //Console.ReadLine();
                Console.WriteLine("Pridať/zmeniť dáta nejakého cenníka? [y/n]");

                while (Console.Read() == 121)
                {
                    Console.ReadLine();
                    Console.WriteLine("Zadaj cestu k súboru:");
                    string pathToFile = Console.ReadLine();
                    Console.WriteLine("Zadaj kód cenníka:");
                    string code = Console.ReadLine().Trim();

                    if (pathToFile[0].Equals('\"') && pathToFile[pathToFile.Length - 1].Equals('\"'))
                    {
                        pathToFile = pathToFile.Substring(1, pathToFile.Length - 2);
                    }

                    PriceList priceList = null;
                    using (AmazingDataModelUnit context = new AmazingDataModelUnit())
                    {
                        priceList = context.PriceListRepository.GetObjectsByExpression(pl => pl.Code.Equals(code)).FirstOrDefault();
                    }
                    var eraser = new PriceListHelpClass();
                    //eraser.DeleteAllFromPriceList(priceList);

                    DataMiner getMiner = new DataMiner();
                    IDataMiner dataMiner = null;
                    try
                    {
                        dataMiner = getMiner.GetDataMiner(pathToFile);
                    }
                    catch (FileNotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    if (dataMiner != null)
                    {
                        dataMiner.Execute(pathToFile, priceList);
                        WriteResult(dataMiner.IsExecutedCorrectly(pathToFile, priceList), priceList);
                    }
                    else
                    {
                        Console.WriteLine("Dolováč dát nenájdený.");
                    }
                    Console.WriteLine("\nChcete načítavať ďalší cenník?  [y/n]");
                }
                Console.WriteLine("Pre pokračovanie stlačte klávesu.");
                Console.Read();
            }

            private static void WriteResult(bool result, PriceList priceList)
            {
                if (result)
                {
                    Console.WriteLine("Cenník '" + priceList.Name + "' je úspešne aktualizovaný.");
                }
                else
                {
                    Console.WriteLine("Cenník '" + priceList.Name + "' je úspešne aktualizovaný, ale je možné, že nejaké položky chýbajú.");
                }
            }
        }

        /// <summary>
        /// Delete all data from price list.
        /// </summary>
        /// <param name="priceListId">Price list ID</param>
        public void DeleteAllFromPriceList(PriceList priceList)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                Console.WriteLine("Vymazávanie všetkých prvkov cenníka '" + priceList.Name + "' z databázy.");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.ItemNavigation where NavigationId in (Select Id from dbo.Navigation where IdPriceList = " + priceList.Id + ")");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.ItemNavigation where NavigationId in (Select Id from dbo.Navigation where IdPriceList = " + priceList.Id + ")");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Register where NavigationId in (Select Id from dbo.Navigation where IdPriceList = " + priceList.Id + ")");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Navigation where IdPriceList = " + priceList.Id);
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Item where Id not in (select ItemId from dbo.ItemNavigation)");
                Console.WriteLine("Prvky z cenníka '" + priceList.Name + "' boli vymazané.");
            }
        }

        private static void CreateDistributorsAndPriceLists()
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.ItemNavigation");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.ItemNavigation");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Register");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Navigation");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Item");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.PriceList");
                ((AmazingDataModel)context.Context).Database.ExecuteSqlCommand("DELETE from dbo.Distributor");

                Distributor distributor = new Distributor() { Name = "Estavmat", Contact = "Názov firmy: CA-STAV s.r.o.\ntel.číslo: 0907 654 259\nEmail: info@estavmat.sk\nweb: www.estavmat.sk", Location = "Dolné Saliby 537", Logo = "http://im9.cz/sk/iR/importobchod-orig/12297_logo--mm130x40.png" };
                PriceList priceList = new PriceList() { Name = "STAVEBNINY Estavmat.sk", Content = "stavebný materiál", TransportCost = "individuálne podľa miesta dodania, charakteru a množstva materiálu", Updates = "2014", Distributor = distributor, Code = "S02" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "HAGARD:HAL", Contact = "Názov firmy: HAGARD:HAL, spol. s r.o.\ntel.číslo: : +421 37 7913000\nEmail: : hagard@hagard.sk\nweb: http://www.hagard.sk", Location = "Pražská 9, 949 11 Nitra", Logo = "http://www.hagard.sk/buxus/images/design/logo.gif" };
                priceList = new PriceList() { Name = "Cenník elektroinštalačného materiálu a svietidiel", Content = "zhodný s e-shopom HAGARD:HAL", TransportCost = "N/A", Updates = "týždenne", Distributor = distributor, Code = "S16" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Solidstav", Logo = "http://www.solidstav.sk/vyrobna/photos/original/logo_solidstav_farba_94bdc8f5af1ef84a8b0c39ef792ce2de.png" };
                priceList = new PriceList() { Name = "Solidstav", Content = "Solidstav", Code = "S08", Distributor = distributor };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Stavekon", Contact = "Názov firmy: STAVEKON s.r.o. tel.číslo: +421 903 020 966, +421 41 7001245 Email: stavekon@stavekon.sk web: www.webstavba24.sk, www.stavekon.sk", Logo = "http://webstavba24.sk/media/logo-stavekon-na-webstavbu.png" };
                priceList = new PriceList() { Name = "Verejný cenník Stavekon 2014", Content = "stavebná chémia SIKA", TransportCost = "N/A", Updates = "kvartálne intervaly", Distributor = distributor, Code = "S07" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Lacnestavanie", Contact = "Názov firmy: Lacné stavanie s.r.o.\ntel.číslo: infolinka 0917 969 003\nEmail: info@lacnestavanie.sk\nweb: www.lacnestavanie.sk", Location = "Sídlo: Považská Bystrica", Logo = "http://www.pesiazona.sk/logos/13821.jpg" };
                priceList = new PriceList() { Name = "LS – online", Content = "online stavebniny Lacné Stavanie s.r.o.", TransportCost = "doprava zdarma v rámci celej SR", Distributor = distributor, Code = "S04" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Nej stavebniny", Logo = "http://www.nejstavebniny.cz/img/design/logo.png" };
                priceList = new PriceList() { Name = "NejStavebniny", Code = "output", Distributor = distributor };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Tripark", Contact = "Názov firmy: Tripark s.r.o.\ntel.číslo: 056 6442994\nEmail: info@tripark.sk\nweb: www.tripark.sk", Location = "Stavbárov 6, 071 01 Michalovce", Logo = "http://www.tripark.sk/pic/logo.gif" };
                priceList = new PriceList() { Name = "Tripark - Stavomat", Content = "stavebniny", TransportCost = "od € 3,50", Distributor = distributor, Code = "S05" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Raven", Logo = "http://www.raven.sk/img/raven.logo.png" };
                priceList = new PriceList() { Name = "Raven", Content = "Raven", TransportCost = "dohodou", Updates = "1.7.2014", Distributor = distributor, Code = "S11" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "STAVIVÁ Pavlík a Solárik", Contact = "Názov firmy: STAVIVÁ Pavlík a Solárik, spol. s r.o.\ntel.číslo: : 0903 504 330\nEmail: stavivasp@gmail.com\nweb: www.eshop.stavivasp.sk", Location = "Kratinova 71, 036 01  Martin", Logo = "http://eshop.stavivasp.sk/images/staviva.png" };
                priceList = new PriceList() { Name = "STAVIVA SP", Content = "stavebný materiál", TransportCost = "dohodou", Updates = "1 items za 2 týždne", Distributor = distributor, Code = "S13" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Verex", Contact = "Názov firmy: VEREX-ELTO, a.s.\ntel.číslo: +421 44 547 47 11\nEmail: info@verexelto.sk\nweb: www.verexelto.sk", Location = "Priemyselná 22; 03101 Liptovský Mikuláš; Slovensko", Logo = "http://www.legrand.sk/userfiles/image/predajne/kosicky-kraj/logo_verex-elto.jpg" };
                priceList = new PriceList() { Name = "VEREX-ELTO Základný cenník", Content = "svietidlá a elektroinštalačný materiál", TransportCost = "dohodou", Updates = "týždenne", Distributor = distributor, Code = "S03" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);

                distributor = new Distributor() { Name = "Cenekon", Logo = "http://www.svf.tuke.sk/orsp2012/images/cenekon%20logo%20kopie.png" };
                priceList = new PriceList() { Name = "Cenekon", Distributor = distributor, Code = "Cenekon" };
                distributor.PriceLists.Add(priceList);
                context.DistributorRepository.Add(distributor);
                context.PriceListRepository.Add(priceList);
                context.SaveChanges();
            }
        }
    }
}
