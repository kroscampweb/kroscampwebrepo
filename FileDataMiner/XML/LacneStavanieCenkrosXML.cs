﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelAndTools.Tools;
using System.Xml.Linq;
using ModelAndTools.Tools.External;

namespace FileDataMiner
{
    /// <summary>
    /// Dataminer for Lacne Stavanie, format xml file
    /// </summary>
    class LacneStavanieCenkrosXML : IDataMiner
    {
        #region Private fields

        private PriceList _priceList;
        private List<Navigation> _navigations;
        private AmazingDataModelUnit _context;
        private XElement _xmlFile;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public LacneStavanieCenkrosXML()
        {
            _context = new AmazingDataModelUnit();
            _navigations = new List<Navigation>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Mine data from file and update database.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        public void Execute(string pathToFile, PriceList priceList)
        {
            _xmlFile = XElement.Load(pathToFile);
            _priceList = priceList;
            _context.PriceListRepository.Attach(_priceList);
            LoadNavigations();
            LoadItems();
            SaveLoaded();
        }

        /// <summary>
        /// Check if data was correctly updated.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        /// <returns>true if update was succesfull else false</returns>
        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            Console.WriteLine("Reading data from File");
            XElement xmlFile = XElement.Load(pathToFile);
            int FileItemCount = (from element in xmlFile.Descendants("Item") select element).Count();
            var FileItems = (from element in xmlFile.Descendants("Item") select element).ToList();
            var WrongNavigations = (from element in xmlFile.Descendants("Navigation") select element).Where(nav => nav.Attribute("description").Value == null || nav.Attribute("description").Value.Equals("")).ToList();
            int wrongItems = 0;
            foreach (XElement navigation in WrongNavigations)
            {
                foreach (XElement item in navigation.Descendants("Item"))
                {
                    wrongItems++;
                }
            }

            Console.WriteLine("Reading data from Database");
            _context = new AmazingDataModelUnit();
            IEnumerable<int> DBNavigationIds = _context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceList.Id).Select(nav => nav.Id);
            int DBItemCount = _context.ItemRepository.GetObjectsByExpression(item => DBNavigationIds.Contains(item.Navigations.FirstOrDefault().Id)).Count();



            _context.Dispose();
            if (FileItemCount-wrongItems == DBItemCount)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Load navigations from file.
        /// </summary>
        private void LoadNavigations()
        {
            IEnumerable<XElement> navigationPaths = from element in _xmlFile.Descendants("Navigation") select element;
            foreach (XElement navigationPath in navigationPaths)
            {
                var navigations = navigationPath.FirstAttribute.Value.SplitBySeparator("/");

                for (short index = 0; index < navigations.Count; index++)
                {
                    Navigation nav = new Navigation() { Name = navigations[index], PriceList = _priceList, Level = index };
                    Navigation parent = new Navigation();
                    if (index > 0)
                    {
                        List<string> path = new List<string>();
                        for (int i = 0; i < index; i++)
                        {
                            path.Add(navigations[i]);
                        }
                        parent = GetNavigation(path);
                        nav.Navigation2 = parent;
                    }
                    if (!NavigationExists(nav, index))
                    {
                        if (index == 0)
                        {
                            _priceList.Navigations.Add(nav);
                        }
                        else
                        {
                            parent.Navigation1.Add(nav);
                        }
                        _navigations.Add(nav);
                    }
                }
            }
        }

        /// <summary>
        /// Load items from file.
        /// </summary>
        private void LoadItems()
        {
            IEnumerable<XElement> navigationPaths = from element in _xmlFile.Descendants("Navigation") select element;
            foreach (XElement items in navigationPaths)
            {
                if (items.FirstAttribute.Value.SplitBySeparator("/").ToList().Count == 0)
                {
                    continue;
                }
                foreach (XElement item in items.Descendants("Item"))
                {

                    Item itm = new Item();
                    itm.Code = item.Attribute("Code").Value;
                    itm.Name = item.Attribute("Name").Value;
                    itm.Description = item.Attribute("Description").Value;
                    itm.Order = int.Parse(item.Attribute("Code").Value);
                    Navigation parent = GetNavigation(items.FirstAttribute.Value.SplitBySeparator("/").ToList());
                    itm.Navigations.Add(parent);
                    itm.Price = decimal.Parse(item.Attribute("Price").Value.Replace(" €", "").ReplaceWithCorrectDecimalSeparator());
                    itm.Unit = item.Attribute("Unit").Value;
                    itm.Url = item.Attribute("Url").Value;
                    itm.ImgUrl = item.Attribute("ImgUrl").Value;
                    parent.Items.Add(itm);
                }
            }
        }

        /// <summary>
        /// Save loaded navigations and items to database.
        /// </summary>
        private void SaveLoaded()
        {
            _context.SaveChanges();
            _context.Dispose();
        }

        /// <summary>
        /// Check if navigation exists.
        /// </summary>
        /// <param name="navigation">navigation</param>
        /// <param name="level">level of the navigation</param>
        /// <returns>true if exists</returns>
        private bool NavigationExists(Navigation navigation, int level)
        {
            var navigations = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigation.Name));
            if (navigations.Count() == 0)
            {
                return false;
            }
            else
            {
                if (level == 0)
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Name.Equals(nav.Name) && nav.Level == 0)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Navigation2 == nav.Navigation2)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Get navigation from navigation path.
        /// </summary>
        /// <param name="navigations">navigation path</param>
        /// <returns>navigation</returns>
        private Navigation GetNavigation(List<string> navigations)
        {
            Navigation result;
            result = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigations[0]) && nav.Level == 0).First();
            for (int i = 1; i < navigations.Count(); i++)
            {
                result = result.Navigation1.Where<Navigation>(nav => nav.Name.Equals(navigations[i])).First();
            }

            return result;
        }

        #endregion
    }
}
