﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace FileDataMiner
{
    /// <summary>
    /// Dataminer for Solidstav and Stavekon, format xml file.
    /// </summary>
    public class KrosFormat : IDataMiner
    {
        #region Private fields

        private string _pathToFile;
        private PriceList _priceList;
        private AmazingDataModelUnit _context;
        private int _order = 0;
        private int _priceListId;

        #endregion

        #region Public methods

        /// <summary>
        /// Mine data from file and update database.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        public void Execute(string pathToFile, PriceList priceList)
        {
            _priceList = priceList;
            _priceListId = priceList.Id;
            //SaveXmlFormat(pathToFile);
            SaveXmlToDb(_priceListId, pathToFile);
        }

        /// <summary>
        /// Check if data was correctly updated.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        /// <returns>true if update was succesfull else false</returns>
        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            Console.WriteLine("Reading data from File");
            XElement xmlFile = XElement.Load(pathToFile);
            int FileNavigationCount = (from element in xmlFile.Descendants("Navigation") select element).Count();
            int FileItemCount = (from element in xmlFile.Descendants("Item") select element).Where(item => !item.Attribute("Code").Value.Equals("") && item.Attribute("Code").Value != null).Count();


            Console.WriteLine("Reading data from Database");
            CreateContext();
            IEnumerable<int> DBNavigationIds = _context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceList.Id).Select(nav => nav.Id);
            int DBItemCount = _context.ItemRepository.GetObjectsByExpression(item => DBNavigationIds.Contains(item.Navigations.FirstOrDefault().Id)).Count();

            if (FileNavigationCount == DBNavigationIds.Count() && FileItemCount == DBItemCount)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Private helpers

        //private void SaveXmlFormat(string file)
        //{
        //    KrosFormat xfmt = new KrosFormat();
        //    xfmt.SaveXmlToDb(_priceListId, file);
        //}

        /// <summary>
        /// Save items to DB.
        /// </summary>
        /// <param name="priceListId">PriceList ID.</param>
        private void SaveXmlToDb(int priceListId, string filePath)
        {
            _pathToFile = filePath;
            CreateContext();
            _priceListId = priceListId;
            this._priceList = _context.PriceListRepository.GetObjectByID(_priceListId);
            Console.WriteLine("Reading File");
            XmlDocument doc = new XmlDocument();
            doc.Load(_pathToFile);
            Console.WriteLine("File loaded");

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                Navigation nav = new Navigation() { Name = node.Attributes["Description"].InnerText, Level = 0, PriceList = this._priceList };
                _priceList.Navigations.Add(nav);
                Work(node, nav);
                SaveChangesToDBAndRecreateContext();
                Console.WriteLine("Hotovo: node - " + node.Attributes["Description"].InnerText);
            }
            SaveChangesToDBAndRecreateContext();

            Console.WriteLine("Vymazávanie prázdnych navigácií.");
            var emptyNavigations = _context.NavigationRepository.GetObjectsByExpression(nav => !nav.Navigation1.Any() && !nav.Items.Any());
            foreach (Navigation emptyNavigation in emptyNavigations)
            {
                _context.NavigationRepository.Remove(emptyNavigation);
            }
            _context.SaveChanges();

            _context.Dispose();
            Console.WriteLine("Celé hotovo !");
            Console.Read();
        }

        /// <summary>
        /// Does all the work
        /// </summary>
        /// <param name="parent">Parent node</param>
        /// <param name="parentNav">Parent navigation</param>
        private void Work(XmlNode parent, Navigation parentNav)
        {
            if (parent.HasChildNodes && parent.FirstChild.Name.Equals("Items")) // Spracuj Itemy
            {
                foreach (XmlNode xmlItem in parent.FirstChild.ChildNodes)
                {
                    Item item = new Item();
                    decimal dPrice;
                    string strPrice = xmlItem.Attributes["Price"].InnerText.Replace('.', ',');
                    Decimal.TryParse(strPrice, out dPrice);
                    item.Price = dPrice;
                    item.Code = xmlItem.Attributes["Code"].InnerText;
                    item.Name = xmlItem.Attributes["Name"].InnerText;
                    item.Order = ++_order;
                    string unit = xmlItem.Attributes["Unit"].InnerText;
                    item.Unit = unit.Equals("") ? "n/a" : unit;

                    if (item.Code.Equals(""))
                    {
                        continue;
                    }

                    if (xmlItem.Attributes["Url"] != null)
                    {
                        item.Url = xmlItem.Attributes["Url"].InnerText;
                    }
                    if (xmlItem.Attributes["ImgUrl"] != null)
                    {
                        item.ImgUrl = xmlItem.Attributes["ImgUrl"].InnerText;
                    }
                    if (xmlItem.Attributes["Description"] != null)
                    {
                        item.Description = xmlItem.Attributes["Description"].InnerText;
                    }
                    if (xmlItem.Attributes["Weight"] != null)
                    {
                        Decimal.TryParse(xmlItem.Attributes["Weight"].InnerText, out dPrice);
                        item.Weight = (double)dPrice;
                    }

                    item.Navigations.Add(parentNav);
                    parentNav.Items.Add(item);
                }
            }
            else
                foreach (XmlNode xmlNav in parent.ChildNodes) // Navigacia
                {
                    Navigation nav = new Navigation() { Navigation2 = parentNav, Name = xmlNav.Attributes["Description"].InnerText, Level = Convert.ToInt16(parentNav.Level + 1), PriceList = _priceList };
                    _priceList.Navigations.Add(nav);
                    Work(xmlNav, nav);
                    Console.WriteLine("Hotovo: node - " + xmlNav.Attributes["Description"].InnerText);
                }
        }

        /// <summary>
        /// Saves changes to database and recreates context.
        /// </summary>
        private void SaveChangesToDBAndRecreateContext()
        {
            _context.SaveChangesAndRecreateContext(false);
            this._priceList = _context.PriceListRepository.GetObjectByID(_priceListId);
            _priceListId = _priceList.Id;
        }

        /// <summary>
        /// Creates context.
        /// </summary>
        private void CreateContext()
        {
            this._context = new AmazingDataModelUnit();


            // Lambda na logy do konzoly
            //this.ctx.Database.Log = (p) =>
            //{
            //    Console.WriteLine(p);
            //};
        }

        #endregion
    }
}
