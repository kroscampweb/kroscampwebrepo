﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using ModelAndTools.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ModelAndTools.Tools.External;

namespace FileDataMiner
{
    /// <summary>
    /// Dataminer for Tripark, format xml file.
    /// </summary>
    class TriparkXML : IDataMiner
    {
        #region Private fields

        private PriceList _priceList;
        private List<Navigation> _navigations;
        private XElement _xmlFile;
        private AmazingDataModelUnit _context; 

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public TriparkXML()
        {
            _context = new AmazingDataModelUnit();
            _navigations = new List<Navigation>();
        } 

        #endregion

        #region Public methods

        /// <summary>
        /// Mine data from file and update database.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        public void Execute(string pathToFile, PriceList priceList)
        {
            _xmlFile = XElement.Load(pathToFile);
            _priceList = priceList;
            _context.PriceListRepository.Attach(priceList);
            LoadNavigations();
            LoadItems();
            SaveLoaded();
        }

        /// <summary>
        /// Check if data was correctly updated.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        /// <returns>true if update was succesfull else false</returns>
        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            Console.WriteLine("Reading data from File");
            XElement xmlFile = XElement.Load(pathToFile);
            int FileItemCount = (from element in xmlFile.Descendants("SHOPITEM") select element).Count();


            Console.WriteLine("Reading data from Database");
            _context = new AmazingDataModelUnit();
            IEnumerable<int> DBNavigationIds = _context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceList.Id).Select(nav => nav.Id);
            int DBItemCount = _context.ItemRepository.GetObjectsByExpression(item => DBNavigationIds.Contains(item.Navigations.FirstOrDefault().Id)).Count();
            _context.Dispose();
            if (FileItemCount == DBItemCount)
            {
                return true;
            }
            return false;
        } 

        #endregion

        #region Private helpers

        /// <summary>
        /// Load navigations from file.
        /// </summary>
        private void LoadNavigations()
        {
            IEnumerable<XElement> navigationPaths = from element in _xmlFile.Descendants("CATEGORYTEXT") select element;
            foreach (XElement navigationPath in navigationPaths)
            {
                var navigations = navigationPath.Value.SplitBySeparator(" > ");

                for (short index = 0; index < navigations.Count; index++)
                {
                    Navigation nav = new Navigation() { Name = navigations[index], PriceList = _priceList, Level = index };
                    Navigation parent = new Navigation();
                    if (index > 0)
                    {
                        List<string> path = new List<string>();
                        for (int i = 0; i < index; i++)
                        {
                            path.Add(navigations[i]);
                        }
                        parent = GetNavigation(path);
                        nav.Navigation2 = parent;
                    }
                    if (!NavigationExists(nav, index))
                    {
                        if (index == 0)
                        {
                            _priceList.Navigations.Add(nav);
                        }
                        else
                        {
                            parent.Navigation1.Add(nav);
                        }
                        _navigations.Add(nav);
                    }
                }
            }
        }

        /// <summary>
        /// Load items from file.
        /// </summary>
        private void LoadItems()
        {
            IEnumerable<XElement> items = from element in _xmlFile.Descendants("SHOPITEM") select element;
            foreach (XElement item in items)
            {
                Item itm = new Item();
                itm.Code = item.Descendants("ITEM_ID").First().Value;
                itm.Name = item.Descendants("PRODUCTNAME").First().Value;
                itm.Description = item.Descendants("DESCRIPTION").First().Value;
                itm.Order = int.Parse(item.Descendants("ITEM_ID").First().Value);
                Navigation parent = GetNavigation(item.Descendants("CATEGORYTEXT").First().Value.SplitBySeparator(" > ").ToList());
                itm.Navigations.Add(parent);
                itm.Price = decimal.Parse(item.Descendants("PRICE").First().Value.ReplaceWithCorrectDecimalSeparator());
                itm.VAT = decimal.Parse(item.Descendants("VAT").First().Value.ReplaceWithCorrectDecimalSeparator());
                itm.Unit = item.Descendants("UNIT").First().Value;
                itm.Url = item.Descendants("URL").First().Value;
                itm.ImgUrl = item.Descendants("IMGURL").Count() > 0 ? item.Descendants("IMGURL").First().Value : "";
                parent.Items.Add(itm);
            }
        }

        /// <summary>
        /// Save loaded navigations and items to database.
        /// </summary>
        private void SaveLoaded()
        {
            _context.SaveChanges();
            _context.Dispose();
        }

        /// <summary>
        /// Check if navigation exists.
        /// </summary>
        /// <param name="navigation">navigation</param>
        /// <param name="level">level of the navigation</param>
        /// <returns>true if navigation exists</returns>
        private bool NavigationExists(Navigation navigation, int level)
        {
            var navigations = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigation.Name));
            if (navigations.Count() == 0)
            {
                return false;
            }
            else
            {
                if (level == 0)
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Name.Equals(nav.Name) && nav.Level == 0)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Navigation2 == nav.Navigation2)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Get navigation through navigation path.
        /// </summary>
        /// <param name="navigations">navigation path</param>
        /// <returns>navigation</returns>
        private Navigation GetNavigation(List<string> navigations)
        {
            Navigation result;
            result = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigations[0]) && nav.Level == 0).First();
            for (int i = 1; i < navigations.Count(); i++)
            {
                result = result.Navigation1.Where<Navigation>(nav => nav.Name.Equals(navigations[i])).First();
            }

            return result;
        } 

        #endregion
    }
}
