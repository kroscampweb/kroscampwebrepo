﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace FileDataMiner
{

    /// <summary>
    /// Help class for work with items.
    /// </summary>
    class HelpItem
    {
        public int id;
        public int parent;
        public double price;
    }

    /// <summary>
    /// Mine data from file and update database.
    /// </summary>
    class NejStavebninyXML : IDataMiner
    {
        #region Private fields

        private Dictionary<int, Navigation> _parents = new Dictionary<int, Navigation>();
        private Dictionary<int, List<Navigation>> _navigationsWithoutParents = new Dictionary<int, List<Navigation>>(); // Prvky, kt nemajú rodiča. Ako index je ID rodiča, kt. je zatiaľ neznámy.

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pathToFile"></param>
        public NejStavebninyXML()
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Read and save items and navigations to DB.
        /// </summary>
        /// <param name="priceListId">ID PriceListu</param>
        public void Execute(string pathToFile, PriceList priceList)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                context.PriceListRepository.Attach(priceList);
                if (priceList == null)
                {
                    throw new ArgumentNullException("code", "PriceList sa nenachádza v databáze.");
                }
                IEnumerable<XElement> categoriesAndGoods = XElement.Load(pathToFile).Elements();

                var navigations = categoriesAndGoods.First();
                var items = categoriesAndGoods.Last();
                SaveNavigations(navigations, priceList.Id);
                SaveItems(items, priceList.Id);
            }
        }

        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            IEnumerable<XElement> categoriesAndGoods = XElement.Load(pathToFile).Elements();
            var categories = categoriesAndGoods.First();
            var goods = categoriesAndGoods.Last();
            Dictionary<short, List<int>> levelNavigations = new Dictionary<short, List<int>>();
            levelNavigations.Add(0, (from navig in categories.Elements() where navig.Element("ParentIDS").Value.Equals("0") select Convert.ToInt32(navig.Element("ID").Value)).ToList());

            short actualLevel = 0;
            while (levelNavigations.ContainsKey(actualLevel))
            {
                List<int> navigationIds = new List<int>();
                foreach (int baseId in levelNavigations[actualLevel])
                {
                    navigationIds.AddRange(from navig in categories.Elements() where navig.Element("ParentIDS").Value.Equals(baseId.ToString()) select Convert.ToInt32(navig.Element("ID").Value));
                }
                actualLevel++;
                if (navigationIds.Any())
                {
                    levelNavigations.Add(actualLevel, navigationIds.ToList());
                }
            }

            List<int> allNavigationIds = new List<int>();
            foreach (short level in levelNavigations.Keys)
            {
                allNavigationIds.AddRange(levelNavigations[level]);
            }

#if DEBUG
            var allItems = (from item in goods.Elements() select new { id = Convert.ToInt64(item.Element("ID").Value), parent = Convert.ToInt32(item.Element("CategoryID").Value), price = Convert.ToDouble(item.Element("Price").Value.Replace('.', ',')) }).Distinct();
            var allItemsWithParent = allItems.Where(i => allNavigationIds.Contains(i.parent));
            var xmlItems = allItemsWithParent.Where(i => i.price > 0);

            int xmlNavigationsCount = allItemsWithParent.Where(i => i.price == 0).Count() + allNavigationIds.Count;
            int xmlItemsCount = xmlItems.Count();
            int dbNavigationCount;
            int dbItemsCount = GetAllItemIdsItems(GetAllItemsFromDB(priceList.Id)).Count;
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                IEnumerable<Navigation> navigations = context.NavigationRepository.GetObjectsByExpression(navigation => navigation.IdPriceList == priceList.Id);
                dbNavigationCount = navigations.Count();
            }
#endif

            List<int> dbItemIds = GetAllItemIdsItems(GetAllItemsFromDB(priceList.Id));
            var xmlItemsNotInDb = GetItemsNotInDb(goods, allNavigationIds, dbItemIds);
            return !xmlItemsNotInDb.Any();
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Returns items that are not in DB.
        /// </summary>
        /// <param name="goods">XML items</param>
        /// <param name="allNavigationIds">IDs of navigations</param>
        /// <param name="dbItemIds">ID of items stored in database</param>
        /// <returns></returns>
        private IEnumerable<HelpItem> GetItemsNotInDb(XElement goods, List<int> allNavigationIds, List<int> dbItemIds)
        {
            var allItems = (from item in goods.Elements() select new HelpItem() { id = Convert.ToInt32(item.Element("ID").Value), parent = Convert.ToInt32(item.Element("CategoryID").Value), price = Convert.ToDouble(item.Element("Price").Value.Replace('.', ',')) }).Distinct();
            var xmlItems = allItems.Where(i => i.price > 0);
            var allItemsWithParent = xmlItems.Where(i => allNavigationIds.Contains(i.parent));
            var dbMissingItems = (from item in allItemsWithParent where !dbItemIds.Contains(item.id) select item);
            return dbMissingItems;
        }

        /// <summary>
        /// Save navigations to database.
        /// </summary>
        /// <param name="catgs">Navigations of XML</param>
        /// <param name="pl">Price lis</param>
        private void SaveNavigations(XElement catgs, int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                PriceList priceList = (PriceList)context.PriceListRepository.GetObjectByID(priceListId);
                Console.WriteLine("Pridávanie navigácií cenníku '" + priceList.Name + "'.");

                // Pre každú kategóriu
                foreach (XElement xmlNavigation in catgs.Elements())
                {
                    int order = Convert.ToInt32(xmlNavigation.Element("ID").Value);
                    if (_parents.ContainsKey(order))
                    {
                        Console.WriteLine("Navigácia: '" + xmlNavigation + "' sa opakuje!");
                    }
                    else
                    {
                        Navigation navigation = new Navigation() { Description = xmlNavigation.Element("Description").Value, Level = 0, PriceList = priceList, Order = order, Name = xmlNavigation.Element("Name").Value };
                        XElement xmlParent = xmlNavigation.Element("ParentIDS").Elements().First();
                        int parentId = Convert.ToInt32(xmlParent.Value);

                        SetParent(parentId, navigation);
                        SetChildren(order, navigation);

                        _parents.Add(order, navigation);
                    }
                }
                priceList.Navigations = SetBaseLevelToNavigations();
                Console.WriteLine("Ukladanie navigácií do databázy.");
                context.SaveChanges();
                Console.WriteLine("Navigácie pre cenník '" + priceList.Name + "' boli úspešne aktualizované.");
            }
        }

        /// <summary>
        /// Set parent to navigation.
        /// </summary>
        /// <param name="parentId">ID of parent navigation</param>
        /// <param name="navigation">Navigation</param>
        private void SetParent(int parentId, Navigation navigation)
        {
            if (_parents.ContainsKey(parentId)) // Pridaj rodiča 
            {
                navigation.Navigation2 = _parents[parentId]; //  Spoliehame sa že navigation má vždy len jedneho rodiča.
                _parents[parentId].Navigation1.Add(navigation);
            }
            else
            {
                if (!_navigationsWithoutParents.ContainsKey(parentId)) // Pridaj do zoznamu čakajúcich na rodiča
                {
                    _navigationsWithoutParents[parentId] = new List<Navigation>();
                }
                _navigationsWithoutParents[parentId].Add(navigation);
            }
        }

        /// <summary>
        /// Set children to navigation.
        /// </summary>
        /// <param name="order">Element ID in navigation</param>
        /// <param name="navigation">Navigation</param>
        private void SetChildren(int order, Navigation navigation)
        {
            if (_navigationsWithoutParents.ContainsKey(order)) // Priraď deti
            {
                foreach (Navigation n in _navigationsWithoutParents[order])
                {
                    n.Navigation2 = navigation;
                    navigation.Navigation1.Add(n);
                }
                _navigationsWithoutParents.Remove(order);
            }
        }

        /// <summary>
        /// Set 0 level to navigations of parent 0. Other navigations ignore.
        /// </summary>
        private List<Navigation> SetBaseLevelToNavigations()
        {
            if (_navigationsWithoutParents.ContainsKey(0))
            {
                foreach (Navigation navigation in _navigationsWithoutParents[0])
                {
                    SetLevelToChildren(navigation);
                }
                return _navigationsWithoutParents[0];
            }
            return null;
        }

        private void SetLevelToChildren(Navigation parent)
        {
            foreach (Navigation navigation in parent.Navigation1)
            {
                navigation.Level = Convert.ToInt16(parent.Level + 1);
                SetLevelToChildren(navigation);
            }
        }

        /// <summary>
        /// Save XML items to database.
        /// </summary>
        /// <param name="goods">XML items</param>
        /// <param name="priceListId">Price list ID</param>
        private void SaveItems(XElement goods, int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                Console.WriteLine("Ukladanie položiek cenníka '" + context.PriceListRepository.GetObjectByID(priceListId).Name + "' do databázy.");
                context.LazyLoading = true;

                IEnumerable<XElement> itemNavigation = from item in goods.Elements() where (double)item.Element("Price") == 0 select item;
                Dictionary<int, List<Navigation>> navigations = GetDictionaryOfNavigationsFromXElement(itemNavigation);

                MakeNavigationsFromItemsAndAddItems(context, goods, navigations, priceListId);
                Console.WriteLine("Ukladanie položiek do databázy.");
                context.SaveChangesAndRecreateContext(true);

                var itemIds = GetAllItemIdsItems(GetAllItemsFromDB(priceListId));
                var allNavigations = context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceListId).ToList();

                // Starý kód slúžiaci na získavanie položiek, kt. nie sú v DB ale patria tam.
                //IEnumerable<XElement> itemsNotInDB = from item in goods.Elements() where !itemIds.Contains(Convert.ToInt64(item.Element("ID").Value)) && navigations.Keys.Contains(Convert.ToInt32(item.Element("CategoryID").Value)) && (double)item.Element("Price") > 0 select item;
                //var x = (from item in itemsNotInDB select new { id = Convert.ToInt64(item.Element("ID").Value), parent = Convert.ToInt32(item.Element("CategoryID").Value), price = Convert.ToDouble(item.Element("Price").Value.Replace('.', ',')) }).Distinct();

                List<int> allNavigationIds = new List<int>();
                allNavigations.ForEach(n => allNavigationIds.Add((int)n.Order));
                var itemsNotInDb = GetItemsNotInDb(goods, allNavigationIds, itemIds);

                AddOtherItems(context, goods, itemsNotInDb, priceListId);

                context.SaveChanges();
                Console.WriteLine("Položky pre cenník '" + context.PriceListRepository.GetObjectByID(priceListId).Name + "' boli súspešne aktualizované.");
            }
        }

        /// <summary>
        /// Returns all items of price list.
        /// </summary>
        /// <param name="priceListId">Price list ID</param>
        /// <returns>List of items of price list</returns>
        private List<Item> GetAllItemsFromDB(int priceListId)
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                context.LazyLoading = true;
                IEnumerable<Navigation> navigations = context.NavigationRepository.GetObjectsByExpression(navigation => navigation.IdPriceList == priceListId);

                List<Item> dbItems = new List<Item>();
                foreach (Navigation navigation in navigations)
                {
                    dbItems.AddRange(navigation.Items);
                }

                return dbItems;
            }
        }

        /// <summary>
        /// Returns list of IDs from tems.
        /// </summary>
        /// <param name="items">List of items</param>
        /// <returns>List of Ids of items</returns>
        private List<int> GetAllItemIdsItems(List<Item> items)
        {
            List<int> ids = new List<int>();
            items.ForEach(i => ids.Add(i.Order));
            return ids;
        }

        /// <summary>
        /// Make navigations from items with 0 price and add items to them.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="goods">XML items</param>
        /// <param name="navigations"> Items with 0 price</param>
        /// <param name="priceListId">Price list ID</param>
        private void MakeNavigationsFromItemsAndAddItems(AmazingDataModelUnit context, XElement goods, Dictionary<int, List<Navigation>> navigations, int priceListId)
        {
            foreach (int parentId in navigations.Keys)
            {
                var parentNavigations = context.NavigationRepository.GetObjectsByExpression(nav => nav.Order == parentId && nav.IdPriceList == priceListId);
                if (parentNavigations.Any())
                {
                    Navigation parentNavigation = parentNavigations.First();
                    IEnumerable<XElement> items = from item in goods.Elements() where (double)item.Element("Price") > 0 && Convert.ToInt32(item.Element("CategoryID").Value) == parentId select item;
                    foreach (XElement xItem in items)
                    {
                        Item item = GetItemOfExistingNavigationFromXmlElement(xItem);
                        if (item != null)
                        {
                            Navigation realParent = GetParentItemNavigation(navigations[parentId], item.Name.Split(' '));

                            if (realParent == null)
                            {
                                if (!ContainsItem(parentNavigation.Items, item))
                                {
                                    parentNavigation.Items.Add(item);
                                }
                            }
                            else if (!ContainsItem(realParent.Items, item))
                            {
                                realParent.Items.Add(item);
                            }
                        }
                    }

                    foreach (Navigation navigation in navigations[parentId])
                    {
                        parentNavigation.Navigation1.Add(navigation);
                        navigation.Parent = parentNavigation.Id;
                        navigation.PriceList = parentNavigation.PriceList;
                        navigation.Level = (short)(parentNavigation.Level + 1);
                    }
                }
            }
        }

        /// <summary>
        /// Add items that was not added.
        /// </summary>
        /// <param name="context">Database context</param>
        /// <param name="items">Items</param>
        /// <param name="priceListId">Price list ID</param>
        private void AddOtherItems(AmazingDataModelUnit context, XElement goods, IEnumerable<HelpItem> items, int priceListId)
        {
            foreach (HelpItem helpItem in items)
            {
                XElement xmlItem = (from item in goods.Elements() where Convert.ToInt32(item.Element("CategoryID").Value) == helpItem.parent && Convert.ToInt64(item.Element("ID").Value) == helpItem.id select item).FirstOrDefault();
                Item item1 = GetItemOfExistingNavigationFromXmlElement(xmlItem);
                if (item1 != null)
                {
                    Navigation navigation = context.NavigationRepository.GetObjectsByExpression(navig => navig.Order == helpItem.parent && navig.IdPriceList == priceListId).FirstOrDefault();
                    if (navigation != null)
                    {
                        navigation.Items.Add(item1);
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    ;
                }
            }

            //Dictionary<int, List<int>> addeditems = new Dictionary<int, List<int>>();
            //foreach (XElement xmlItem in items)
            //{
            //    int navigationId = Convert.ToInt32(xmlItem.Element("CategoryID").Value);
            //    var navigations2 = context.NavigationRepository.GetObjectsByExpression(navig => navig.Order == navigationId && navig.IdPriceList == priceListId);
            //    if (navigations2.Any())
            //    {
            //        Item item = GetItemOfExistingNavigationFromXmlElement(xmlItem);
            //        if (item != null)
            //        {

            //            if (!addeditems.ContainsKey(navigationId))
            //            {
            //                addeditems.Add(navigationId, new List<int>());
            //            }
            //            if (!addeditems[navigationId].Contains(item.Id))
            //            {
            //                navigations2.First().Items.Add(item);
            //                addeditems[navigationId].Add(item.Id);
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Returns true when collection contains item.
        /// </summary>
        /// <param name="items">Collection of items</param>
        /// <param name="item">item</param>
        /// <returns></returns>
        private bool ContainsItem(ICollection<Item> items, Item item)
        {
            foreach (Item helpItem in items)
            {
                if (helpItem.Order == item.Order)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns correct navigation from list via array of string.
        /// </summary>
        /// <param name="navigations">List of navigations</param>
        /// <param name="splitItemName">Array of item name</param>
        /// <returns>Parent of item which name is in array parameter</returns>
        private Navigation GetParentItemNavigation(List<Navigation> navigations, string[] splitItemName)
        {
            foreach (Navigation navigation in navigations)
            {
                string[] splitNavigationName = navigation.Name.Split();
                if (splitNavigationName.Length < splitItemName.Length)
                {
                    short j = 0;
                    short i = 0;
                    for (i = 0; i < splitNavigationName.Length; i++)
                    {
                        while (i + j < splitItemName.Length && !splitNavigationName[i].Equals(splitItemName[i + j]))
                        {
                            j++;
                        }
                        if (i + j == splitItemName.Length)
                        {
                            break;
                        }
                    }
                    if (i == splitNavigationName.Length)
                    {
                        return navigation;
                    }
                }
                else if (splitNavigationName.Length == splitItemName.Length)
                {
                    short i;
                    for (i = 0; i < splitNavigationName.Length; i++)
                    {
                        if (!splitNavigationName[i].Equals(splitItemName[i]))
                        {
                            break;
                        }
                    }
                    if (splitNavigationName.Length == i)
                    {
                        return navigation;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Returns navigations
        /// </summary>
        /// <param name="itemNavigations">Elements</param>
        /// <returns>Dictionary of navigations where key is parent element</returns>
        private Dictionary<int, List<Navigation>> GetDictionaryOfNavigationsFromXElement(IEnumerable<XElement> itemNavigations)
        {
            Dictionary<int, List<Navigation>> dictItemNavigations = new Dictionary<int, List<Navigation>>();
            foreach (XElement element in itemNavigations)
            {
                if (HasItemNavigation(element))
                {
                    Navigation navigation = new Navigation() { Name = element.Element("Name").Value, Description = element.Element("Description").Value, Order = Convert.ToInt32(element.Element("ID").Value), Parent = Convert.ToInt32(element.Element("CategoryID").Value) };
                    if (dictItemNavigations.ContainsKey((int)navigation.Parent))
                    {
                        bool canAdd = true;
                        foreach (Navigation nav in dictItemNavigations[(int)navigation.Parent])
                        {
                            if (navigation.Order == nav.Order)
                            {
                                canAdd = false;
                                break;
                            }
                        }
                        if (canAdd)
                        {
                            dictItemNavigations[(int)navigation.Parent].Add(navigation);
                        }
                    }
                    else
                    {
                        dictItemNavigations.Add((int)navigation.Parent, new List<Navigation>());
                        dictItemNavigations[(int)navigation.Parent].Add(navigation);
                    }
                }
            }
            return dictItemNavigations;
        }

        /// <summary>
        /// Returns item from XML item.
        /// </summary>
        /// <param name="xmlItem">XML item</param>
        /// <returns>Item</returns>
        private Item GetItemOfExistingNavigationFromXmlElement(XElement xmlItem)
        {
            Item item = new Item();

            if (!HasItemNavigation(xmlItem))
            {
                return null;
            }

            item.Order = Convert.ToInt32(xmlItem.Element("ID").Value);
            item.Name = xmlItem.Element("Name").Value;
            item.Description = xmlItem.Element("Description").Value;
            string price = xmlItem.Element("Price").Value;
            price = price.Replace('.', ',');
            item.Price = Convert.ToDecimal(price);
            item.Unit = xmlItem.Element("Unit").Value;
            item.Url = xmlItem.Element("URL").Value;
            item.ImgUrl = xmlItem.Element("Image").Value;
            item.Code = item.Order.ToString();

            return item;
        }

        /// <summary>
        /// Returns true if XML item has existing navigation.
        /// </summary>
        /// <param name="xmlItem">XML item</param>
        /// <returns>true if item has navigation else false</returns>
        private bool HasItemNavigation(XElement xmlItem)
        {
            var parentId = Convert.ToInt32(xmlItem.Element("CategoryID").Value);

            if (_parents.ContainsKey(parentId))
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}
