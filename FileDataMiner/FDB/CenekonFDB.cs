﻿using FirebirdSql.Data.FirebirdClient;
using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace FileDataMiner
{
    // ######################## V navigation path si uchovávame staré IDčka !!! ##############################
    /// <summary>
    ///  
    /// </summary>
    class CenekonFDB : IDataMiner
    {
        private PriceList _priceList;
        private FbConnection _connection;
        private List<Navigation> _navigations;

        private static byte[] _salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        private static string _initVector = "0123456789123456";

        public CenekonFDB()
        {
            _navigations = new List<Navigation>();
        }

        public void Execute(string pathToFile, PriceList priceList)
        {
            _priceList = priceList;
            _connection = Get_Connection(pathToFile);
            _connection.Open();

            //ReadTestItems();
            ReadTables();

            _connection.Close();
        }

        private void ReadTestItems() //#############################################################
        {
            FbCommand cmd = new FbCommand("Select * from \"Polozka\"", _connection);
            var reader = cmd.ExecuteReader();
            List<Item> items = new List<Item>();

            int riadok = 0;
            while (reader.Read() && riadok<500)
            {
                var riadokTyp = reader.GetInt32(12);
                if (riadokTyp == 70 || riadokTyp == 50)
                {
                    var code = reader[9];
                    var orderStr = reader[10];
                    var name = reader[13];
                    var description = reader[14];
                    var unit = reader[17];
                    var priceStr = reader[19];
                    var weightStr = reader[21];

                    if (code == null || DBNull.Value.Equals(code))
                    {
                        continue;
                    }
                    int order;
                    int.TryParse(orderStr.ToString(), out order);
                    name = DBNull.Value.Equals(name) ? "n/a" : Decrypt(name.ToString(), "dChlsXh965hdfkljsdf");

                    description = DBNull.Value.Equals(description) ? "" : Decrypt(description.ToString(), "dChlsXh965hdfkljsdf");
                    unit = DBNull.Value.Equals(unit) || unit.Equals("") ? "n/a" : unit;
                    decimal price;
                    Decimal.TryParse(priceStr.ToString(), out price);
                    double weight;
                    Double.TryParse(weightStr.ToString(), out weight);


                    Item item = new Item() { Code = code.ToString(), Order = order, Name = name.ToString(), Description = description.ToString(), Unit = unit.ToString(), Price = price, Weight = weight };
                    items.Add(item);
                    riadok++;
                }
            }
        }

        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            _priceList = priceList;

            _connection = Get_Connection(pathToFile);
            _connection.Open();
            int navigationCount = 0; 
            int itemCount = 0;
            List<int> navigationIds=new List<int>();

            FbCommand cmd = new FbCommand("Select * from \"Navigacia\" where \"NavigaciaTyp\"=1 or \"NavigaciaTyp\"=2", _connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                navigationIds.Add(reader.GetInt32(0));
                navigationCount++;
            }

            cmd = new FbCommand("Select * from \"Polozka\" where \"Navigacia\" in (Select \"OID\" from \"Navigacia\" where \"NavigaciaTyp\"=1 or \"NavigaciaTyp\"=2)", _connection);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                itemCount++;
            }

            int dbNavigationCount;
            int dbItemCount;
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                context.PriceListRepository.Attach(_priceList);
                var dbNavigations =
                    context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceList.Id).Select(nav=>nav.Id);
                dbItemCount =
                    context.ItemRepository.GetObjectsByExpression(item => dbNavigations.Contains(item.Navigations.FirstOrDefault().Id)).Count();
                dbNavigationCount = dbNavigations.Count();
            }

            _connection.Close();

            if (navigationCount == dbNavigationCount && itemCount==dbItemCount)
            {
                return true;
            }
            return false;
        }

        private FbConnection Get_Connection(string path)
        {
            string cs = @"DataSource=localhost;User=SYSDBA;Password=masterkey;Database=" + path + ";ServerType=0;Charset=UTF8; Port=42042;Pooling=false";
            return new FbConnection(cs);
        }

        private List<Navigation> ReadBaseNavigations(int NavigationType, Navigation parent) //Materialy=2, Konstrukcie=1
        {
            FbCommand cmd = new FbCommand("Select * from \"Navigacia\" where \"NavigaciaTyp\"=" + NavigationType + " and \"Uroven\"=1", _connection);
            var reader = cmd.ExecuteReader();

            List<Navigation> navigations = new List<Navigation>();
            while (reader.Read())
            {
                Navigation nav = new Navigation() { NavigationPath = reader.GetString(0), PriceList = _priceList, Name = reader.GetString(10), Level = reader.GetInt16(7), Order = reader.GetInt32(9) };

                nav.Navigation2 = parent;
                navigations.Add(nav);
            }
            parent.Navigation1 = navigations;

            return navigations;
        }

        private void ReadTables()
        {
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                Console.WriteLine("Začiatok spracovania databázy Cenekon.");

                context.PriceListRepository.Attach(_priceList);

                Navigation konstrukcie = new Navigation() { Name = "Konštrukcie", PriceList = _priceList, Level = 0 };
                Navigation materialy = new Navigation() { Name = "Materiály", PriceList = _priceList, Level = 0 };

                _priceList.Navigations.Add(konstrukcie);
                _priceList.Navigations.Add(materialy);
                _navigations.AddRange(ReadBaseNavigations(1, konstrukcie));
                _navigations.AddRange(ReadBaseNavigations(2, materialy));

                context.SaveChangesAndRecreateContext(false);
                context.PriceListRepository.Attach(_priceList);

                int index = 0;
                while (0 < _navigations.Count)
                {
                    if (_navigations.ElementAt(0).NavigationPath.Equals("4029")) //navigacia v konstrukciach ktora obsahuje zakladne navigacie materialov
                    {
                        _navigations.RemoveAt(0);
                        index++;
                        continue;
                    }

                    Navigation navigation = context.NavigationRepository.GetObjectByID(_navigations.ElementAt(0).Id);

                    ReadChildNavigations(navigation, _priceList);
                    ReadChildItems(navigation);

                    if (index % 100 == 0)
                    {
                        context.SaveChangesAndRecreateContext(false);
                        context.PriceListRepository.Attach(_priceList);
                        Console.Write("\rSpracovanych " + index + " navigacii.");
                    }
                    _navigations.RemoveAt(0);
                    index++;
                }
                context.SaveChanges();
                Console.WriteLine("\rSpracovanych všetkých " + index + " navigacii.");
            }
        }

        private void ReadChildNavigations(Navigation navig, PriceList priceList)
        {
            FbCommand cmd = new FbCommand("Select * from \"Navigacia\" where \"Rodic\"=" + navig.NavigationPath + "", _connection);
            var reader = cmd.ExecuteReader();

            List<Navigation> navigations = new List<Navigation>();

            while (reader.Read())
            {
                Navigation navigation = new Navigation() { NavigationPath = reader.GetString(0), PriceList = priceList, Name = reader.GetString(10), Level = reader.GetInt16(7), Order = reader.GetInt32(9) };
                navigations.Add(navigation);
            }
            if (navigations.Count != 0)
            {
                navig.Navigation1 = navigations;
                _navigations.AddRange(navigations);
            }
        }

        private void ReadChildItems(Navigation navigation)
        {
            FbCommand cmd = new FbCommand("Select * from \"Polozka\" where \"Navigacia\"=" + navigation.NavigationPath + "", _connection);
            var reader = cmd.ExecuteReader();
            List<Item> items = new List<Item>();

            while (reader.Read())
            {
                var riadokTyp = reader.GetInt32(12);
                if (riadokTyp == 70)
                {
                    var code = reader[9];
                    var orderStr = reader[10];
                    var name = reader[14];//reader[13];
                    var description = reader[14];
                    var unit = reader[17];
                    var priceStr = reader[19];
                    var weightStr = reader[21];

                    if (code == null || DBNull.Value.Equals(code))
                    {
                        continue;
                    }
                    int order;
                    int.TryParse(orderStr.ToString(), out order);
                    name = DBNull.Value.Equals(name) ? "n/a" : Decrypt(name.ToString(), "dChlsXh965hdfkljsdf");

                    description = DBNull.Value.Equals(description) ? "" : Decrypt(description.ToString(), "dChlsXh965hdfkljsdf");
                    unit = DBNull.Value.Equals(unit) || unit.Equals("") ? "n/a" : unit;
                    decimal price;
                    Decimal.TryParse(priceStr.ToString(), out price);
                    double weight;
                    Double.TryParse(weightStr.ToString(), out weight);


                    Item item = new Item() { Code = code.ToString(), Order = order, Name = name.ToString(), Description = description.ToString(), Unit = unit.ToString(), Price = price, Weight = weight };
                    items.Add(item);
                }
            }
            if (items.Count != 0)
            {
                navigation.Items = items;
            }
        }


        public static string Decrypt(string iCipherText, string iKey)
        {
            string ret = null;

            if (!string.IsNullOrEmpty(iCipherText))
            {
                string CipherText = iCipherText.Substring(1);
                byte[] cipherTextBytes = Convert.FromBase64String(CipherText);
                byte[] initVector = Encoding.ASCII.GetBytes(_initVector);
                byte[] key;
                byte[] plainTextBytes;
                int decryptedByteCount;
                ICryptoTransform decryptor;
                MemoryStream memoryStream;
                CryptoStream cryptoStream;

                PasswordDeriveBytes password = new PasswordDeriveBytes(iKey, _salt);
                key = password.GetBytes(256 / 8);

                RijndaelManaged k = new RijndaelManaged();
                k.Mode = CipherMode.CBC;

                initVector[0] = (byte)iCipherText[0];

                decryptor = k.CreateDecryptor(key, initVector);
                memoryStream = new MemoryStream(cipherTextBytes);
                cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

                plainTextBytes = new byte[cipherTextBytes.Length];

                decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                memoryStream.Close();
                cryptoStream.Close();

                ret = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            }

            return ret;
        }
    }
}
