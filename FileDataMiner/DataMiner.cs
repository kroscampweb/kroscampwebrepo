﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FileDataMiner
{
    class DataMiner
    {

        public IDataMiner GetDataMiner(string pathToFile)
        {
            if (File.Exists(pathToFile))
            {
                var extension = Path.GetExtension(pathToFile);
                switch (extension)
                {
                    case ".xml":
                        return GetXMLDataMiner(pathToFile);
                    case ".csv":
                        return GetCSVDataMiner(pathToFile);
                    case ".fdb":
                        return new CenekonFDB();
                }
                return null;
            }
            throw new FileNotFoundException("Súbor neexistuje!");
        }

        private IDataMiner GetXMLDataMiner(string pathToFile)
        {
            var xmlDocument = XElement.Load(pathToFile);
            switch (xmlDocument.Name.ToString())
            {
                case "SHOP":
                    var elements = xmlDocument.Elements();
                    if (elements.Count() == 2)
                    {
                        return new NejStavebninyXML();
                    }
                    else
                    {
                        var element = elements.FirstOrDefault();
                        if (element.Name.LocalName.Equals("SHOPITEM"))
                        {
                            var f = from elementAttribute in element.Elements() select elementAttribute;
                            var EstavmatElements = from elementAttribute in element.Elements() where elementAttribute.Name.LocalName.Equals("PRODUCTNO") select elementAttribute;
                            var x = EstavmatElements.ToList();
                            if (EstavmatElements.Count() > 0)
                            {
                                return new EstavmatXML();
                            }
                            else
                            {
                                return new TriparkXML();
                            }
                        }
                    }
                    return null;
                case "EShop":
                    var navigation = xmlDocument.Elements().FirstOrDefault();
                    if (navigation.Attribute("description") != null)
                    {
                        return new LacneStavanieCenkrosXML();
                    }
                    else if (navigation.Attribute("Description") != null)
                    {
                        return new KrosFormat();
                    }
                    else return null;
                case "products":
                    return new HagardXML();
                case "eshop": // SolidStav
                    return new KrosFormat();
            }
            return null;
        }

        private IDataMiner GetCSVDataMiner(string pathToFile)
        {
            var firstLine = new System.IO.StreamReader(pathToFile).ReadLine();
            if (firstLine.Split('#')[0].Equals("category_path"))
            {
                return new StavivaPavlikSolarikCSV();
            }
            else if (firstLine.Split(';')[0].Equals("kód"))
            {
                return new RavenCSV();
            }
            else
            {
                return new VerexCSV();
            }
        }

    }
}
