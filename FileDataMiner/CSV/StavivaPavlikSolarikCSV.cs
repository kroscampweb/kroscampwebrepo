﻿using ModelAndTools.Models;
using ModelAndTools.Models.DataModels;
using System.Collections.Generic;
using System.IO;
using ModelAndTools.Tools;
using ModelAndTools.Tools.External;
using System.Linq;
using System;
using ModelAndTools.Tools.External;

namespace FileDataMiner
{
    /// <summary>
    /// Dataminer for Staviva Pavlik a Solarik, format csv file
    /// </summary>
    class StavivaPavlikSolarikCSV : IDataMiner
    {
        #region Private fields

        private PriceList _priceList;
        private List<Navigation> _navigations;
        private AmazingDataModelUnit _context;
        private string[] _lines; 

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public StavivaPavlikSolarikCSV()
        {
            _context = new AmazingDataModelUnit();
            _navigations = new List<Navigation>();
        } 

        #endregion

        #region Public methods

        /// <summary>
        /// Mine data from file and update database.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        public void Execute(string pathToFile, PriceList priceList)
        {
            _lines = File.ReadAllLines(pathToFile);
            _priceList = priceList;
            _context.PriceListRepository.Attach(_priceList);
            LoadNavigations();
            LoadItems();
            SaveLoaded();
        }

        /// <summary>
        /// Check if data was correctly updated.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        /// <returns>true if update was succesfull else false</returns>
        public bool IsExecutedCorrectly(string pathToFile, PriceList priceList)
        {
            Console.WriteLine("Reading data from File");
            _lines = File.ReadAllLines(pathToFile);
            int FileItemCount = _lines.Count() - 1;


            Console.WriteLine("Reading data from Database");
            _context = new AmazingDataModelUnit();
            IEnumerable<int> DBNavigationIds = _context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == priceList.Id).Select(nav => nav.Id);
            int DBItemCount = _context.ItemRepository.GetObjectsByExpression(item => DBNavigationIds.Contains(item.Navigations.FirstOrDefault().Id)).Count();
            _context.Dispose();
            if (FileItemCount == DBItemCount)
            {
                return true;
            }
            return false;
        } 

        #endregion

        #region Private helpers

        /// <summary>
        /// Load navigations from file.
        /// </summary>
        private void LoadNavigations()
        {
            for (int j = 1; j < _lines.Count(); j++)
            {
                string line = _lines[j];
                var part = (List<string>)line.SplitBySeparator("#");
                var navigationPaths = part[0].SplitBySeparator("|");

                foreach (var navigationPath in navigationPaths)
                {
                    List<string> navigations = navigationPath.SplitBySeparator("/").ToList();
                    for (short index = 0; index < navigations.Count; index++)
                    {
                        Navigation nav = new Navigation() { Name = navigations[index], PriceList = _priceList, Level = index };
                        Navigation parent = new Navigation();
                        if (index > 0)
                        {
                            List<string> path = new List<string>();
                            for (int i = 0; i < index; i++)
                            {
                                path.Add(navigations[i]);
                            }
                            parent = GetNavigation(path);
                            nav.Navigation2 = parent;
                        }
                        if (!NavigationExists(nav, index))
                        {
                            if (index == 0)
                            {
                                _priceList.Navigations.Add(nav);
                            }
                            else
                            {
                                parent.Navigation1.Add(nav);
                            }
                            _navigations.Add(nav);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Load items from file.
        /// </summary>
        private void LoadItems()
        {
            for (int j = 1; j < _lines.Count(); j++)
            {
                string line = _lines[j];
                var part = line.Split(new char[] { '#' }).ToList();
                var navigationPaths = part[0].SplitBySeparator("|");

                Item itm = new Item();
                itm.Code = part[5];
                itm.Name = part[2];
                itm.Description = part[3];
                itm.Order = int.Parse(part[5]);

                foreach (var navigationPath in navigationPaths)
                {
                    List<string> navigations = navigationPath.SplitBySeparator("/").ToList();
                    Navigation parent = GetNavigation(navigations.ToList());
                    itm.Navigations.Add(parent);
                    parent.Items.Add(itm);
                }


                itm.Price = decimal.Parse(part[6].ReplaceWithCorrectDecimalSeparator());
                itm.Unit = "n/a";
                itm.Url = part[4];
                itm.ImgUrl = part[1];
            }
        }

        /// <summary>
        /// Save loaded navigations and items to database.
        /// </summary>
        private void SaveLoaded()
        {
            _context.SaveChanges();
            _context.Dispose();
        }

        /// <summary>
        /// Checks if navigation already exists
        /// </summary>
        /// <param name="navigation">navigation we wish to check</param>
        /// <param name="level">level of the navigation</param>
        /// <returns>true if exists</returns>
        private bool NavigationExists(Navigation navigation, int level)
        {
            var navigations = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigation.Name));
            if (navigations.Count() == 0)
            {
                return false;
            }
            else
            {
                if (level == 0)
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Name.Equals(nav.Name) && nav.Level == 0)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    foreach (var nav in navigations)
                    {
                        if (navigation.Navigation2 == nav.Navigation2)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns navigation from navigation path
        /// </summary>
        /// <param name="navigations">navigation path</param>
        /// <returns>navigation</returns>
        private Navigation GetNavigation(List<string> navigations)
        {
            Navigation result;
            result = _navigations.Where<Navigation>(nav => nav.Name.Equals(navigations[0]) && nav.Level == 0).First();
            for (int i = 1; i < navigations.Count(); i++)
            {
                result = result.Navigation1.Where<Navigation>(nav => nav.Name.Equals(navigations[i])).First();
            }

            return result;
        } 

        #endregion
    }
}
