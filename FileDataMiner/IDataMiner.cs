﻿using ModelAndTools.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataMiner
{
    interface IDataMiner
    {
        /// <summary>
        /// Mine data from file and update database.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        void Execute(string pathToFile, PriceList priceList);

        /// <summary>
        /// Check if data was correctly updated.
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="priceList">Updated price list</param>
        /// <returns>true if update was succesfull else false</returns>
        bool IsExecutedCorrectly(string pathToFile, PriceList priceList);
    }
}
