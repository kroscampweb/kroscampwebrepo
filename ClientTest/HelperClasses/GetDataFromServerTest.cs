﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelAndTools.Models;
using System.Collections.Generic;
using Klient.ViewModel;
using Klient.HelperClasses;
using System.Net;
using Newtonsoft.Json.Linq;
using ModelAndTools.Models.DataModels;

namespace ClientTest.HelperClasses
{
    [TestClass]
    public class GetDataFromServerTest
    {
        // TODO: Otestovať všetko okrem vyhľadávania - to sa otestovať asi ani nedá :)

        [TestMethod]
        public void GetDistributorsTest()
        {
            List<DistributorViewModel> distributors = new List<DistributorViewModel>();
            JArray data = GetJArrayFromServer("http://localhost:1832/api/distributors");

            foreach (var obj in data)
            {
                PriceListDistributor priceListDistributor = obj.ToObject<PriceListDistributor>();
                DistributorViewModel dist = new DistributorViewModel(priceListDistributor);
                dist.Navigations.Add(new NavigationViewModel(null));
                distributors.Add(dist);
            }

            int dbDistributorCount;
            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                dbDistributorCount = context.PriceListRepository.GetAllObjects().Count();
            }

            Assert.AreEqual(dbDistributorCount, distributors.Count);
        }

        [TestMethod]
        public void GetNavigationsOfPriceList()
        {
            List<NavigationViewModel> navigations = new List<NavigationViewModel>();
            int dbNavigationCount;
            int idPriceList;

            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var ids = context.PriceListRepository.GetAllObjects().Select(priceList => priceList.Id).ToList();
                Random rand = new Random();
                idPriceList = ids[rand.Next(ids.Count())];

                dbNavigationCount =
                    context.NavigationRepository.GetObjectsByExpression(nav => nav.IdPriceList == idPriceList && nav.Level == 0).Count();
            }

            var data = GetJArrayFromServer("http://localhost:1832/api/navigations/GetBaseNavigationOfPriceList/" + idPriceList);

            foreach (var obj in data)
            {
                Navigation navigation = obj.ToObject<Navigation>();
                NavigationViewModel nav = new NavigationViewModel(navigation);
                nav.ChildNavigations.Add(new NavigationViewModel(null));
                navigations.Add(nav);
            }
            Assert.AreEqual(dbNavigationCount, navigations.Count);
        }

        [TestMethod]
        public void GetChildNavigations()
        {
            List<NavigationViewModel> childNavigations = new List<NavigationViewModel>();
            int idNavigation;
            int dbNavigationCount;

            using (AmazingDataModelUnit context=new AmazingDataModelUnit())
            {
                var ids = context.NavigationRepository.GetAllObjects().Select(nav => nav.Id).ToList();
                Random rand = new Random();
                idNavigation = ids[rand.Next(ids.Count())];
                dbNavigationCount=context.NavigationRepository.GetObjectsByExpression(nav => nav.Parent == idNavigation).Count();
            }


            JArray data = GetJArrayFromServer("http://localhost:1832/api/navigations/GetChildrenOfNavigation/" + idNavigation);

            foreach (var obj in data)
            {
                Navigation navigation = obj.ToObject<Navigation>();
                NavigationViewModel nav = new NavigationViewModel(navigation);
                nav.ChildNavigations.Add(new NavigationViewModel(null));
                childNavigations.Add(nav);
            }

            Assert.AreEqual(dbNavigationCount,childNavigations.Count);
        }

        [TestMethod]
        public void GetItems()
        {
            List<ItemViewModel> items = new List<ItemViewModel>();
            int idNavigation; 
            int dbItemCount;

            using (AmazingDataModelUnit context = new AmazingDataModelUnit())
            {
                var ids = context.NavigationRepository.GetAllObjects().Select(nav => nav.Id).ToList();
                Random rand = new Random();
                idNavigation = ids[rand.Next(ids.Count())];
                dbItemCount = context.ItemRepository.GetObjectsByExpression(item => item.Navigations.Select(nav => nav.Id).Contains(idNavigation)).Count();
            }

            JArray data = GetJArrayFromServer("http://localhost:1832/api/items/SetItemsOfNavigation/" + idNavigation);
            
            foreach (var obj in data)
            {
                var x = obj["navigations"];
                if (obj["Code"] != null && obj["Name"] != null)
                {
                    Item i = obj.ToObject<Item>();
                    ItemViewModel item = new ItemViewModel(i);
                    items.Add(item);
                }
            }
            Assert.AreEqual(dbItemCount,items.Count);
        }

        private JArray GetJArrayFromServer(string url)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                var jsonDistributors = client.DownloadString(url);
                if (jsonDistributors.Equals("null"))
                {
                    return JArray.Parse("[]");
                }
                return JArray.Parse(jsonDistributors);
            }
        }
    }
}
